---
title: "Statistical Test used in NBI Sales Effect"
output: html_document
---
<style type="text/css">

body{ 
   font-size: 15px;
}

table.sample {
  width: 100%;
  border-top-with: 1px;
  border-bottom-width: 1px;
	border-left-width: 0px;
	border-right-width: 0px;
	border-spacing: 2px;
	border-style: outset;
	border-color: gray;
	border-collapse: collapse;
	background-color: white;
	margin: auto;
}

caption {
    caption-side: top;
    color: black;
    font-size: 15px;
    margin: auto;
}

table.sample th {
  border-top-with: 0px;
  border-bottom-width: 1px;
	border-left-width: 0px;
	border-right-width: 0px;	padding: 1px;
	border-style: inset;
	border-color: gray;
	background-color: white;
	-moz-border-radius: ;
}
table.sample td {
	border-width: 0px;
	padding: 1px;
	border-style: inset;
	border-color: gray;
	background-color: white;
	-moz-border-radius: ;
}
</style>

```{r setup, include=FALSE}
knitr::opts_chunk$set(echo = TRUE)
```


The two-sample t-test is used to determine whether the means of two groups are equal to each other. The assumption for the test is that both groups are sampled from normal distributions with unknow but equal variances. The null hypothesis is that the two means are equal, and the alternative is that they are not. Before proceeding with the t-test, it is necessary to evaluate the sample variances of the two groups, using a Fisher's F-test to verify the homoskedasticity 

For NBI Sales Effect, we use one-tailed two-sample t-Test (weighted using rim weighting weight). The null hypothesis is that the mean of the exposed group is equal or less than the mean of unexposed group, the alternative is that the mean of the exposed group is greater than the mean of unexposed group.

This documentation first explains F-test and t-test definitions, then it uses example data to calculate F-test, t-test. In the last section it compare t-test result from SAS and R.

## F-Test Definition
$$
\begin{aligned}
H_0 & :\sigma_1 = \sigma_2 \\
H_a & : \sigma_1 \neq \sigma_2 \\
\text{Test Statistic} & : F = \frac{s^2_1}{s^2_2} \\
\text{Significance Level} & : \alpha \\
\text{Rejection Region} & : F > F_{\alpha/2, N_1 - 1, N_2 - 1}
\end{aligned}
$$

$$
\begin{aligned}
\text{where } & s^2_1 \text{ and } s^2_2 \text{ are the weighted sample variances, }
\end{aligned}
$$


## t-Test Definition

$$
\begin{aligned}
H_0 & :\mu_1 - \mu_2 > 0 \\
H_a & : \mu_1 - \mu_2 <= 0 \\
\text{Test Statistic} & : T = \frac{\bar{X}_1 - \bar{X}_2}{\sqrt{s^2_1/N_1+s^2_2/N_2}} \\
\text{Significance Level} & : \alpha \\
\text{Rejection Region} & : T > t_{1-\alpha, df}
\end{aligned}
$$

$$
\begin{aligned}
\text{where } & N_1 \text{ and } N_2 \text{ are the weighted sample sizes, } N  = \sum_{i=1}^n weight_i \\
& \bar{X}_1 \text{ and } \bar{X}_2 \text{ are the weighted sample means, } \bar{X}  =\frac{\sum_{i=1}^n \left(weight_i * x_i \right)}{\sum_{i=1}^n weight_i} \\
& s^2_1 \text{ and } s^2_2 \text{ are the sample variances, } s^2  =\frac{\sum_{i=1}^n \left( weight_i *\left( x_i - \bar{x}\right)^2 \right)}{\sum_{i=1}^n weight_i - 1} \\
& df = \frac{(s^2_1/N_1 + s^2_2/N_2)^2}{(s^2_1/N_1)^2/(N_1 - 1) +(s^2_2/N_2)^2/(N_2 - 1)} \\
& \text{if equal variances are assumed, then } df = N_1 + N_2 -2
\end{aligned}
$$




## Example
### Generate some example data
```{r generate data}
spend <- c(75, 76, 80, 77, 80, 77, 73, 82, 80, 85, 85, 78, 87, 82)
weight <- c(0.5, 0.5, 2, 1, 0.5, 0.5, 2, 1, 1, 1, 1, 1, 1, 1)
exposure <- c(1, 1, 1, 1, 1, 1, 1, 0, 0, 0, 0, 0, 0, 0)

spending <- data.table::data.table(exposure = exposure, spend = spend, weight = weight)
spending
```


### F-test for equality of variances
There is no existing R package/function to do weighted F-test. So we need to construct F-test statistics ourselves.

```{r f test}
# slice the data and create vectors
spend.exposed <- spend[1:7]
spend.unexposed <- spend[8:14]

weight.exposed <- weight[1:7]
weight.unexposed <- weight[8:14]

# Calculate variances
varx = Hmisc::wtd.var(spend.exposed, weight.exposed)
vary = Hmisc::wtd.var(spend.unexposed, weight.unexposed)

# Calculate weighted N
Nx = sum(weight.exposed)
Ny = sum(weight.unexposed)

# Calculate weighted F-statistics, df, and p-value
foldedf <- max(varx, vary) / min(varx, vary)

    if (varx > vary) {

        dfnum <- Nx - 1
        dfden <- Ny - 1

    } else {

        dfnum <- Ny - 1
        dfden <- Nx - 1

    }

foldedf

    pf <- 2 * pf(foldedf,
                 df1 = dfnum,
                 df2 = dfden,
                 lower.tail = FALSE)
pf
```


### T-test using weights::wtd.t.test in R
weights::wtd.t.test can only produce Welch t-test
```{r wtd.t.test}
# set alternative = "less" to get one-tailed p-value (1/2 two-tailed p-value)
# then calculate actual p-value in EXCEL
# should we just set alternative = "greater" so that we do not need to do additional calculation in Excel
weights::wtd.t.test(x = spend.exposed
                    , y = spend.unexposed
                    , weight = weight.exposed
                    , weighty = weight.unexposed
                    , alternative = "less"
                    , mean1 = FALSE
                    , samedata = FALSE)

weights::wtd.t.test(x = spend.exposed
                    , y = spend.unexposed
                    , weight = weight.exposed
                    , weighty = weight.unexposed
                    , mean1 = FALSE
                    , samedata = FALSE)
 

```

### customized function wtd.t.test.se
Create function wtd.t.test.se to: (1) use F-test to check equal variances assumption, and (2) do two sample t-test (equal variances or unequal variances)

```{r funtion2}
# two sample weighted t test
# what if variance is zero

wtd.t.test.se <- function (dataset, var, wt, group,
                           alternative = "two.tailed", env = parent.frame()) {

  #############################################################################
  # 1. checks
  #############################################################################
  if (is.null(dataset)) {stop("Please specify dataset")}
  if (is.null(var)) {stop("Please specify var")}
  if (is.null(wt)) {stop("Please specify weight")}
  if (is.null(group)) {stop("Please specify group")}
  if (length(unique(dataset[[group]])) != 2) {
    stop("group variable must have exactly 2 levels")}
  

  #############################################################################
  # 2. weighted sample size, mean, and variance
  #############################################################################
  wtd.stats <- dataset[
    , {
      unwtd.n = length(get(var))
      wtd.n = sum(get(wt))
      m = Hmisc::wtd.mean(get(var), get(wt), na.rm = TRUE)
      v = Hmisc::wtd.var(get(var), get(wt), na.rm = TRUE)
      
      list(
        unwtd.n = unwtd.n,
        wtd.n = wtd.n,
        m = m,
        v = v)
    }
    , by = get(group)]
  
  unwtd.n <- wtd.stats[["unwtd.n"]][[1]]
  n <- wtd.stats[["wtd.n"]][1]
  mx <- wtd.stats[["m"]][1]
  vx <- wtd.stats[["v"]][1]

  unwtd.n2 <- wtd.stats[["unwtd.n"]][[2]]
  n2 <- wtd.stats[["wtd.n"]][2]
  my <- wtd.stats[["m"]][2]
  vy <- wtd.stats[["v"]][2]


  #############################################################################
  # 3. F-test
  #############################################################################
  if ((unwtd.n < 2) | (unwtd.n2 < 2)){

    # Print out warning message but do not stop the ttest process
    if (unwtd.n < 2) warning(paste("Error: not enough 'x' sample. Demo:"
      , env$demovar, "Stats:", env$var.ttest, "Type:", env$wh ))

    if (unwtd.n2 < 2) warning(paste("Error: not enough 'y' sample. Demo:"
      , env$demovar, "Stats:", env$var.ttest, "Type:", env$wh ))

    coef <- c(NA, NA, NA)
    out2 <- c(NA, NA, NA, NA, NA, NA)

    names(coef) <- c("t.value", "df", "p.value")
    names(out2) <- c("Difference", "Mean.x", "Mean.y", "Std. Err"
      , "F.value", "F-test p.value")

    out <- list("Not enough sample to do T-Test", coef, 
                out2)
    names(out) <- c("test", "coefficients", "additional")

  } else if (vx == 0 && vy == 0) {

    # Print out warning message but do not stop the ttest process
    warning(paste("Error: 'x' and 'y' are both constant. Demo:"
       , env$demovar, "Stats:", env$var.ttest, "Type:", env$wh ))


    coef <- c(NA, NA, NA)
    out2 <- c(NA, NA, NA, NA, NA, NA)

    names(coef) <- c("t.value", "df", "p.value")
    names(out2) <- c("Difference", "Mean.x", "Mean.y", "Std. Err"
      , "F.value", "F-test p.value")

    out <- list("Data is essentially constant", coef, out2)

    names(out) <- c("test", "coefficients", "additional")

  } else {

    foldedf <- max(vx, vy) / min(vx, vy)  

    if (vx > vy) {
    
    dfnum <- n - 1
    dfden <- n2 - 1

    } else {
    
    dfnum <- n2 - 1
    dfden <- n - 1

    }

    pf.F <- 2 * pf(foldedf,
                 df1 = dfnum,
                 df2 = dfden,
                 lower.tail = FALSE)
  
  #############################################################################
  # 4. t-test
  #############################################################################
  
  dif <- mx - my
  sxy <- sqrt((vx/n) + (vy/n2))

  if (pf.F <= 0.05){

    df <- (((vx/n) + (vy/n2))^2)/((((vx/n)^2)/(n - 1)) + 
                                    ((vy/n2)^2/(n2 - 1)))
    t <- (mx - my)/sxy

    p.value <- (1 - pt(abs(t), df)) * 2
    if (alternative == "greater") 
      p.value <- pt(t, df, lower.tail = FALSE)
    if (alternative == "less") 
      p.value <- pt(t, df, lower.tail = TRUE)
    
    coef <- c(t, df, p.value)
    out2 <- c(dif, mx, my, sxy, foldedf, pf.F)

    names(coef) <- c("t.value", "df", "p.value")
    names(out2) <- c("Difference", "Mean.x", "Mean.y", "Std. Err"
      , "F.value", "F-test p.value")

    out <- list("Two Sample Weighted T-Test (Welch) (unequal variances)", coef, 
                out2)
    
    names(out) <- c("test", "coefficients", "additional")
  
  } else if (pf.F > 0.05){

    df <- n + n2 - 2
    t <- (mx - my)/sxy
    p.value <- (1 - pt(abs(t), df)) * 2
    if (alternative == "greater") 
      p.value <- pt(t, df, lower.tail = FALSE)
    if (alternative == "less") 
      p.value <- pt(t, df, lower.tail = TRUE)
    
    coef <- c(t, df, p.value)
    out2 <- c(dif, mx, my, sxy, foldedf, pf.F)
    
    names(coef) <- c("t.value", "df", "p.value")
    names(out2) <- c("Difference", "Mean.x", "Mean.y", "Std. Err"
      , "F.value", "F-test p.value")
    
    out <- list("Two Sample Weighted T-Test (equal variances)", coef, 
                out2)
    
    names(out) <- c("test", "coefficients", "additional")

  }
}

out
}


wtd.t.test.se(dataset = spending
              , var = "spend"
              , wt = "weight"
              , group = "exposure"
              , alternative = "two.tailed", env = parent.frame())
  
```



## Comparing current SAS t-test vs. R t-test (Welch t-test)

### 1. When the weight sum up to n
#### SAS
```{r SAS1, eval=FALSE}
data scores;
input exposure $ spend @@ weight @@;
datalines;
1 75  0.5  1 76  0.5  1 80  2  1 77  1  1 80  0.5  1 77  0.5  1 73  2  
0 82  1  0 80  1  0 85  1  0 85  1  0 78  1  0 87  1  0 82  1  
;
run;

ods output Equality = tequal1 TTests = ttest1 Statistics=tstat1;
proc ttest data=scores;
class exposure;
var spend;
weight weight;
run;

```

exposure   | N | Mean   |Std Dev|Std Err|Minimum |Maximum 
-----------|---|--------|-------|-------|--------|-------
0          | 7 |82.7143 |3.1472 |1.1895 |78.0000 |87.0000 
1          | 7 |76.7143 |3.0667 |1.1591 |73.0000 |80.0000 
Diff (1-2) |   |6.0000  |3.1072 |1.6609 |        |


Method        |Variances |DF     |t Value|Pr 
--------------|----------|-------|-------|-------
Pooled        |Equal     |12     |3.61   |0.0036 
Satterthwaite |Unequal   |11.992 |3.61   |0.0036 


Equality of Variances 

Method  |Num DF |Den DF|F Value| Pr > F
--------|-------|------|-------|-------
Folded F| 6     |6     |1.05   | 0.9515 



#### R
```{r compare1}
spend <- c(75, 76, 80, 77, 80, 77, 73, 82, 80, 85, 85, 78, 87, 82)
weight <- c(0.5, 0.5, 2, 1, 0.5, 0.5, 2, 1, 1, 1, 1, 1, 1, 1)
exposure <- c(1, 1, 1, 1, 1, 1, 1, 0, 0, 0, 0, 0, 0, 0)

spending <- data.table::data.table(exposure = exposure, spend = spend, weight = weight)

spend.exposed <- spend[1:7]
spend.unexposed <- spend[8:14]
weight.exposed <- weight[1:7]
weight.unexposed <- weight[8:14]

weights::wtd.t.test(x = spend.exposed
                    , y = spend.unexposed
                    , weight = weight.exposed
                    , weighty = weight.unexposed
                    , mean1 = FALSE
                    , samedata = FALSE)
```



### 2. When the weight does not sum up to n
#### SAS
```{r SAS2, eval=FALSE}
data scores2;
input exposure $ spend @@ weight @@;
datalines;
1 75  2  1 76  0.5  1 80  2.5  1 77  1  1 80  0.5  1 77  0.5  1 73  2  
0 82  1  0 80  1  0 85  1  0 85  1  0 78  1  0 87  1  0 82  1  
;
run;

ods output Equality = tequal2 TTests = ttest2 Statistics=tstat2;
proc ttest data=scores2;
class exposure;
var spend;
weight weight;
run;
```

exposure  |N     |Mean   |Std Dev|Std Err|Minimum|Maximum 
--------- |------|-------|-------|-------|-------|-------
0         |7     |82.7143|3.1472 |1.1895 |78.0000|87.0000 
1         |7     |76.6111|3.3201 |1.1067 |73.0000|80.0000 
Diff (1-2)|6.1032|3.2348 |1.6302 |       |       |


Method        |Variances|DF    |t Value|Pr
--------------|---------|------|-------|-------
Pooled        |Equal    |12    |3.74   |0.0028 
Satterthwaite |Unequal  |11.938|3.76   |0.0028 


Equality of Variances 

Method  |Num DF|Den DF|F Value| Pr > F
--------|------|------|-------|-------
Folded F|6     |6     |1.11   | 0.9000 


#### R
```{r compare2}
# Generate example data
spend <- c(75, 76, 80, 77, 80, 77, 73, 82, 80, 85, 85, 78, 87, 82)
weight <- c(2, 0.5, 2.5, 1, 0.5, 0.5, 2, 1, 1, 1, 1, 1, 1, 1)
exposure <- c(1, 1, 1, 1, 1, 1, 1, 0, 0, 0, 0, 0, 0, 0)

spending <- data.table::data.table(exposure = exposure, spend = spend, weight = weight)

spend.exposed <- spend[1:7]
spend.unexposed <- spend[8:14]
weight.exposed <- weight[1:7]
weight.unexposed <- weight[8:14]

weights::wtd.t.test(x = spend.exposed
                    , y = spend.unexposed
                    , weight = weight.exposed
                    , weighty = weight.unexposed
                    , mean1 = FALSE
                    , samedata = FALSE)
```

### Conclusion:
You can see from above example that, result between R and SAS are not consistent when the weight does not sum up to n, the reason being that SAS use the below function to calcualte variances if default calculation is used

$$s^2  =\frac{\sum_{i=1}^n \left( weight_i *\left( x_i - \bar{x}\right)^2 \right)}{n - 1}$$
where $n$ is unwtd sample size



## Reference
F-test:http://www.itl.nist.gov/div898/handbook/eda/section3/eda359.htm
T-test:http://www.itl.nist.gov/div898/handbook/eda/section3/eda353.htm

Statements with the Same Function in Multiple Procedures: WEIGHT:
http://support.sas.com/documentation/cdl/en/proc/61895/HTML/default/viewer.htm#a002473731.htm
