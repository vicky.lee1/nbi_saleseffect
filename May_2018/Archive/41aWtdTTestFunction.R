# two sample weighted t test (what if variance is zero?)
wtd.t.test.se <- function (dataset, var, wt, group,
                           alternative = "two.tailed", env = parent.frame()) {

  #############################################################################
  # 1. checks
  #############################################################################
  if (is.null(dataset)) {stop("Please specify dataset")}
  if (is.null(var)) {stop("Please specify var")}
  if (is.null(wt)) {stop("Please specify weight")}
  if (is.null(group)) {stop("Please specify group")}
  if (length(unique(dataset[[group]])) != 2) {
    stop("group variable must have exactly 2 levels")}
  

  #############################################################################
  # 2. weighted sample size, mean, and variance
  #############################################################################
  wtd.stats <- dataset[
    , {
      unwtd.n = length(get(var))
      wtd.n = sum(get(wt))
      m = Hmisc::wtd.mean(get(var), get(wt), na.rm = TRUE)
      v = Hmisc::wtd.var(get(var), get(wt), na.rm = TRUE)
      
      list(
        unwtd.n = unwtd.n,
        wtd.n = wtd.n,
        m = m,
        v = v)
    }
    , by = get(group)]
  
   

   
  unwtd.n <- wtd.stats[["unwtd.n"]][[1]]
  n <- wtd.stats[["wtd.n"]][1]
  mx <- wtd.stats[["m"]][1]
  vx <- wtd.stats[["v"]][1]

  unwtd.n2 <- wtd.stats[["unwtd.n"]][[2]]
  n2 <- wtd.stats[["wtd.n"]][2]
  my <- wtd.stats[["m"]][2]
  vy <- wtd.stats[["v"]][2]


  #############################################################################
  # 3. F-test
  #############################################################################
  if ((unwtd.n < 2) | (unwtd.n2 < 2)){

    # Print out warning message but do not stop the ttest process
    if (unwtd.n < 2) warning(paste("Error: not enough 'x' sample. Demo:"
      , env$demovar, "Stats:", env$var.ttest, "Type:", env$wh ))

    if (unwtd.n2 < 2) warning(paste("Error: not enough 'y' sample. Demo:"
      , env$demovar, "Stats:", env$var.ttest, "Type:", env$wh ))

    coef <- c(NA, NA, NA)
    out2 <- c(NA, NA, NA, NA, NA, NA)

    names(coef) <- c("t.value", "df", "p.value")
    names(out2) <- c("Difference", "Mean.x", "Mean.y", "Std. Err"
      , "F.value", "F-test p.value")

    out <- list("Not enough sample to do T-Test", coef, 
                out2)
    names(out) <- c("test", "coefficients", "additional")

  } else if (vx == 0 && vy == 0) {

    # Print out warning message but do not stop the ttest process
    warning(paste("Error: 'x' and 'y' are both constant. Demo:"
       , env$demovar, "Stats:", env$var.ttest, "Type:", env$wh ))


    coef <- c(NA, NA, NA)
    out2 <- c(NA, NA, NA, NA, NA, NA)

    names(coef) <- c("t.value", "df", "p.value")
    names(out2) <- c("Difference", "Mean.x", "Mean.y", "Std. Err"
      , "F.value", "F-test p.value")

    out <- list("Data is essentially constant", coef, out2)

    names(out) <- c("test", "coefficients", "additional")

  } else {

    foldedf <- max(vx, vy) / min(vx, vy)  

    if (vx > vy) {
    
    dfnum <- n - 1
    dfden <- n2 - 1

    } else {
    
    dfnum <- n2 - 1
    dfden <- n - 1

    }

    pf.F <- 2 * pf(foldedf,
                 df1 = dfnum,
                 df2 = dfden,
                 lower.tail = FALSE)
  
  #############################################################################
  # 4. t-test
  #############################################################################
  
  dif <- mx - my
  sxy <- sqrt((vx/n) + (vy/n2))

  if (pf.F <= 0.05){

    df <- (((vx/n) + (vy/n2))^2)/((((vx/n)^2)/(n - 1)) + 
                                    ((vy/n2)^2/(n2 - 1)))
    t <- (mx - my)/sxy

    p.value <- (1 - pt(abs(t), df)) * 2
    if (alternative == "greater") 
      p.value <- pt(t, df, lower.tail = FALSE)
    if (alternative == "less") 
      p.value <- pt(t, df, lower.tail = TRUE)
    
    coef <- c(t, df, p.value)
    out2 <- c(dif, mx, my, sxy, foldedf, pf.F)

    names(coef) <- c("t.value", "df", "p.value")
    names(out2) <- c("Difference", "Mean.x", "Mean.y", "Std. Err"
      , "F.value", "F-test p.value")

    out <- list("Two Sample Weighted T-Test (Welch) (unequal variances)", coef, 
                out2)
    
    names(out) <- c("test", "coefficients", "additional")
  
  } else if (pf.F > 0.05){

    df <- n + n2 - 2
    t <- (mx - my)/sxy
    p.value <- (1 - pt(abs(t), df)) * 2
    if (alternative == "greater") 
      p.value <- pt(t, df, lower.tail = FALSE)
    if (alternative == "less") 
      p.value <- pt(t, df, lower.tail = TRUE)
    
    coef <- c(t, df, p.value)
    out2 <- c(dif, mx, my, sxy, foldedf, pf.F)
    
    names(coef) <- c("t.value", "df", "p.value")
    names(out2) <- c("Difference", "Mean.x", "Mean.y", "Std. Err"
      , "F.value", "F-test p.value")
    
    out <- list("Two Sample Weighted T-Test (equal variances)", coef, 
                out2)
    
    names(out) <- c("test", "coefficients", "additional")

  }
}

out
}
  
  