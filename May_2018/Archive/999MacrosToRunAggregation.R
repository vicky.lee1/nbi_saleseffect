
# Purpose: create macros to output data to standard template

# Input: longhorn_neu_wtd.csv
# Output: outputsummary_r.csv
# Packages: data.table
# Programmer: Yali Chen


# read in file
# load("argus_neu_wtd.RData")
# weight <- "weight_longhorn"

############################ parameters: ############################


# Function for aggregation
aggreg <- function (project,predesc,testdesc,reporttype,demo,merch,datain,
                    dataout,segment,exposure,expcount,desc,wherestate,weightvar,
                    testper,preper,Totalvar) {
  
  # For Dynamic renaming
  segment_transyes_preper  <- parse(text=paste(segment,"transyes",preper,sep="_"))
  segment_transyes_testper <- parse(text=paste(segment,"transyes",testper,sep="_"))
  
  segment_trans_preper     <- parse(text=paste(segment,"trans",preper,sep="_"))
  segment_trans_testper    <- parse(text=paste(segment,"trans",testper,sep="_"))
  
  segment_spend_preper     <- parse(text=paste(segment,"spend",preper,sep="_"))
  segment_spend_testper    <- parse(text=paste(segment,"spend",testper,sep="_"))
  
  Totalvar_spend_preper    <- parse(text=paste(Totalvar,"spend",preper,sep="_"))
  Totalvar_spend_testper   <- parse(text=paste(Totalvar,"spend",testper,sep="_"))
  
  expcount   <- parse(text=expcount)
  weightvar  <- parse(text=weightvar)
  wherestate <- parse(text=wherestate)
  
  dataout = datain[eval(wherestate),
                   {
                     Exposure                  = project                        
                     PrePeriod                 = predesc
                     TestPeriod                = testdesc
                     Type                      = reporttype
                     Demo                      = demo
                     Freq                      = desc
                     Merchant                  = merch
                     Freqmin                   = min(eval(expcount))
                     Freqmax                   = max(eval(expcount))
                     Sample                    = sum(sample)
                     MatchedSample             = sum(sample*eval(weightvar))
                     ESS                       = (sum(eval(weightvar))^2)/(sum(eval(weightvar))^2)
                     # Buyer counts
                     SpendersPre               = sum(eval(segment_transyes_preper)*eval(weightvar))
                     SpendersTest              = sum(eval(segment_transyes_testper)*eval(weightvar))
                     SpendersChange            = 100*((SpendersTest - SpendersPre) / SpendersPre)
                     
                     # Buyer Penetration
                     PenetrationPre            = 100*(SpendersPre / MatchedSample) 
                     PenetrationTest           = 100*(SpendersTest / MatchedSample)
                     PenetrationChange         = 100*((PenetrationTest - PenetrationPre) / PenetrationPre)
                     
                     # Avg Transactions per buyer
                     AvgTransactionsPre        = sum(eval(segment_trans_preper)*eval(weightvar)) / SpendersPre
                     AvgTransactionsTest       = sum(eval(segment_trans_testper)*eval(weightvar)) / SpendersTest
                     AvgTransactionsChange     = 100*((AvgTransactionsTest - AvgTransactionsPre) / AvgTransactionsPre)
                     
                     # Spending per Transactions
                     SpendPerTransactionsPre   = sum(eval(segment_spend_preper) *eval(weightvar))/sum(eval(segment_trans_preper)*eval(weightvar)) 
                     SpendPerTransactionsTest  = sum(eval(segment_spend_testper) *eval(weightvar))/sum(eval(segment_trans_testper)*eval(weightvar))
                     SpendPerTransactionsChange= 100*((SpendPerTransactionsTest - SpendPerTransactionsPre) / SpendPerTransactionsPre)
                     
                     # Avg Spending per buyer
                     AvgSpendPre               = sum(eval(segment_spend_preper)*eval(weightvar)) / SpendersPre 
                     AvgSpendTest              = sum(eval(segment_spend_testper)*eval(weightvar)) / SpendersTest 
                     AvgSpendChange            = 100*((AvgSpendTest - AvgSpendPre) / AvgSpendPre)
                     
                     # Share of Category
                     ShareCategoryPre          = 100*(sum(eval(segment_spend_preper)*eval(weightvar)) / sum(eval(Totalvar_spend_preper)*eval(weightvar)))
                     ShareCategoryTest         = 100*(sum(eval(segment_spend_testper)*eval(weightvar)) / sum(eval(Totalvar_spend_testper)*eval(weightvar)))
                     ShareCategoryChange       = 100*((ShareCategoryTest - ShareCategoryPre) / ShareCategoryPre)      
                     
                     # Total Transaction
                     TotalTransactionsPre       = sum(eval(segment_trans_preper)*eval(weightvar))
                     TotalTransactionsTest      = sum(eval(segment_trans_testper)*eval(weightvar))
                     TotalTransactionsChange    = 100*((TotalTransactionsTest - TotalTransactionsPre) / TotalTransactionsPre)
                     
                     # Total Spend
                     TotalSpendPre              = sum(eval(segment_spend_preper)*eval(weightvar))
                     TotalSpendTest             = sum(eval(segment_spend_testper)*eval(weightvar))
                     TotalSpendChange           = 100*((TotalSpendTest - TotalSpendPre) / TotalSpendPre)
                     
                     # Spend per person
                     SpendPerPersonPre          = sum(eval(segment_spend_preper)*eval(weightvar)) / MatchedSample
                     SpendPerPersonTest         = sum(eval(segment_spend_testper)*eval(weightvar)) / MatchedSample
                     SpendPerPersonChange       = 100*((SpendPerPersonTest - SpendPerPersonPre) / SpendPerPersonPre)
                     
                     list(
                       Exposure = Exposure,
                       PrePeriod  = PrePeriod ,
                       TestPeriod = TestPeriod,
                       Type = Type,
                       Demo = Demo,
                       Freq = Freq,
                       Merchant = Merchant,
                       Freqmin  = Freqmin ,
                       Freqmax  = Freqmax ,
                       Sample = Sample,
                       MatchedSample  = MatchedSample ,
                       ESS  = ESS ,
                       SpendersPre  = SpendersPre ,
                       SpendersTest = SpendersTest,
                       SpendersChange = SpendersChange,
                       PenetrationPre = PenetrationPre,
                       PenetrationTest  = PenetrationTest ,
                       PenetrationChange  = PenetrationChange , 
                       AvgTransactionsPre = AvgTransactionsPre,
                       AvgTransactionsTest  = AvgTransactionsTest ,
                       AvgTransactionsChange  = AvgTransactionsChange ,
                       SpendPerTransactionsPre  = SpendPerTransactionsPre ,
                       SpendPerTransactionsTest = SpendPerTransactionsTest,
                       SpendPerTransactionsChange = SpendPerTransactionsChange,
                       AvgSpendPre  = AvgSpendPre ,
                       AvgSpendTest = AvgSpendTest,
                       AvgSpendChange = AvgSpendChange,
                       ShareCategoryPre = ShareCategoryPre,
                       ShareCategoryTest  = ShareCategoryTest ,
                       ShareCategoryChange  = ShareCategoryChange,
                       TotalTransactionsPre = TotalTransactionsPre,
                       TotalTransactionsTest = TotalTransactionsTest,
                       TotalTransactionsChange = TotalTransactionsChange,
                       TotalSpendPre = TotalSpendPre,
                       TotalSpendTest = TotalSpendTest,
                       TotalSpendChange = TotalSpendChange,
                       SpendPerPersonPre = SpendPerPersonPre,
                       SpendPerPersonTest = SpendPerPersonTest,
                       SpendPerPersonChange = SpendPerPersonChange
                     )
                     
                   }]
  return(dataout)
  
}


# Function - GenerateOutput
GenerateOutput <- function (inputdata,anfr,antime,weight,expmin,expmax,prfr,prtime) {
  # define empty datalists to be used for rbind rows
  datalist.i <- 1 # starting from 1
  datalist = list()
  
  # Loop through exposures
  for (expnum in (expmin:expmax)){
    # Loop through the types of analyses
    for (types in 1:4){
      # Loop through demo
      for (dems in 1:length(demolst)){
        demovar  <- demolst[dems]
        demoname <- demodesclst[dems]
        #Loop through spend segments
        for (segs in 1:length(segmentlst)){
          segm <- segmentlst[segs]
          segd <- segmentdesclst[segs]
          
          typn1 <- paste0("All ",segd," Buyers")
          typn2 <- paste0("New ",segd," Buyers")
          typn3 <- paste0("Existing ",segd," Buyers")
          typn4 <- paste0("Heavy ",segd," Buyers")
          
          # Where statement
          typst1 <- "sample == 1"
          typst2 <- paste0(segm,"_transyes_",prtime," == 0")
          typst3 <- paste0(segm,"_transyes_",prtime," > 0")
          typst4 <- paste0(segm,"_spend_",prtime,"_hml"," == 3") 
          # We really need to standardize the metrics name!!!!!!!! e.g, spend vs. Spend
          
          # Overall
          outputsummary.overall<-aggreg(
            project= exposure[expnum],
            predesc= prfr,
            preper = prtime,
            testdesc= anfr,
            reporttype = eval(parse(text=paste0("typn",types))),
            datain = inputdata,
            dataout = outputsummary.overall,
            demo = demoname,           
            segment = segm,
            merch = segd,
            expcount = explist[expnum],
            desc = "Total",
            wherestate = paste("sample == 1",eval(parse(text=paste0("typst",types))), paste0(demovar," == 1"),sep = " & "),
            weightvar = weight,
            testper = antime,            
            Totalvar = totalvar)
          
          datalist[[datalist.i]] <- outputsummary.overall
          
          # Exposed vs Unexposed
          for (ex in 0:1){
            nameslst <- c("Unexposed","Exposed")
            names <- nameslst[ex+1]
            
            outputsummary.exp<-aggreg(
              project= exposure[expnum],
              predesc= prfr,
              preper = prtime,
              testdesc= anfr,
              reporttype = eval(parse(text=paste0("typn",types))),
              datain = inputdata,
              dataout = outputsummary.exp,
              demo = demoname,           
              segment = segm,
              merch = segd,
              expcount = explist[expnum],
              desc = names,
              wherestate = paste("sample == 1",paste0(expyes[expnum]," == ",ex), eval(parse(text=paste0("typst",types))), paste0(demovar," == 1"),sep = " & "),
              weightvar = weight,
              testper = antime,            
              Totalvar = totalvar)
            
            datalist[[datalist.i+ex+1]] <- outputsummary.exp
          }
          
          # Exposed HML
          for (f in 1:3){
            nameslst <- c("Low Exposure","Medium Exposure","High Exposure")
            names <- nameslst[f]
            
            outputsummary.hml<-aggreg(
              project= exposure[expnum],
              predesc= prfr,
              preper = prtime,
              testdesc= anfr,
              reporttype = eval(parse(text=paste0("typn",types))),
              datain = inputdata,
              dataout = outputsummary.hml,
              demo = demoname,           
              segment = segm,
              merch = segd,
              expcount = explist[expnum],
              desc = names,
              wherestate = paste("sample == 1",paste0(expfreq[expnum]," == ",f), eval(parse(text=paste0("typst",types))), paste0(demovar," == 1"),sep = " & "),
              weightvar = weight,
              testper = antime,            
              Totalvar = totalvar)
            
            datalist[[datalist.i+f-1+3]] <- outputsummary.hml
          }
          
          datalist.i <- datalist.i + 6 # assuming we have overall, exposed vs unexposed,exposed hml = 6 types of exposure cuts
          
          # End of spend segments loop          
        }
        # End of demo loop
      }
      # End of types of analyses loop
    }
    # End of exposures loop
  }
  # append rows and generate the output dataset
  dataout <- data.table::rbindlist(datalist)
  return(dataout)
}




############################## Reference ##############################
# This is super useful: http://brooksandrew.github.io/simpleblog/articles/advanced-data-table/
# Efficient way to append rows: http://stackoverflow.com/questions/29402528/append-data-frames-together-in-a-for-loop
# `:=` is for adding new columns to the existing columns
# This is the pass-by reference: http://stackoverflow.com/questions/10225098/understanding-exactly-when-a-data-table-is-a-reference-to-vs-a-copy-of-another


############################## Next step ##############################
# 1. Take out Totalvar in aggreg function
# 2. Construct total spend, total transaction, spend per person / spend per household metrics in aggreg function
# We construct the above 3 metrics in Excel Template right now
# We want to construct the 3 metrics in R instead
# 3. The wherestatement (or i operation) in aggreg function is not efficient, need to think of a way to setkey and use key to subset
# obstacle: how to use key to expre <, >?
# Set keys, for fast subsetting
# setkey(datain,sample, segment_transyes_prtime,segment_spend_prtime_hml,segment_ExpYes, segment_ExpFreq, demovar)
# 4. Validate the results