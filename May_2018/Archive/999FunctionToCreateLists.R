# Purpose: function to create lists
# Input  : Parameters
# Output : Lists


CreateMetricsList <- function(segmentlst,metrics,period.def){
  if (is.null(period.def) == FALSE) {
    outputlist <- vector(mode="list"
      , length = length(segmentlst) * length(metrics) * length(period.def))

    i <- 1
    for (x in seq_along(segmentlst)){
      for (y in seq_along(metrics)){
        for (z in seq_along(period.def)){
          outputlist[i] <- lapply(
            period.def[z]
            ,function(m) paste(segmentlst[x],metrics[y],m,sep="_"))
          i<- i + 1
        }
      }
    }
    return(outputlist)
  }
  
  else if (is.null(period.def) == TRUE){
    outputlist <- vector(mode="list"
      , length= length(segmentlst) * length(metrics))

    i <- 1
    for (x in seq_along(segmentlst)){
      for (y in seq_along(metrics)){
        outputlist[i] <- paste(segmentlst[x],metrics[y],sep="_")
        i<- i + 1
      }
    }
    return(outputlist)
  }
  
}


####### Concatenate strings of segment, metrics, period into one list ##########

# list of strings to be used in the createn function in script 17
segment.metrics.period     <- CreateMetricsList(
  period.def = period.agg.def
  , segmentlst = segmentlst
  , metrics = renstats)
segment.spend.period       <- CreateMetricsList(
  period.def = period.agg.def
  , segmentlst = segmentlst
  , metrics = renstats[1])
segment.trans.period       <- CreateMetricsList(
  period.def = period.agg.def
  , segmentlst = segmentlst
  , metrics = renstats[2])
segment.transyes.period    <- CreateMetricsList(
  period.def = period.agg.def
  , segmentlst = segmentlst
  , metrics = penlist)
segment.d.per.tr.period    <- CreateMetricsList(
  period.def = period.agg.def
  , segmentlst = segmentlst
  , metrics = statslst[3])
segment.share.period       <- CreateMetricsList(
  period.def = period.agg.def
  , segmentlst = segmentlst
  , metrics = statslst[4])
segment.stats.period       <- CreateMetricsList(
  period.def = period.agg.def
  , segmentlst = segmentlst
  , metrics = statslst)

totalvar.spend.period      <- CreateMetricsList(
  period.def = period.agg.def
  , segmentlst = totalvar
  , metrics = renstats[1])
segment.transyes           <- CreateMetricsList(
  period.def = NULL
  , segmentlst = segmentlst
  , metrics = penlist)

segment.metrics.period.i   <- CreateMetricsList(
  period.def = period.def
  , segmentlst = segmentlst
  , metrics = renstats)
segment.spend.period.i     <- CreateMetricsList(
  period.def = period.def
  , segmentlst = segmentlst
  , metrics = renstats[1])
segment.trans.period.i     <- CreateMetricsList(
  period.def = period.def
  , segmentlst = segmentlst
  , metrics = renstats[2])
segment.transyes.period.i  <- CreateMetricsList(
  period.def = period.def
  , segmentlst = segmentlst
  , metrics = penlist)

segment.transyes.testdiff  <- CreateMetricsList(
  period.def = paste0(period.agg.def[[2]],"diff")
  , segmentlst = segmentlst
  , metrics = penlist)
segment.transyes2.testdiff <- CreateMetricsList(
  period.def = paste0(period.agg.def[[2]],"diff")
  , segmentlst = segmentlst
  , metrics = paste0(penlist,"2"))
segment.stats.testdiff     <- CreateMetricsList(
  period.def = paste0(period.agg.def[[2]],"diff")
  , segmentlst = segmentlst
  , metrics = statslst)

# For function to remove spend outlier in 18RemoveOutliers.R
category.metrics.period     <- CreateMetricsList(
  period.def = period.agg.def
  , segmentlst = segmentlst[length(segmentlst)]
  , metrics = renstats)

log.category.metrics.period <- lapply(
  category.metrics.period
  , function(x) paste0("log_",x))

# For 20aPrepWeightingDatasets.R
segment.metrics.pre        <- CreateMetricsList(
  period.def = "pre"
  , segmentlst = segmentlst
  , metrics = renstats)

segment.metrics.pre.decile <- lapply(
  segment.metrics.pre
  , function(x) paste0("decile_",x))


