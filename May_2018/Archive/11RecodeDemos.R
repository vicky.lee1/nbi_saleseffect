# Purpose: Demo Recoding for Expanded TV, Digital, Mobile, and Print
# Source of demos: Argus & Epsilon 
#  Input:  argus
#  Output: R_Datasets\\argus.RData
# Packages required: xda

# Author: Yali Chen
# Aug, 2016

###############################################################################
# 1. Recoding Demos For Expanded TV, Digital, Mobile, and Print
###############################################################################
# https://www.r-bloggers.com/introducing-xda-r-package-for-exploratory-data-analysis/
# Note: package xda is still under development, it hasn't been put on CRAN yet.

# rm(list = ls(all = TRUE))
# options(run.main=TRUE)

################################################################################
############################# Main Function ####################################
################################################################################
{
  
  main_11 <- function(params,argus) {
    
    colnames(argus) <- tolower(colnames(argus))
    
    # Recode demo vars for rim weighting
    message("\n  Recoding Demos for Rim Weighting")
    # Get list of demos
    demos_index <- get_indexes(c(paste0(tolower(params$datatype),"demos")),params)
    demolist <- tolower(params[[demos_index]])
    for (demo in demolist) {
      #demo <- demolist[1]
      
      if (!(demo %in% colnames(argus))) {
      
        if (demo == 'age8') {
          message(paste0('  - Recoding ',demo))
          message(paste0('    No recoding instructions for this demo. Please add ',
                         'coding to 11RecodeDemos.R, under ',demo,' section'))
          message('    Stopping execution...')
          stop(paste0('No recoding instructions for this demo. Please add ',
                      'coding to 11RecodeDemos.R, under ',demo,' section'))
        } else if (demo == 'broadband') {
          message(paste0('  - Recoding ',demo))
          message(paste0('    No recoding instructions for this demo. Please add ',
                         'coding to 11RecodeDemos.R, under ',demo,' section'))
          message('    Stopping execution...')
          stop(paste0('No recoding instructions for this demo. Please add ',
                      'coding to 11RecodeDemos.R, under ',demo,' section'))
        } else if (demo == 'county_size') {
          message(paste0('  - Recoding ',demo))
          if ('nielsen_county_size_code' %in% colnames(argus)) {
            # set(argus,
            #     j     = demo,
            #     value = ifelse(argus$nielsen_county_size_code == "A", 1,
            #             ifelse(argus$nielsen_county_size_code == "B", 2,
            #             ifelse(argus$nielsen_county_size_code == "C", 3,
            #             ifelse(argus$nielsen_county_size_code == "D", 4, NA)))
            #     )
            argus[,`:=`(county_size = ifelse(argus$nielsen_county_size_code == "A", 1,
                                             ifelse(argus$nielsen_county_size_code == "B", 2,
                                                    ifelse(argus$nielsen_county_size_code == "C", 3, 
                                                           ifelse(argus$nielsen_county_size_code == "D", 4, NA)))))]
            # argus[, county_size := ifelse(argus$nielsen_county_size_code == "A", 1,
            #                        ifelse(argus$nielsen_county_size_code == "B", 2,
            #                        ifelse(argus$nielsen_county_size_code == "C", 3,
            #                        ifelse(argus$nielsen_county_size_code == "D", 4, NA)))]
            
            # Remove respondents with missing county_size
            old_n <- nrow(argus)
            argus <- argus[!is.na(county_size),]
            foo <- gc()
            message(paste0('    Removed respondents with missing county_size: ',old_n-nrow(argus)))
            
            if (params$mod_11$clean) { argus[, nielsen_county_size_code := NULL] }
          } else {
            message('    Error: nielsen_county_size_code missing in Argus dataset')
            message('    Stopping execution...')
            stop(paste0("Error: nielsen_county_size_code missing in Argus dataset"))
          }
        } else if (demo == 'dvr') {
          message(paste0('  - Recoding ',demo))
          message(paste0('    No recoding instructions for this demo. Please add ',
                         'coding to 11RecodeDemos.R, under ',demo,' section'))
          message('    Stopping execution...')
          stop(paste0('No recoding instructions for this demo. Please add ',
                      'coding to 11RecodeDemos.R, under ',demo,' section'))
        } else if (demo == 'education1') {
          message(paste0('  - Recoding ',demo))
          message(paste0('    No recoding instructions for this demo. Please add ',
                         'coding to 11RecodeDemos.R, under ',demo,' section'))
          message('    Stopping execution...')
          stop(paste0('No recoding instructions for this demo. Please add ',
                      'coding to 11RecodeDemos.R, under ',demo,' section'))
        } else if (demo == 'education2') {
          message(paste0('  - Recoding ',demo))
          message(paste0('    No recoding instructions for this demo. Please add ',
                         'coding to 11RecodeDemos.R, under ',demo,' section'))
          message('    Stopping execution...')
          stop(paste0('No recoding instructions for this demo. Please add ',
                      'coding to 11RecodeDemos.R, under ',demo,' section'))
        } else if (demo == 'gender') {
          message(paste0('  - Recoding ',demo))
          message(paste0('    No recoding instructions for this demo. Please add ',
                         'coding to 11RecodeDemos.R, under ',demo,' section'))
          message('    Stopping execution...')
          stop(paste0('No recoding instructions for this demo. Please add ',
                      'coding to 11RecodeDemos.R, under ',demo,' section'))
        } else if (demo == 'gender1') {
          message(paste0('  - Recoding ',demo))
          message(paste0('    No recoding instructions for this demo. Please add ',
                         'coding to 11RecodeDemos.R, under ',demo,' section'))
          message('    Stopping execution...')
          stop(paste0('No recoding instructions for this demo. Please add ',
                      'coding to 11RecodeDemos.R, under ',demo,' section'))
        } else if (demo == 'hhsize') {
          message(paste0('  - Recoding ',demo))
          message(paste0('    No recoding instructions for this demo. Please add ',
                         'coding to 11RecodeDemos.R, under ',demo,' section'))
          message('    Stopping execution...')
          stop(paste0('No recoding instructions for this demo. Please add ',
                      'coding to 11RecodeDemos.R, under ',demo,' section'))
        } else if (demo == 'hh_size') {
          message(paste0('  - Recoding ',demo))
          message(paste0('    No recoding instructions for this demo. Please add ',
                         'coding to 11RecodeDemos.R, under ',demo,' section'))
          message('    Stopping execution...')
          stop(paste0('No recoding instructions for this demo. Please add ',
                      'coding to 11RecodeDemos.R, under ',demo,' section'))
        } else if (demo == 'hh_size1') {
          message(paste0('  - Recoding ',demo))
          if ('num_ind_in_hh' %in% colnames(argus)) {
            argus[,`:=`(num_ind_in_hh = as.numeric(num_ind_in_hh))]
            
            # Remove respondents with hh_size > 10
            old_n <- nrow(argus)
            argus <- argus[num_ind_in_hh < 11,]
            foo <- gc()
            message(paste0('    Removed respondents with hh_size > 10: ',old_n-nrow(argus)))
            
            set(argus,
                j     = demo,
                value = ifelse(argus$num_ind_in_hh >= 5, 5, argus$num_ind_in_hh)
            )
            # argus[,`:=`(hh_size1 = ifelse(num_ind_in_hh >= 5, 5, num_ind_in_hh))]
            if (params$mod_11$clean) { argus[, num_ind_in_hh := NULL] }
          } else {
            message('    Error: num_ind_in_hh missing in Argus dataset')
            message('    Stopping execution...')
            stop(paste0("Error: num_ind_in_hh missing in Argus dataset"))
          }
        } else if (demo == 'income') {
          message(paste0('  - Recoding ',demo))
          message(paste0('    No recoding instructions for this demo. Please add ',
                         'coding to 11RecodeDemos.R, under ',demo,' section'))
          message('    Stopping execution...')
          stop(paste0('No recoding instructions for this demo. Please add ',
                      'coding to 11RecodeDemos.R, under ',demo,' section'))
        } else if (demo == 'income1') {
          message(paste0('  - Recoding ',demo))
          message(paste0('    No recoding instructions for this demo. Please add ',
                         'coding to 11RecodeDemos.R, under ',demo,' section'))
          message('    Stopping execution...')
          stop(paste0('No recoding instructions for this demo. Please add ',
                      'coding to 11RecodeDemos.R, under ',demo,' section'))
        } else if (demo == 'income2') {
          message(paste0('  - Recoding ',demo))
          message(paste0('    No recoding instructions for this demo. Please add ',
                         'coding to 11RecodeDemos.R, under ',demo,' section'))
          message('    Stopping execution...')
          stop(paste0('No recoding instructions for this demo. Please add ',
                      'coding to 11RecodeDemos.R, under ',demo,' section'))
        } else if (demo == 'kids_any') {
          message(paste0('  - Recoding ',demo))
          if ('children_counts' %in% colnames(argus)) {
            argus[,`:=`(children_counts = as.numeric(children_counts))]
            argus[,`:=`(kids_any = ifelse(children_counts > 0, 1, 2))]
            argus[is.na(argus$kids_any),`:=`(kids_any = 2)]
            if (params$mod_11$clean) { argus[, children_counts := NULL] }
          } else {
            message('    Error: children_counts missing in Argus dataset')
            message('    Stopping execution...')
            stop(paste0("Error: children_counts missing in Argus dataset"))
          }
        } else if (demo == 'kidsyesno') {
          message(paste0('  - Recoding ',demo))
          message(paste0('    No recoding instructions for this demo. Please add ',
                         'coding to 11RecodeDemos.R, under ',demo,' section'))
          message('    Stopping execution...')
          stop(paste0('No recoding instructions for this demo. Please add ',
                      'coding to 11RecodeDemos.R, under ',demo,' section'))
        } else if (demo == 'hispanic') {
          message(paste0('  - Recoding ',demo))
          if ('num_hisp' %in% colnames(argus)) {
            argus[,`:=`(num_hisp = as.numeric(num_hisp))]
            argus[,`:=`(hispanic = ifelse(num_hisp > 0, 1, 2))]
            argus[is.na(argus$hispanic),`:=`(hispanic = 2)]
            argus[,`:=`(hp = ifelse(hispanic == 1, 1, 0))]
            if (params$mod_11$clean) { argus[, num_hisp := NULL] }
          } else {
            message('    Error: num_hisp missing in Argus dataset')
            message('    Stopping execution...')
            stop(paste0("Error: num_hisp missing in Argus dataset"))
          }
        } else if (demo == 'male') {
          message(paste0('  - Recoding ',demo))
          message(paste0('    No recoding instructions for this demo. Please add ',
                         'coding to 11RecodeDemos.R, under ',demo,' section'))
          message('    Stopping execution...')
          stop(paste0('No recoding instructions for this demo. Please add ',
                      'coding to 11RecodeDemos.R, under ',demo,' section'))
        } else if (demo == 'number_of_tvs1') {
          message(paste0('  - Recoding ',demo))
          message(paste0('    No recoding instructions for this demo. Please add ',
                         'coding to 11RecodeDemos.R, under ',demo,' section'))
          message('    Stopping execution...')
          stop(paste0('No recoding instructions for this demo. Please add ',
                      'coding to 11RecodeDemos.R, under ',demo,' section'))
        } else if (demo == 'paycable') {
          message(paste0('  - Recoding ',demo))
          message(paste0('    No recoding instructions for this demo. Please add ',
                         'coding to 11RecodeDemos.R, under ',demo,' section'))
          message('    Stopping execution...')
          stop(paste0('No recoding instructions for this demo. Please add ',
                      'coding to 11RecodeDemos.R, under ',demo,' section'))
        } else if (demo == 'race1') {
          message(paste0('  - Recoding ',demo))
          message(paste0('    No recoding instructions for this demo. Please add ',
                         'coding to 11RecodeDemos.R, under ',demo,' section'))
          message('    Stopping execution...')
          stop(paste0('No recoding instructions for this demo. Please add ',
                      'coding to 11RecodeDemos.R, under ',demo,' section'))
        } else if (demo == 'race2') {
          message(paste0('  - Recoding ',demo))
          message(paste0('    No recoding instructions for this demo. Please add ',
                         'coding to 11RecodeDemos.R, under ',demo,' section'))
          message('    Stopping execution...')
          stop(paste0('No recoding instructions for this demo. Please add ',
                      'coding to 11RecodeDemos.R, under ',demo,' section'))
        } else if (demo == 'race_asian') {
          message(paste0('  - Recoding ',demo))
          if ('num_asians' %in% colnames(argus)) {
            argus[,`:=`(num_asians = as.numeric(num_asians))]
            argus[,`:=`(race_asian = ifelse(num_asians > 0 , 1, 2))]
            argus[is.na(argus$race_asian),`:=`(race_asian = 2)]
            if (params$mod_11$clean) { argus[, num_asians := NULL] }
          } else {
            message('    Error: num_asians missing in Argus dataset')
            message('    Stopping execution...')
            stop(paste0("Error: num_asians missing in Argus dataset"))
          }
        } else if (demo == 'race_afram') {
          message(paste0('  - Recoding ',demo))
          if ('num_afr_am' %in% colnames(argus)) {
            argus[,`:=`(num_afr_am = as.numeric(num_afr_am))]
            argus[,`:=`(race_afram = ifelse(num_afr_am > 0 , 1, 2))]
            argus[is.na(argus$race_afram),`:=`(race_afram = 2)]
            if (params$mod_11$clean) { argus[, num_afr_am := NULL] }
          } else {
            message('    Error: num_afr_am missing in Argus dataset')
            message('    Stopping execution...')
            stop(paste0("Error: num_afr_am missing in Argus dataset"))
          }
        } else if (demo == 'race_black') {
          message(paste0('  - Recoding ',demo))
          message(paste0('    No recoding instructions for this demo. Please add ',
                         'coding to 11RecodeDemos.R, under ',demo,' section'))
          message('    Stopping execution...')
          stop(paste0('No recoding instructions for this demo. Please add ',
                      'coding to 11RecodeDemos.R, under ',demo,' section'))
        } else if (demo == 'race_cauc') {
          message(paste0('  - Recoding ',demo))
          if ('num_cauc' %in% colnames(argus)) {
            argus[,`:=`(num_cauc = as.numeric(num_cauc))]
            argus[,`:=`(race_cauc = ifelse(num_cauc > 0 , 1, 2))]
            argus[is.na(argus$race_cauc),`:=`(race_cauc = 2)]
            if (params$mod_11$clean) { argus[, num_cauc := NULL] }
          } else {
            message('    Error: num_cauc missing in Argus dataset')
            message('    Stopping execution...')
            stop(paste0("Error: num_cauc missing in Argus dataset"))
          }
        } else if (demo == 'tv_hrs_week_hml') {
          message(paste0('  - Recoding ',demo))
          message(paste0('    No recoding instructions for this demo. Please add ',
                         'coding to 11RecodeDemos.R, under ',demo,' section'))
          message('    Stopping execution...')
          stop(paste0('No recoding instructions for this demo. Please add ',
                      'coding to 11RecodeDemos.R, under ',demo,' section'))
        } else {
          message(paste0('    No recoding instructions for ',demo,'. Please create ',
                         'new entry in 11RecodeDemos.R'))
          message('    Stopping execution...')
          stop(paste0('No recoding instructions for ',demo,'. Please create ',
                      'new entry in 11RecodeDemos.R'))
        }
        
      }
      
    }
    
    # Recode demo vars for output cuts
    message("\n  Recoding Demos for Output Cuts")
    # Get list of demos
    demolist <- tolower(params$demolst)
    for (demo in demolist) {
      #demo <- demolist[1]
      
      if (!(demo %in% colnames(argus))) {
      
        if (demo == 'sample') {
          message(paste0('  - Recoding ',demo))
          argus[,`:=`(sample = 1)]
        } else if (demo == 'asian_yes') {
          message(paste0('  - Recoding ',demo))
          if ('race_asian' %in% colnames(argus)) {
            argus[,`:=`(asian_yes = ifelse(race_asian == 1, 1, 0))]
          } else {
            message('    Error: race_asian missing in Argus dataset')
            message('    Stopping execution...')
            stop(paste0("Error: race_asian missing in Argus dataset"))
          }
        } else if (demo == 'asian_no') {
          message(paste0('  - Recoding ',demo))
          if ('race_asian' %in% colnames(argus)) {
            argus[,`:=`(asian_no = ifelse(race_asian == 2, 1, 0))]
          } else {
            message('    Error: race_asian missing in Argus dataset')
            message('    Stopping execution...')
            stop(paste0("Error: race_asian missing in Argus dataset"))
          }
        } else if (demo == 'hispanic_yes') {
          message(paste0('  - Recoding ',demo))
          if ('hispanic' %in% colnames(argus)) {
            argus[,`:=`(hispanic_yes = ifelse(hispanic == 1, 1, 0))]
          } else {
            message('    Error: hispanic missing in Argus dataset')
            message('    Stopping execution...')
            stop(paste0("Error: hispanic missing in Argus dataset"))
          }
        } else if (demo == 'hispanic_no') {
          message(paste0('  - Recoding ',demo))
          if ('hispanic' %in% colnames(argus)) {
            argus[,`:=`(hispanic_no = ifelse(hispanic == 2, 1, 0))]
          } else {
            message('    Error: hispanic missing in Argus dataset')
            message('    Stopping execution...')
            stop(paste0("Error: hispanic missing in Argus dataset"))
          }
        } else if (demo == 'cauc_yes') {
          message(paste0('  - Recoding ',demo))
          if ('race_cauc' %in% colnames(argus)) {
            argus[,`:=`(cauc_yes = ifelse(race_cauc == 1, 1, 0))]
          } else {
            message('    Error: race_cauc missing in Argus dataset')
            message('    Stopping execution...')
            stop(paste0("Error: race_cauc missing in Argus dataset"))
          }
        } else if (demo == 'cauc_no') {
          message(paste0('  - Recoding ',demo))
          if ('race_cauc' %in% colnames(argus)) {
            argus[,`:=`(cauc_no = ifelse(race_cauc == 2, 1, 0))]
          } else {
            message('    Error: race_cauc missing in Argus dataset')
            message('    Stopping execution...')
            stop(paste0("Error: race_cauc missing in Argus dataset"))
          }
        } else if (demo == 'kids_yes') {
          message(paste0('  - Recoding ',demo))
          if ('kids_any' %in% colnames(argus)) {
            argus[,`:=`(kids_yes = ifelse(kids_any == 1, 1, 0))]
          } else {
            message('    Error: kids_any missing in Argus dataset')
            message('    Stopping execution...')
            stop(paste0("Error: kids_any missing in Argus dataset"))
          }
        } else if (demo == 'kids_no') {
          message(paste0('  - Recoding ',demo))
          if ('kids_any' %in% colnames(argus)) {
            argus[,`:=`(kids_no = ifelse(kids_any == 2, 1, 0))]
          } else {
            message('    Error: kids_any missing in Argus dataset')
            message('    Stopping execution...')
            stop(paste0("Error: kids_any missing in Argus dataset"))
          }
        } else if (demo == 'gender_male') {
        } else if (demo == 'gender_female') {
        } else if (demo == 'gender_both') {
        } else if (demo == 'countysize_A') {
          message(paste0('  - Recoding ',demo))
          if ('county_size' %in% colnames(argus)) {
            argus[,`:=`(countysize_A = ifelse(county_size == 'A', 1, 0))]
          } else {
            message('    Error: county_size missing in Argus dataset')
            message('    Stopping execution...')
            stop(paste0("Error: county_size missing in Argus dataset"))
          }
        } else if (demo == 'countysize_B') {
          message(paste0('  - Recoding ',demo))
          if ('county_size' %in% colnames(argus)) {
            argus[,`:=`(countysize_B = ifelse(county_size == 'B', 1, 0))]
          } else {
            message('    Error: county_size missing in Argus dataset')
            message('    Stopping execution...')
            stop(paste0("Error: county_size missing in Argus dataset"))
          }
        } else if (demo == 'countysize_C') {
          message(paste0('  - Recoding ',demo))
          if ('county_size' %in% colnames(argus)) {
            argus[,`:=`(countysize_C = ifelse(county_size == 'C', 1, 0))]
          } else {
            message('    Error: county_size missing in Argus dataset')
            message('    Stopping execution...')
            stop(paste0("Error: county_size missing in Argus dataset"))
          }
        } else if (demo == 'countysize_D') {
          message(paste0('  - Recoding ',demo))
          if ('county_size' %in% colnames(argus)) {
            argus[,`:=`(countysize_D = ifelse(county_size == 'D', 1, 0))]
          } else {
            message('    Error: county_size missing in Argus dataset')
            message('    Stopping execution...')
            stop(paste0("Error: county_size missing in Argus dataset"))
          }
        } else if (demo == 'hhsize_1') {
          message(paste0('  - Recoding ',demo))
          if ('hh_size1' %in% colnames(argus)) {
            argus[,`:=`(hhsize_1 = ifelse(hh_size1 == 1, 1, 0))]
          } else {
            message('    Error: hh_size1 missing in Argus dataset')
            message('    Stopping execution...')
            stop(paste0("Error: hh_size1 missing in Argus dataset"))
          }
        } else if (demo == 'hhsize_2') {
          message(paste0('  - Recoding ',demo))
          if ('hh_size1' %in% colnames(argus)) {
            argus[,`:=`(hhsize_2 = ifelse(hh_size1 == 2, 1, 0))]
          } else {
            message('    Error: hh_size1 missing in Argus dataset')
            message('    Stopping execution...')
            stop(paste0("Error: hh_size1 missing in Argus dataset"))
          }
        } else if (demo == 'hhsize_3') {
          message(paste0('  - Recoding ',demo))
          if ('hh_size1' %in% colnames(argus)) {
            argus[,`:=`(hhsize_3 = ifelse(hh_size1 == 3, 1, 0))]
          } else {
            message('    Error: hh_size1 missing in Argus dataset')
            message('    Stopping execution...')
            stop(paste0("Error: hh_size1 missing in Argus dataset"))
          }
        } else if (demo == 'hhsize_4') {
          message(paste0('  - Recoding ',demo))
          if ('hh_size1' %in% colnames(argus)) {
            argus[,`:=`(hhsize_4 = ifelse(hh_size1 == 4, 1, 0))]
          } else {
            message('    Error: hh_size1 missing in Argus dataset')
            message('    Stopping execution...')
            stop(paste0("Error: hh_size1 missing in Argus dataset"))
          }
        } else if (demo == 'hhsize_5') {
          message(paste0('  - Recoding ',demo))
          if ('hh_size1' %in% colnames(argus)) {
            argus[,`:=`(hhsize_5 = ifelse(hh_size1 == 5, 1, 0))]
          } else {
            message('    Error: hh_size1 missing in Argus dataset')
            message('    Stopping execution...')
            stop(paste0("Error: hh_size1 missing in Argus dataset"))
          }
        } else if (demo == 'dma_grp1') {
          message(paste0('  - Recoding ',demo))
          if ('dma_name' %in% colnames(argus)) {
            argus[,`:=`(dma_grp1 = ifelse(dma_name %in%c('Denver', 'Los Angeles',
                                          'Minneapolis-St. Paul','New York',
                                          'Sacramento-Stockton-Modesto'), 1, 0))]
          } else {
            message('    Error: dma_name missing in Argus dataset')
            message('    Stopping execution...')
            stop(paste0("Error: dma_name missing in Argus dataset"))
          } 
        } else if (demo == 'dma_grp2') {
            message(paste0('  - Recoding ',demo))
            if ('dma_name' %in% colnames(argus)) {
              argus[,`:=`(dma_grp2 = ifelse(dma_name %in%c('Portland', 'San Antonio'), 1, 0))]
            } else {
              message('    Error: dma_name missing in Argus dataset')
              message('    Stopping execution...')
              stop(paste0("Error: dma_name missing in Argus dataset"))
            }
        } else if (demo == 'dma_grp3') {
          message(paste0('  - Recoding ',demo))
          if ('dma_name' %in% colnames(argus)) {
            argus[,`:=`(dma_grp3 = ifelse(dma_name %in%c('Austin', 
                                  'Boston (Manchester)', 'Chicago', 'Dallas-Ft. Worth',
                                  'Houston', 'Las Vegas', 'Memphis', 'Miami-Ft. Lauderdale',
                                  'Nashville', 'Omaha', 'Orlando-Daytona Beach-Melbourne',
                                  'Phoenix (Prescott)', 'San Francisco-Oakland-San Jose',
                                  'Tampa-St.Petersburg (Sarasota)'), 1, 0))]
          } else {
            message('    Error: dma_name missing in Argus dataset')
            message('    Stopping execution...')
            stop(paste0("Error: dma_name missing in Argus dataset"))
          }
        } else if (demo == 'non_sr') {
          message(paste0('  - Recoding ',demo))
          if ('sbux_rewards_prior3months' %in% colnames(argus)) {
            argus[,`:=`(non_sr = ifelse((sbux_rewards_prior3months == 0)| 
                                          (is.na(sbux_rewards_prior3months)), 1, 0))]
          } else {
            message('    Error: sbux_rewards_prior3months missing in Argus dataset')
            message('    Stopping execution...')
            stop(paste0("Error: sbux_rewards_prior3months missing in Argus dataset"))
          }
        } else if (demo == 'sr') {
          message(paste0('  - Recoding ',demo))
          if ('sbux_rewards_prior3months' %in% colnames(argus)) {
            argus[,`:=`(sr = ifelse(sbux_rewards_prior3months == 1, 1, 0))]
          } else {
            message('    Error: sbux_rewards_prior3months missing in Argus dataset')
            message('    Stopping execution...')
            stop(paste0("Error: sbux_rewards_prior3months missing in Argus dataset"))
          }
        } else if (demo == 'a25_54_hhincome_100k') {
          message(paste0('  - Recoding ',demo))
          if (sum(c('age', 'income1') %in% colnames(argus))==2 )  {
            # income1=5 corresponds to hh income >100k
            argus[,`:=`(a25_54_hhincome_100k = ifelse((age>=25 & age<=54) &  (income1==5), 1, 0))]
          } else {
            message('    Error: a25_54_hhincome_100k missing in Argus dataset')
            message('    Stopping execution...')
            stop(paste0("Error: a25_54_hhincome_100k missing in Argus dataset"))
          }
        } else if (demo == 'any_rewards_spent_30k_yes') {
          message(paste0('  - Recoding ',demo))
          if ('any_rewards_spent_30k' %in% colnames(argus)) {
            
            argus[,`:=`(any_rewards_spent_30k_yes = ifelse(any_rewards_spent_30k==1, 1, 0))]
          } else {
            message('    Error: any_rewards_spent_30k_yes missing in Argus dataset')
            message('    Stopping execution...')
            stop(paste0("Error: any_rewards_spent_30k_yes missing in Argus dataset"))
          }
        } else if (demo == 'wallettop10_yes') {
          message(paste0('  - Recoding ',demo))
          if ('wallettop10' %in% colnames(argus)) {
            
            argus[,`:=`(wallettop10_yes = ifelse(wallettop10==1, 1, 0))]
          } else {
            message('    Error: wallettop10_yes missing in Argus dataset')
            message('    Stopping execution...')
            stop(paste0("Error: wallettop10_yes missing in Argus dataset"))
          }
        } else if (demo == 'traveltop10_yes') {
          message(paste0('  - Recoding ',demo))
          if ('traveltop10' %in% colnames(argus)) {
            
            argus[,`:=`(traveltop10_yes = ifelse(traveltop10==1, 1, 0))]
          } else {
            message('    Error: traveltop10_yes missing in Argus dataset')
            message('    Stopping execution...')
            stop(paste0("Error: traveltop10_yes missing in Argus dataset"))
          }
        }
        
        else {
          message(paste0('  - Error: No recoding instructions for ',demo,'. Please create ',
                         'new entry in 11RecodeDemos.R'))
          message('    Stopping execution...')
          stop(paste0('Error: No recoding instructions for ',demo,'. Please create ',
                      'new entry in 11RecodeDemos.R'))
        }
      
      }
      
    }
    
    # Save modified argus dataset
    message(paste0('\n  Memory usage is: ',memory.size(max=FALSE),' MB'))
    message(paste0('\n  Writing ',params$datapath,'/argus_11.RData'))
    save(argus, file=paste0(params$datapath,'/argus_11.RData'))
    
    foo <- gc()
    message(paste0('\n  Memory usage is: ',memory.size(max=FALSE),' MB'))
    message(paste0('\n  Dataset has ',nrow(argus),' rows and ',ncol(argus),' columns.'))
    return(argus)
  }
  
}
################################################################################
############################## Run Main Function ###############################
################################################################################
if (getOption('run.main', default=TRUE)) {
  
  # LOAD UTILITY FUNCTIONS
  # (these are normally loaded by the main script - 10SetParameters)
  
  # Function that checks if package is installed.
  # If installed, it loads it. If it isn't, it installs it.
  pkgTest <- function(x) {
    if (!(x %in% installed.packages()[, 'Package'])) {
      tryCatch({
        message(paste0('INSTALLING PACKAGE ',toupper(x)))
        if (x == 'xda') {
          install_github("ujjwalkarn/xda")
        } else {
          install.packages(x, quiet = TRUE)
        }
      }, error = function(e) {
        stop(paste0('  ERROR: ',e))
      })
    } 
    message(paste0('LOADING PACKAGE ',toupper(x)))
    library(x,character.only = TRUE, quietly = TRUE)
  }
  
  # Get index of columns in a dataset
  get_indexes <- function(columns,dataset) {
    columns <- tolower(columns)
    indexes <- c()
    for (colname in columns) {
      indexes <- c(indexes,match(colname,tolower(names(dataset))))
    }
    return(indexes)
  }
  
  # Function to load and rename RData file, and allocate memory to it
  loadRData <- function(fileName,colalloc){
    #loads an RData file, and returns it
    load(fileName)
    alloc.col(get(ls()[ls() != "fileName"]),colalloc)
  }
  
  # Function to load and rename RData file
  loadRData_noalloc <- function(fileName) {
    #loads an RData file, and returns it
    load(fileName)
    get(ls()[ls() != "fileName"])
  }
  
  # IMPORT PACKAGES
  # (these are normally loaded by the main script - 10SetParameters)
  pkgTest('jsonlite')
  pkgTest('data.table')
  pkgTest('ff')
  pkgTest('ffbase')
  
  path <- 'E:/CapOne'
  params <- fromJSON(file.path(path,'params.json'), simplifyDataFrame=TRUE)
  argus <- loadRData(paste0(params$filepath,'.RData'),params$colalloc)
  main_11(params,argus)
  
}