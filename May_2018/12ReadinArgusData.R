# Purpose: Read in Argus data and, rename, and recoding demo
#  Input:  Argus data file (.csv)
#  Output: argus
#          R_Datasets\\argus.RData

# following V.O.'s recommendation for debugging process.

#rm(list = ls(all = TRUE))
#options(run.main=TRUE)

################################################################################
########################## Auxiliary Functions #################################
################################################################################
{
  
  # Function to rename columns in Argus dataset
  massren <- function(df, metric1, metric2, renstats, times1, times2) {
    
    for (i in 1:length(metric1)) {
      metr1 = metric1[i]
      metr2 = metric2[i]
      for (k in 1:length(renstats)){ 
        renstat = renstats[k]
        for (j in 1:length(times1)){
          time1 = times1[j]
          time2 = times2[j]
          old = paste0(metr1, "_", time1, "_", renstat)
          new = paste0(metr2, "_", renstat, "_", time2)
          # print(old)
          # print(new)
          setnames(df,old,new)
        }      
      }
    }
    return(df)
  }
  
  formatNumbers <- function(x){
    # Function to format numbers
    # Input: numbers (e.g. 20000)
    # Output: numbers in accounting format (e.g. 20,000)
    # Used in script 12ReadinArgusData.R
    formatC(x, format = "d", big.mark = ",")
  }
  
  kurtosis <- function(x, na.rm = TRUE) {
    if (na.rm) { x <- x[which(!is.na(x))] }
    x_mean <- mean(x)
    x_count <- length(x)
    s2 <- sum((x-x_mean)^2)
    s4 <- sum((x-x_mean)^4)
    m2 <- s2/x_count
    m4 <- s4/x_count
    return(((m4/m2^2-3)+3)*(1-1/x_count)^2-3)
  }
  
  
  skewness <- function(x, na.rm = TRUE) {
    if (na.rm) { x <- x[which(!is.na(x))] }
    x_mean <- mean(x)
    x_count <- length(x)
    s2 <- sum((x-x_mean)^2)
    s3 <- sum((x-x_mean)^3)
    m2 <- s2/x_count
    m3 <- s3/x_count
    return((m3/m2^(3/2))*(1-1/x_count)^(3/2))
  }
  
  mode <- function(x) {
    ux <- unique(x)
    return(ux[which.max(tabulate(match(x,ux)))])
  }
  
  topfivelevel <- function(x) {
    tbl_x <- table(x)
    topfive <- sort(tbl_x, decreasing = T)[1:ifelse(length(tbl_x) >= 5, 
                                                    yes = 5, no = length(tbl_x))]
    return(paste0(names(topfive), ':', topfive))
  }
  
  MyNumCharSummary <- function(argus) {
    
    num <- vector(mode='character')
    char <- vector(mode='character')
    for (var in 1:ncol(argus)) {
      # var = 1
      if (class(argus[[var]]) == 'numeric' || class(argus[[var]]) == 'integer') {
        num <- c(num, names(argus)[var])
      } else if (class(argus[[var]]) == 'factor' || class(argus[[var]]) == 'character') {
        char <- c(char, names(argus)[var])
      }
    }
    
    options(digits = 3)
    NumSummary <- data.table(variable = character(), n = integer(), mean = numeric()
                            ,sd = numeric(), max = numeric(), min = numeric()
                            ,range = numeric(), nunique = integer(), nzeros = integer()
                            ,iqr = numeric(), lowerbound = numeric()
                            ,upperbound = numeric(), noutlier = integer()
                            ,kurtosis = numeric(), skewness = numeric()
                            ,mode = integer(), miss = integer(), 'miss%' = numeric()
                            ,'1%' = numeric(), '5%' = numeric(), '25%' = numeric()
                            ,'50%' = numeric(), '75%' = numeric(), '95%' = numeric()
                            ,'99%' = numeric(), check.names = F)
    CharSummary <- data.table(variable = character(), n = integer(), miss = integer()
                             ,'miss%' = numeric(), unique = integer()
                             ,'top5levels:count' = factor(), check.names = F)
    
    if (length(num) != 0) {
      for (col in num) {
        n <- sum(!is.na(argus[[col]]))
        mean <- mean(argus[[col]], na.rm=T)
        sd <- sd(argus[[col]], na.rm=T)
        max <- max(argus[[col]], na.rm=T)
        min <- min(argus[[col]], na.rm=T)
        range <- max-min
        nunique <- length(unique(argus[[col]]))
        nzero <- length(which(argus[[col]] == 0))
        iqr <- IQR(argus[[col]], na.rm=T, type=4)
        lowerbound <- quantile(argus[[col]], 0.25, na.rm = T) - (1.5*iqr)
        upperbound <- quantile(argus[[col]], 0.75, na.rm = T) + (1.5*iqr)
        noofoutliers <- length(which((argus[[col]] > upperbound) | 
                                     (argus[[col]] < lowerbound)))
        kurtosis_val <- kurtosis(argus[[col]])
        skewness_val <- skewness(argus[[col]])
        mode <- mode(argus[[col]])
        miss <- sum(is.na(argus[[col]]))
        missPer <- (miss/length(argus[[col]]))*100
        q <- quantile(argus[[col]], c(0.01, 0.05, 0.25, 0.5, 0.75, 0.95, 0.99),
                      na.rm=T)
        d2 <- list(col, n, mean, sd, max, min, range, nunique, nzero, iqr,
                   lowerbound, upperbound, noofoutliers, kurtosis_val,
                   skewness_val, mode, miss, missPer, q[['1%']], q[['5%']],
                   q[['25%']], q[['50%']], q[['75%']], q[['95%']], q[['99%']])
        NumSummary <- rbindlist(list(NumSummary, d2))
      }
    }
    
    if (length(char) != 0) {
      for (col in char) {
        # col = char[1]
        n <- sum(!is.na(argus[[col]]))
        miss <- sum(is.na(argus[[col]]))
        perc <- (miss/length(argus[[col]]))*100
        unique <- length(unique(argus[[col]]))
        unique_val <- paste0(topfivelevel(argus[[col]]), collapse = ', ')
        d2 <- list(col, n, miss, perc, unique, unique_val)
        CharSummary <- rbindlist(list(CharSummary,d2))
      } 
    }
    
    return(list('NumSummary' = NumSummary, 'CharSummary' = CharSummary))
  }
  
  # Function to validate "comparison" dataset against "model" dataset.
  validate_12 <- function(params,filename_list,key_column_list) {
    
    path <- params$path
    dump_diff_file <- params$dump_diff_file
    
    if (!file.exists(paste0(path,'/Validation'))) { 
      dir.create(paste0(path,'/Validation')) 
    }
    
    for (j in 1:length(filename_list)) {
      #j<-1
      ModelFileName <- filename_list[[j]][1]
      ModelFileType <- substr(ModelFileName,regexpr('.',ModelFileName,fixed=TRUE)+1
                              ,nchar(ModelFileName))
      
      if (file.exists(ModelFileName)) {
        
        key_column <- key_column_list[[j]]
        
        # Find the matching file in the comparison folder
        ComparisonFileName <- filename_list[[j]][2]
        ComparisonFileType <- substr(ComparisonFileName,regexpr('.',ComparisonFileName,fixed=TRUE)+1
                                     ,nchar(ComparisonFileName))
        
        message(paste0('\n  Validating ',ComparisonFileName))
        message(paste0('  against ',ModelFileName))
        
        # Output folder and file names
        outfolder <- paste0(path,'/Validation')
        filename <- substr(ModelFileName,1,regexpr('.',ModelFileName,fixed=TRUE)-1)
        char_pos <- gregexpr('/',filename,fixed=TRUE)[[1]]
        filename <- substr(filename,char_pos[length(char_pos)]+1,nchar(filename))
        filename <- paste0('/diff_',filename)
        outname <- base::paste0(outfolder,filename)
        
        # Upload files
        if (ModelFileType == 'sas7bdat') {
          Modelfile <- haven::read_sas(ModelFileName)
        } else if (ModelFileType == 'csv') {
          Modelfile <- read.csv(file=ModelFileName)
        } else if (ModelFileType == 'RData') {
          Modelfile <- loadRData_noalloc(ModelFileName)
        } else if (ModelFileType == 'xlsx') {
          message('  - Please, add capability to read xlsx files in validate_17 function.')
        }
        if (!('data.table' %in% class(Modelfile))) {
          Modelfile <- as.data.table(Modelfile)
        }
        Modelfile[,key_column] <- rownames(Modelfile)
        colnames(Modelfile) <- base::tolower(base::colnames(Modelfile))
        
        if (nrow(Modelfile) == 0) {
          
          if (ComparisonFileType == 'sas7bdat') {
            Comparisonfile <- haven::read_sas(ComparisonFileName)
          } else if (ComparisonFileType == 'csv') {
            Comparisonfile <- read.csv(file=ComparisonFileName)
          } else if (ComparisonFileType == 'RData') {
            Comparisonfile <- loadRData(ComparisonFileName,params$colalloc)
          } else if (ComparisonFileType == 'xlsx') {
            message('  - Please, add capability to read xlsx files.')
          }
          Comparisonfile[,key_column] <- rownames(Comparisonfile)
          colnames(Comparisonfile) <- base::tolower(base::colnames(Comparisonfile))
          
          if (nrow(Comparisonfile) == 0) {
            
            message('  - No discrepancies were found.')
            
          } else {
            
            message('  - Discrepancies were found.')
            message(paste0('    Dimensions of model dataset were ',
                           toString(nrow(Modelfile)),' x ',
                           toString(ncol(Modelfile))))
            message(paste0('    while dimensions of comparison dataset were ',
                           toString(nrow(Comparisonfile)),' x ',
                           toString(ncol(Comparisonfile))))
            
          }
          
        } else {
          
          if (ComparisonFileType == 'sas7bdat') {
            Comparisonfile <- haven::read_sas(ComparisonFileName)
          } else if (ComparisonFileType == 'csv') {
            Comparisonfile <- read.csv(file=ComparisonFileName)
          } else if (ComparisonFileType == 'RData') {
            Comparisonfile <- loadRData(ComparisonFileName,params$colalloc)
          } else if (ComparisonFileType == 'xlsx') {
            message('  - Please, add capability to read xlsx files.')
          }
          if (!('data.table' %in% class(Comparisonfile))) {
            Comparisonfile <- as.data.table(Comparisonfile)
          }
          Comparisonfile[,key_column] <- rownames(Comparisonfile)
          colnames(Comparisonfile) <- base::tolower(base::colnames(Comparisonfile))
          
          if (nrow(Comparisonfile) == 0) {
            
            message('  - Discrepancies were found.')
            message(paste0('    Dimensions of model dataset were ',
                           toString(nrow(Modelfile)),' x ',
                           toString(ncol(Modelfile))))
            message(paste0('    while dimensions of comparison dataset were ',
                           toString(nrow(Comparisonfile)),' x ',
                           toString(ncol(Comparisonfile))))
            
          } else {
            
            if (length(intersect(colnames(Modelfile),colnames(Comparisonfile))) == 
                length(colnames(Modelfile))) {
              Comparisonfile <- Comparisonfile[,names(Modelfile),with=F]
              foo <- gc()
            }
            
            
            for (icol in colnames(Comparisonfile)) {
              #icol = colnames(Comparisonfile)[15]
              
              if( class(Comparisonfile[[icol]]) == "integer"){
                set(Modelfile, j = icol,
                    value = as.integer(gsub(',','',Modelfile[[icol]])))
              }else if(class(Comparisonfile[[icol]]) == "character"){
                set(Modelfile, j = icol,
                    value = as.character(Modelfile[[icol]]))
              }else if(class(Comparisonfile[[icol]]) == "numeric"){
                set(Modelfile, j = icol,
                    value = as.numeric(Modelfile[[icol]]))
              }
              
            }
            # Compare files
            validate_dt_file(Modelfile,Comparisonfile,key_column,outname,
                             dump_diff_file)
            
          }
          
        }
        
      } else {
        
        message(paste0('  - ',ModelFileName,' does not exist.'))
        
      }
      
    }
    
  }
  
}
################################################################################
############################# Main Function ####################################
################################################################################
{
  
  main_12<- function(params,argus) {
    
    message(paste0('\n  Memory usage is: ',memory.size(max=FALSE),' MB'))
    
    # Rename columns in argus dataset
    message('\n  Renaming columns.')
    metric1 <- tolower(params$metric1)
    metric2 <- tolower(params$metric2)
    renstats <- tolower(params$renstats)
    times1 <- tolower(params$times1)
    times2 <- tolower(params$times2)
    argus <- massren(df=argus, metric1, metric2, renstats, times1, times2)
    
    ###############################################################################
    # Check items:
    # 1. Check if variables exist: demo, exposure
    # 2. Sample Size: toal sample size, exposed sample size, unexposed sample size
    # 3. Check if spending time periods are mapped correctly (Do we need this?)
    # 4. Check the distribution of exposure variables
    # 5. Do we need to add in summary table for spending metrics
    ###############################################################################
    
    message("\n  PreChecks")
    
    # List of exposures
    explist <- tolower(params$explist)
    
    # List of demos
    demos_index <- get_indexes(c(paste0(tolower(params$datatype),"demos")),params)
    demolist <- tolower(params[[demos_index]])
    
    # List of spending variables
    period.def <- lapply(params$period.def,tolower)
    segment.metrics.period.i <- CreateMetricsList(period.def = period.def, 
                                                  segmentlst = tolower(params$segmentlst),
                                                  metrics = renstats)
    
    # Max exposure frequency 
    demos_index <- get_indexes(c(paste0(tolower(params$datatype),"Max")),params)
    expfreqMax <- tolower(params[[demos_index]])
    
    # 1. Check if variables exist: demo, exposure
    message("  - Exposure, Demo, Spending Variables")
    vars.if.exist <- c(explist, demolist, unlist(segment.metrics.period.i))
    
    if (sum(vars.if.exist %in% colnames(argus)) == length(vars.if.exist)) {
      message("    None of the variables are missing.")
    } else {
      invisible(lapply(vars.if.exist
                       , function(x) if (!(x %in% colnames(argus))) { 
                         message(paste0("    Error: Variable \""
                                        , x
                                        , "\" does not exist in the datafile. Please check with Argus."))
                       }
      ))
    }
    
    # 2. Sample Size: total sample size, exposed sample size, unexposed sample size
    message(paste0("  - Sample Size: " , formatNumbers(nrow(argus))))
    for (i in seq_along(explist)) {
      #i<-1
      count_exp   <- sum(argus[[explist[i]]] > 0, na.rm=T)
      count_unexp <- sum(argus[[explist[i]]] == 0, na.rm=T)
      message(paste0("    ", explist[i], ": Exposed Sample Size: "
                     , formatNumbers(count_exp)
                     , ". Unexposed Sample Size: "
                     , formatNumbers(count_unexp)))
    }
    
    # 3. Check if spending time periods are mapped correctly (Do we need this?)
    message("  - Time periods")
    invisible(lapply(seq_along(times1)
                     , function(x) message(paste0("    ", times1[x], " is renamed to ", times2[x]))))
    
    # 4. Check the distribution of exposure variables
    message("  - Distribution of exposure variables")
    # Send warnings when max exp is too high
    invisible(lapply(explist,
                     function(x) 
                       if (max(argus[[x]],na.rm=T) > expfreqMax) {
                         message(paste0("    Warning: Max exposure frequency for ",
                                        x, " is ", max(argus[[x]],na.rm=T), ", which is",
                                        " higher than we normally see. Please ",
                                        "double check the exposure frequency."))
                       } else {
                         message(paste0("    Exposure frequency looks normal for ",
                                        x,"."))
                       } ))
    
    # Get distribution of exposure variables
    quantile.label = c("100%", "99%", "95%", "90%", "75%", "50%"
                              , "25%", "10%", "5%", "1%", "0%")
    quantile.probs = c(1, 0.99, 0.95, 0.90, 0.75, 0.5, 0.25, 0.1, 0.05, 0.01, 0)
    for (i in seq_along(explist)) {
      #i=1
      expo <- argus[,explist[i],with=F]
      expo <- expo[expo[[explist[i]]] > 0]
      exp_quant <- data.frame(quantile = quantile.probs,
                              exposurefrequency = quantile(expo[[explist[i]]]
                                       , probs = quantile.probs)
                             )
      names(exp_quant)[2] <- explist[i] 
      if (i==1) {
        exp.quantile.table <- exp_quant
      } else {
        exp.quantile.table <- merge(exp.quantile.table,exp_quant,by='quantile',all=T)
      }
    }
    rm(expo)
    rm(exp_quant)
    exp.quantile.table <- exp.quantile.table[order(-exp.quantile.table$quantile),]
    exp.quantile.table$quantile <- quantile.label
    
    # exp.quantile.table <- as.data.frame(lapply(setNames(explist, explist)
    #                                     , function(x) data.table(
    #                                       quantile = quantile.label
    #                                     , exposurefrequency = quantile(exposures[exposures[[x]]>0][[x]]
    #                                                                  , probs = quantile.probs)
    #                                     )))
    
    names(exp.quantile.table) <- c('Quantile',params$explist)
    message(paste0('    Writing distribution of exposure variables to ',
                   params$outputpath,'/exp_var_dist.csv'))
    write.csv(exp.quantile.table, file = paste0(params$outputpath,
              '/exp_var_dist.csv'), quote = TRUE, eol = "\n", 
              na = "NA", row.names = FALSE, fileEncoding = "")
    rm(exp.quantile.table)
    
    # Check missing values for both char and numeric vars
    # We continue to evaluate the data quality of demo variables
    message("  - Check missing data in demo variables")
    
    result <- MyNumCharSummary(argus)
    num_summary <- result$NumSummary
    char_summary <- result$CharSummary
    rm(result)
    
    #num_summary <- numSummary(argus)#[,demolist,with=FALSE])
    #num_summary$variable <- rownames(num_summary)
    message(paste0('    Writing output from numSummary to ',params$outputpath,
                   '/numSummary.csv'))
    write.csv(num_summary, file = paste0(params$outputpath,'/numSummary.csv'),
              append = F, quote = T, eol = "\n", na = "NA",
              row.names = F, fileEncoding = "")
    
    #char_summary2 <- charSummary(argus)#[,demolist,with=FALSE])
    #char_summary$variable <- rownames(char_summary)
    message(paste0('    Writing output from charSummary to ',params$outputpath,
                   '/charSummary.csv'))
    write.csv(char_summary, file = paste0(params$outputpath,'/charSummary.csv'),
              append = F, quote = T, eol = "\n", na = "NA",
              row.names = F, fileEncoding = "")
    
    miss_data <- rbind(num_summary[num_summary$miss > 0,c('variable','miss','miss%')],
                       char_summary[char_summary$miss > 0,c('variable','miss','miss%')])
    rm(num_summary)
    rm(char_summary)
    foo <- gc()
    
    if (nrow(miss_data) > 0) {
      message('    Variables with missing data:')
      message('      VARIABLE                MISS   MISS%')
      for (irow in 1:nrow(miss_data)) {
        #irow <- 1
        message(paste0('      ',format(miss_data$variable[[irow]],width=20),
                       format(miss_data$miss[[irow]],width=8),
                       format(miss_data$`miss%`[[irow]],width=8)))
      }  
    }
    rm(miss_data)
    
    # Save argus as RData
    message(paste0('\n  Memory usage is: ',memory.size(max=FALSE),' MB'))
    message(paste0('\n  Writing ',params$datapath,"/argus_12.RData"))
    argus_recoded <- argus
    rm(argus)
    foo <- gc()
    save(argus_recoded, file=paste0(params$datapath,"/argus_12.RData"))
    
    # Validate output
    if (params$mod_12$val) {
      
      # message(paste0('\n  Writing ',params$datapath,'/argus_',params$datatype,'.RData'))
      # save(argus_recoded, file=paste0(params$datapath,'/argus_',params$datatype,'.RData'))
      rm(argus_recoded)
      foo <- gc()
      
      # The list of names of files to compare, in pairs (first is always the 
      # model file, and second is always the comparison file)
      filename_list <- list(c(paste0(params$val_dir,'/SAS_Datasets/argus_',
                                     tolower(params$datatype),'.',params$val_format),
                              paste0(params$datapath,'/argus_12.RData'))
                           )
      
      # For each filename in filename_list, provide the name of the column(s) that
      # identify each unique row
      # Ex. key_column_list <- list('respondentid',
      #                             c('hhid','personid'))
      key_column_list <- list('respondentid')
      
      validate_12(params,filename_list,key_column_list)
      
      argus_recoded <- loadRData(paste0(params$datapath,'/argus_12.RData'),
                                 params$colalloc)
      
    }
    
    message(paste0('\n  Memory usage is: ',memory.size(max=FALSE),' MB'))
    message(paste0('\n  Dataset has ',nrow(argus_recoded),' rows and ',ncol(argus_recoded),' columns.'))
    return(argus_recoded)
  }
  
}
################################################################################
############################## Run Main Function ###############################
################################################################################
if (getOption('run.main', default=TRUE)) {
  
  # IMPORT UTILITY FUNCTIONS
  
  # Function that checks if package is installed.
  # If installed, it loads it. If it isn't, it installs it.
  pkgTest <- function(x) {
    if (!(x %in% installed.packages()[, 'Package'])) {
      tryCatch({
        message(paste0('INSTALLING PACKAGE ',toupper(x)))
        if (x == 'xda') {
          install_github("ujjwalkarn/xda")
        } else {
          install.packages(x, quiet = TRUE)
        }
      }, error = function(e) {
        stop(paste0('  ERROR: ',e))
      })
    } 
    message(paste0('LOADING PACKAGE ',toupper(x)))
    library(x,character.only = TRUE, quietly = TRUE)
  }
  
  # Get index of columns in a dataset
  get_indexes <- function(columns,dataset) {
    columns <- tolower(columns)
    indexes <- c()
    for (colname in columns) {
      indexes <- c(indexes,match(colname,tolower(names(dataset))))
    }
    return(indexes)
  }
  
  # Function to format list to write to file
  write_list <- function(prefix,list,indent,total_length) {
    llist <- length(list)
    cond <- 1
    count <- 1
    while (cond) {
      ltoprint <- paste(list[1:count],collapse=', ')
      line <- paste(prefix,ltoprint)
      lline <- nchar(line)
      if (lline < total_length) {
        if (count < llist) {
          count <- count+1
        } else {
          cond <- 0
        }
      } else {
        cond <- 0
        count <- count-1
        if (count < 1) {
          line <- prefix
        } else {
          ltoprint <- paste(list[1:count],collapse=', ')
          line <- paste(prefix,ltoprint)
        }
      }
    }
    message(line)
    
    prefix <- paste(rep(" ",indent),collapse="")
    
    while (count < llist) {
      cond <- 1
      count <- count+1
      count2 <- count
      while (cond) {
        ltoprint <- paste(list[count2:count],collapse=', ')
        line <- paste0(prefix,ltoprint)
        lline <- nchar(line)
        if (lline < total_length) {
          if (count < llist) {
            count <- count+1
          } else {
            cond <- 0
          }
        } else {
          cond <- 0
          count <- count-1
          if (count < count2) {
            count <- count+1
          } else {
            ltoprint <- paste(list[count2:count],collapse=', ')
            line <- paste0(prefix,ltoprint)
          }
        }
      }
      message(line)
    }
  }
  
  # This function returns TRUE wherever elements are different, including NA's,
  # and FALSE everywhere else.
  compareNA <- function(v1,v2) {
    same <- (v1 == v2) | (is.na(v1) & is.na(v2))
    same[is.na(same)] <- FALSE
    return(!same)
  }
  
  # Function to compare two data.table objects and write differences to file
  validate_dt_file <- function(Modelfile,Comparisonfile,key_column,outname,
                               dump_diff_file) {
    
    # First test: Check that both files have the same number of columns and rows
    
    ncolSAS <- base::ncol(Modelfile)
    nrowSAS <- base::nrow(Modelfile)
    ncolCSV <- base::ncol(Comparisonfile)
    nrowCSV <- base::nrow(Comparisonfile)
    
    if ((ncolSAS == ncolCSV) && (nrowSAS == nrowCSV)) { 
      
      # Second test: Check that columns in both files have the same names
      #              (case can be different (uppercase vs. lowercase) and order
      #               of the columns can be different, but names have to be the same)
      
      SAScolnames <- base::sort(base::tolower(base::colnames(Modelfile)))
      CSVcolnames <- base::sort(base::tolower(base::colnames(Comparisonfile)))
      ndiff <- 0
      for (icol in 1:base::length(SAScolnames)) {
        if (SAScolnames[icol] != CSVcolnames[icol]) { ndiff <- ndiff+1 }
      }
      
      if (ndiff == 0) {
        
        # Third test: Compare files column-by-column
        
        # Rearrange Rfile columns to be in the same order as Modelfile
        base::names(Modelfile) <- base::tolower(base::names(Modelfile))
        base::names(Comparisonfile) <- base::tolower(base::names(Comparisonfile))
        if (ncolSAS > 1) {
          Comparisonfile <- Comparisonfile[,names(Modelfile),with=F]
        }
        
        # Sort datasets according to key_column to have rows in the same order
        indexes <- get_indexes(key_column,Modelfile)
        key_names <- base::names(Modelfile)[indexes]
        if (nrowSAS > 1) {
          Modelfile <- setorderv(Modelfile,key_names)
          Comparisonfile <- setorderv(Comparisonfile,key_names)
        }
        
        # Use "compare" function to validate each column in the file against its 
        # SAS counterpart
        count <- 0
        for (i in 1:ncolSAS) {
          #i=15
          comparison <- compare::compare(Modelfile[[i]], Comparisonfile[[i]]
                                         ,equal = TRUE
                                         ,coerce = TRUE
                                         #,shorten = TRUE
                                         #,ignoreOrder = TRUE
                                         #,ignoreNameCase = TRUE
                                         #,ignoreNames = TRUE
                                         ,ignoreAttrs = TRUE
                                         ,round = TRUE
                                         #,ignoreCase = TRUE
                                         #,trim = TRUE
                                         #,dropLevels = TRUE
                                         #,ignoreLevelOrder = TRUE
                                         #,ignoreDimOrder = TRUE
                                         #,ignoreColOrder = TRUE
                                         #,ignoreComponentOrder = TRUE
                                         #,colsOnly = TRUE
                                         #,allowAll = TRUE
          )
          
          # If discrepancies are found, write discrepancies to file for evaluation
          # by the user
          if (!comparison$result) {
            count <- count+1
            base::message(paste0('  - Found discrepancies in column: ',
                                 base::names(Modelfile)[i]))
            if (dump_diff_file) {
              SAS_np <- Modelfile[compareNA(comparison$tM[], comparison$tC[]),c(indexes,i),with=F]
              
              CSV_np <- Comparisonfile[compareNA(comparison$tM[], comparison$tC[]),c(indexes,i),with=F]
              
              base::names(SAS_np) <- base::paste0('Model_',base::names(SAS_np))
              base::names(CSV_np) <- base::paste0('Comparison_',base::names(CSV_np))
              for (j in 1:base::length(key_names)) {
                base::names(CSV_np)[base::names(CSV_np) == base::paste0('Comparison_',key_names[j])] <- key_names[j]
                base::names(SAS_np)[base::names(SAS_np) == base::paste0('Model_',key_names[j])] <- key_names[j]
              }
              
              diff <- base::merge(SAS_np, CSV_np, by=key_names, all=TRUE)
              base::message(paste0('    ',nrow(diff),' discrepancies found.'))
              model_name <- paste0('Model_',names(Modelfile)[i])
              comparison_name <- paste0('Comparison_',names(Modelfile)[i])
              if (class(diff[[model_name]]) == 'numeric') {
                diff$`%Diff` <- round((diff[,comparison_name,with=F]-diff[,model_name,with=F])/
                                        diff[,model_name,with=F]*100,0)
              }
              
              if (count == 1) {
                xlsx::write.xlsx(diff,paste0(outname,'.xlsx'),row.names=FALSE
                                 ,sheetName = base::names(Modelfile)[i])
              } else {
                xlsx::write.xlsx(diff,paste0(outname,'.xlsx'),row.names=FALSE
                                 ,sheetName = base::names(Modelfile)[i],append = TRUE)
              }
              
              jgc()
            }
          }
        }
        
        if (count == 0) {
          base::message('  - No discrepancies were found.')
        } else {
          if (dump_diff_file) {
            base::message(paste0('  - Report saved in ',outname,'.xlsx'))
            # Sys.setenv(RSTUDIO_PANDOC="C:/Program Files/RStudio/bin/pandoc")
            # render(paste0(params$code_loc,"/gen_val_report.Rmd"),#'all',
            #        output_file = paste0(outname,'.html'),
            #        params = list(path = params$path, outname = outname),
            #        quiet = T)
          }
        }
        
        
      } else {
        
        base::message('  - Discrepancies were found.')
        base::message('    Column names in model and comparison datasets do not match.')
        write_list('    Column names in model dataset are:', SAScolnames, 
                   6,80)
        write_list('    Column names in comparison dataset are:', CSVcolnames, 
                   6,80)
        
      }
      
    } else {
      
      base::message('  - Discrepancies were found.')
      base::message(base::paste0('    The model dataset has ',base::toString(nrowSAS)
                                 ,' rows and ',base::toString(ncolSAS),' columns.'))
      base::message(base::paste0('    while the comparison dataset has ',base::toString(nrowCSV)
                                 ,' rows and ',base::toString(ncolCSV),' columns.'))
    }
    
  }
  
  # Function to collect Java garbage when using xlsx package
  jgc <- function() {
    base::gc()
    .jcall("java/lang/System", method = "gc")
  }
  
  # Function to load and rename RData file, and allocate memory to it
  loadRData <- function(fileName,colalloc){
    #loads an RData file, and returns it
    load(fileName)
    alloc.col(get(ls()[ls() != "fileName"]),colalloc)
  }
  
  # Function to load and rename RData file
  loadRData_noalloc <- function(fileName) {
    #loads an RData file, and returns it
    load(fileName)
    get(ls()[ls() != "fileName"])
  }
  
  # Function to create list of metrics
  CreateMetricsList <- function(segmentlst,metrics,period.def) {
    if (is.null(period.def) == FALSE) {
      outputlist <- vector(mode="list"
                           , length = length(segmentlst) * length(metrics) * length(period.def))
      
      i <- 1
      for (x in seq_along(segmentlst)){
        for (y in seq_along(metrics)){
          for (z in seq_along(period.def)){
            outputlist[i] <- lapply(
              period.def[z]
              ,function(m) paste(segmentlst[x],metrics[y],m,sep="_"))
            i<- i + 1
          }
        }
      }
      return(outputlist)
    }
    
    else if (is.null(period.def) == TRUE){
      outputlist <- vector(mode="list"
                           , length= length(segmentlst) * length(metrics))
      
      i <- 1
      for (x in seq_along(segmentlst)){
        for (y in seq_along(metrics)){
          outputlist[i] <- paste(segmentlst[x],metrics[y],sep="_")
          i<- i + 1
        }
      }
      return(outputlist)
    }
    
  }
  
  # IMPORT PACKAGES
  # (these are normally loaded by the main script - 10SetParameters)
  pkgTest('haven')
  options(java.parameters = "-Xmx16000m")
  pkgTest('rJava')
  pkgTest('xlsx')
  pkgTest('compare')
  pkgTest('data.table')
  pkgTest('jsonlite')
  pkgTest('xda')
  pkgTest('rmarkdown')
  pkgTest('kableExtra')
  pkgTest('plyr')
  
  path <- 'E:/SnapChat_sbux'
  params <- fromJSON(file.path(path,'params.json'), simplifyDataFrame=TRUE)
  argus <- loadRData(paste0(params$datapath,'/argus_11.RData'),params$colalloc)
  main_12(params,argus)

}