# Purpose: Demo Recoding for Expanded TV, Digital, Mobile, and Print
# Source of demos: Argus & Epsilon 
#  Input:  argus
#  Output: R_Datasets\\argus.RData
# Packages required: xda

# Author: Yali Chen
# Aug, 2016

###############################################################################
# 1. Recoding Demos For Expanded TV, Digital, Mobile, and Print
###############################################################################
# https://www.r-bloggers.com/introducing-xda-r-package-for-exploratory-data-analysis/
# Note: package xda is still under development, it hasn't been put on CRAN yet.

# rm(list = ls(all = TRUE))
# options(run.main=TRUE)

################################################################################
############################# Main Function ####################################
################################################################################
{
  
  main_11 <- function(params,argus) {
    
    colnames(argus) <- tolower(colnames(argus))
    
    # Recode demo vars for rim weighting
    message("\n  Recoding Demos for Rim Weighting")
    # Get list of demos
    
    # 
    demos_index <- get_indexes(c(paste0(tolower(params$datatype),"demos")),params)
    demolist <- tolower(params[[demos_index]])
    # 
    for (demo in demolist) {
      #demo <- demolist[1]
      
      if (!(demo %in% colnames(argus))) {
      
        if (demo == 'age8') {
          message(paste0('  - Recoding ',demo))
          message(paste0('    No recoding instructions for this demo. Please add ',
                         'coding to 11RecodeDemos.R, under ',demo,' section'))
          message('    Stopping execution...')
          stop(paste0('No recoding instructions for this demo. Please add ',
                      'coding to 11RecodeDemos.R, under ',demo,' section'))
        } else if (demo == 'broadband') {
          message(paste0('  - Recoding ',demo))
          message(paste0('    No recoding instructions for this demo. Please add ',
                         'coding to 11RecodeDemos.R, under ',demo,' section'))
          message('    Stopping execution...')
          stop(paste0('No recoding instructions for this demo. Please add ',
                      'coding to 11RecodeDemos.R, under ',demo,' section'))
        } else if (demo == 'county_size') {
          message(paste0('  - Recoding ',demo))
          if ('nielsen_county_size_code' %in% colnames(argus)) {
            # set(argus,
            #     j     = demo,
            #     value = ifelse(argus$nielsen_county_size_code == "A", 1,
            #             ifelse(argus$nielsen_county_size_code == "B", 2,
            #             ifelse(argus$nielsen_county_size_code == "C", 3,
            #             ifelse(argus$nielsen_county_size_code == "D", 4, NA)))
            #     )
            argus[,`:=`(county_size = ifelse(argus$nielsen_county_size_code == "A", 1,
                                             ifelse(argus$nielsen_county_size_code == "B", 2,
                                                    ifelse(argus$nielsen_county_size_code == "C", 3, 
                                                           ifelse(argus$nielsen_county_size_code == "D", 4, NA)))))]
            # argus[, county_size := ifelse(argus$nielsen_county_size_code == "A", 1,
            #                        ifelse(argus$nielsen_county_size_code == "B", 2,
            #                        ifelse(argus$nielsen_county_size_code == "C", 3,
            #                        ifelse(argus$nielsen_county_size_code == "D", 4, NA)))]
            
            # Remove respondents with missing county_size
            old_n <- nrow(argus)
            argus <- argus[!is.na(county_size),]
            foo <- gc()
            message(paste0('    Removed respondents with missing county_size: ',old_n-nrow(argus)))
            
            if (params$mod_11$clean) { argus[, nielsen_county_size_code := NULL] }
          } else {
            message('    Error: nielsen_county_size_code missing in Argus dataset')
            message('    Stopping execution...')
            stop(paste0("Error: nielsen_county_size_code missing in Argus dataset"))
          }
        } else if (demo == 'dvr') {
          message(paste0('  - Recoding ',demo))
          message(paste0('    No recoding instructions for this demo. Please add ',
                         'coding to 11RecodeDemos.R, under ',demo,' section'))
          message('    Stopping execution...')
          stop(paste0('No recoding instructions for this demo. Please add ',
                      'coding to 11RecodeDemos.R, under ',demo,' section'))
        } else if (demo == 'education1') {
          message(paste0('  - Recoding ',demo))
          message(paste0('    No recoding instructions for this demo. Please add ',
                         'coding to 11RecodeDemos.R, under ',demo,' section'))
          message('    Stopping execution...')
          stop(paste0('No recoding instructions for this demo. Please add ',
                      'coding to 11RecodeDemos.R, under ',demo,' section'))
        } else if (demo == 'education2') {
          message(paste0('  - Recoding ',demo))
          message(paste0('    No recoding instructions for this demo. Please add ',
                         'coding to 11RecodeDemos.R, under ',demo,' section'))
          message('    Stopping execution...')
          stop(paste0('No recoding instructions for this demo. Please add ',
                      'coding to 11RecodeDemos.R, under ',demo,' section'))
        } else if (demo == 'gender') {
          message(paste0('  - Recoding ',demo))
          message(paste0('    No recoding instructions for this demo. Please add ',
                         'coding to 11RecodeDemos.R, under ',demo,' section'))
          message('    Stopping execution...')
          stop(paste0('No recoding instructions for this demo. Please add ',
                      'coding to 11RecodeDemos.R, under ',demo,' section'))
        } else if (demo == 'gender1') {
          message(paste0('  - Recoding ',demo))
          message(paste0('    No recoding instructions for this demo. Please add ',
                         'coding to 11RecodeDemos.R, under ',demo,' section'))
          message('    Stopping execution...')
          stop(paste0('No recoding instructions for this demo. Please add ',
                      'coding to 11RecodeDemos.R, under ',demo,' section'))
        } else if (demo == 'hhsize') {
          message(paste0('  - Recoding ',demo))
          message(paste0('    No recoding instructions for this demo. Please add ',
                         'coding to 11RecodeDemos.R, under ',demo,' section'))
          message('    Stopping execution...')
          stop(paste0('No recoding instructions for this demo. Please add ',
                      'coding to 11RecodeDemos.R, under ',demo,' section'))
        } else if (demo == 'hh_size') {
          message(paste0('  - Recoding ',demo))
          message(paste0('    No recoding instructions for this demo. Please add ',
                         'coding to 11RecodeDemos.R, under ',demo,' section'))
          message('    Stopping execution...')
          stop(paste0('No recoding instructions for this demo. Please add ',
                      'coding to 11RecodeDemos.R, under ',demo,' section'))
        } else if (demo == 'hh_size1') {
          message(paste0('  - Recoding ',demo))
          if ('num_ind_in_hh' %in% colnames(argus)) {
            argus[,`:=`(num_ind_in_hh = as.numeric(num_ind_in_hh))]
            
            # Remove respondents with hh_size > 10
            old_n <- nrow(argus)
            argus <- argus[num_ind_in_hh < 11,]
            foo <- gc()
            message(paste0('    Removed respondents with hh_size > 10: ',old_n-nrow(argus)))
            
            set(argus,
                j     = demo,
                value = ifelse(argus$num_ind_in_hh >= 5, 5, argus$num_ind_in_hh)
            )
            # argus[,`:=`(hh_size1 = ifelse(num_ind_in_hh >= 5, 5, num_ind_in_hh))]
            if (params$mod_11$clean) { argus[, num_ind_in_hh := NULL] }
          } else {
            message('    Error: num_ind_in_hh missing in Argus dataset')
            message('    Stopping execution...')
            stop(paste0("Error: num_ind_in_hh missing in Argus dataset"))
          }
        } else if (demo == 'income') {
          message(paste0('  - Recoding ',demo))
          message(paste0('    No recoding instructions for this demo. Please add ',
                         'coding to 11RecodeDemos.R, under ',demo,' section'))
          message('    Stopping execution...')
          stop(paste0('No recoding instructions for this demo. Please add ',
                      'coding to 11RecodeDemos.R, under ',demo,' section'))
        } else if (demo == 'income1') {
          message(paste0('  - Recoding ',demo))
          message(paste0('    No recoding instructions for this demo. Please add ',
                         'coding to 11RecodeDemos.R, under ',demo,' section'))
          message('    Stopping execution...')
          stop(paste0('No recoding instructions for this demo. Please add ',
                      'coding to 11RecodeDemos.R, under ',demo,' section'))
        } else if (demo == 'income2') {
          message(paste0('  - Recoding ',demo))
          message(paste0('    No recoding instructions for this demo. Please add ',
                         'coding to 11RecodeDemos.R, under ',demo,' section'))
          message('    Stopping execution...')
          stop(paste0('No recoding instructions for this demo. Please add ',
                      'coding to 11RecodeDemos.R, under ',demo,' section'))
        } else if (demo == 'kids_any') {
          message(paste0('  - Recoding ',demo))
          if ('children_counts' %in% colnames(argus)) {
            argus[,`:=`(children_counts = as.numeric(children_counts))]
            argus[,`:=`(kids_any = ifelse(children_counts > 0, 1, 2))]
            argus[is.na(argus$kids_any),`:=`(kids_any = 2)]
            if (params$mod_11$clean) { argus[, children_counts := NULL] }
          } else {
            message('    Error: children_counts missing in Argus dataset')
            message('    Stopping execution...')
            stop(paste0("Error: children_counts missing in Argus dataset"))
          }
        } else if (demo == 'kidsyesno') {
          message(paste0('  - Recoding ',demo))
          message(paste0('    No recoding instructions for this demo. Please add ',
                         'coding to 11RecodeDemos.R, under ',demo,' section'))
          message('    Stopping execution...')
          stop(paste0('No recoding instructions for this demo. Please add ',
                      'coding to 11RecodeDemos.R, under ',demo,' section'))
        } else if (demo == 'hispanic') {
          message(paste0('  - Recoding ',demo))
          if ('num_hisp' %in% colnames(argus)) {
            argus[,`:=`(num_hisp = as.numeric(num_hisp))]
            argus[,`:=`(hispanic = ifelse(num_hisp > 0, 1, 2))]
            argus[is.na(argus$hispanic),`:=`(hispanic = 2)]
            argus[,`:=`(hp = ifelse(hispanic == 1, 1, 0))]
            if (params$mod_11$clean) { argus[, num_hisp := NULL] }
          } else {
            message('    Error: num_hisp missing in Argus dataset')
            message('    Stopping execution...')
            stop(paste0("Error: num_hisp missing in Argus dataset"))
          }
        } else if (demo == 'male') {
          message(paste0('  - Recoding ',demo))
          message(paste0('    No recoding instructions for this demo. Please add ',
                         'coding to 11RecodeDemos.R, under ',demo,' section'))
          message('    Stopping execution...')
          stop(paste0('No recoding instructions for this demo. Please add ',
                      'coding to 11RecodeDemos.R, under ',demo,' section'))
        } else if (demo == 'number_of_tvs1') {
          message(paste0('  - Recoding ',demo))
          message(paste0('    No recoding instructions for this demo. Please add ',
                         'coding to 11RecodeDemos.R, under ',demo,' section'))
          message('    Stopping execution...')
          stop(paste0('No recoding instructions for this demo. Please add ',
                      'coding to 11RecodeDemos.R, under ',demo,' section'))
        } else if (demo == 'paycable') {
          message(paste0('  - Recoding ',demo))
          message(paste0('    No recoding instructions for this demo. Please add ',
                         'coding to 11RecodeDemos.R, under ',demo,' section'))
          message('    Stopping execution...')
          stop(paste0('No recoding instructions for this demo. Please add ',
                      'coding to 11RecodeDemos.R, under ',demo,' section'))
        } else if (demo == 'race1') {
          message(paste0('  - Recoding ',demo))
          message(paste0('    No recoding instructions for this demo. Please add ',
                         'coding to 11RecodeDemos.R, under ',demo,' section'))
          message('    Stopping execution...')
          stop(paste0('No recoding instructions for this demo. Please add ',
                      'coding to 11RecodeDemos.R, under ',demo,' section'))
        } else if (demo == 'race2') {
          message(paste0('  - Recoding ',demo))
          message(paste0('    No recoding instructions for this demo. Please add ',
                         'coding to 11RecodeDemos.R, under ',demo,' section'))
          message('    Stopping execution...')
          stop(paste0('No recoding instructions for this demo. Please add ',
                      'coding to 11RecodeDemos.R, under ',demo,' section'))
        } else if (demo == 'race_asian') {
          message(paste0('  - Recoding ',demo))
          if ('num_asians' %in% colnames(argus)) {
            argus[,`:=`(num_asians = as.numeric(num_asians))]
            argus[,`:=`(race_asian = ifelse(num_asians > 0 , 1, 2))]
            argus[is.na(argus$race_asian),`:=`(race_asian = 2)]
            if (params$mod_11$clean) { argus[, num_asians := NULL] }
          } else {
            message('    Error: num_asians missing in Argus dataset')
            message('    Stopping execution...')
            stop(paste0("Error: num_asians missing in Argus dataset"))
          }
        } else if (demo == 'race_afram') {
          message(paste0('  - Recoding ',demo))
          if ('num_afr_am' %in% colnames(argus)) {
            argus[,`:=`(num_afr_am = as.numeric(num_afr_am))]
            argus[,`:=`(race_afram = ifelse(num_afr_am > 0 , 1, 2))]
            argus[is.na(argus$race_afram),`:=`(race_afram = 2)]
            if (params$mod_11$clean) { argus[, num_afr_am := NULL] }
          } else {
            message('    Error: num_afr_am missing in Argus dataset')
            message('    Stopping execution...')
            stop(paste0("Error: num_afr_am missing in Argus dataset"))
          }
        } else if (demo == 'race_black') {
          message(paste0('  - Recoding ',demo))
          message(paste0('    No recoding instructions for this demo. Please add ',
                         'coding to 11RecodeDemos.R, under ',demo,' section'))
          message('    Stopping execution...')
          stop(paste0('No recoding instructions for this demo. Please add ',
                      'coding to 11RecodeDemos.R, under ',demo,' section'))
        } else if (demo == 'race_cauc') {
          message(paste0('  - Recoding ',demo))
          if ('num_cauc' %in% colnames(argus)) {
            argus[,`:=`(num_cauc = as.numeric(num_cauc))]
            argus[,`:=`(race_cauc = ifelse(num_cauc > 0 , 1, 2))]
            argus[is.na(argus$race_cauc),`:=`(race_cauc = 2)]
            if (params$mod_11$clean) { argus[, num_cauc := NULL] }
          } else {
            message('    Error: num_cauc missing in Argus dataset')
            message('    Stopping execution...')
            stop(paste0("Error: num_cauc missing in Argus dataset"))
          }
        } else if (demo == 'tv_hrs_week_hml') {
          message(paste0('  - Recoding ',demo))
          message(paste0('    No recoding instructions for this demo. Please add ',
                         'coding to 11RecodeDemos.R, under ',demo,' section'))
          message('    Stopping execution...')
          stop(paste0('No recoding instructions for this demo. Please add ',
                      'coding to 11RecodeDemos.R, under ',demo,' section'))
        } else {
          message(paste0('    No recoding instructions for ',demo,'. Please create ',
                         'new entry in 11RecodeDemos.R'))
          message('    Stopping execution...')
          stop(paste0('No recoding instructions for ',demo,'. Please create ',
                      'new entry in 11RecodeDemos.R'))
        }
        
      }
      
    }
    
    
    # Save modified argus dataset
    message(paste0('\n  Memory usage is: ',memory.size(max=FALSE),' MB'))
    message(paste0('\n  Writing ',params$datapath,'/argus_11.RData'))
    save(argus, file=paste0(params$datapath,'/argus_11.RData'))
    
    foo <- gc()
    message(paste0('\n  Memory usage is: ',memory.size(max=FALSE),' MB'))
    message(paste0('\n  Dataset has ',nrow(argus),' rows and ',ncol(argus),' columns.'))
    return(argus)
  }
  
}
################################################################################
############################## Run Main Function ###############################
################################################################################
if (getOption('run.main', default=TRUE)) {
  
  # LOAD UTILITY FUNCTIONS
  # (these are normally loaded by the main script - 10SetParameters)
  
  # Function that checks if package is installed.
  # If installed, it loads it. If it isn't, it installs it.
  pkgTest <- function(x) {
    if (!(x %in% installed.packages()[, 'Package'])) {
      tryCatch({
        message(paste0('INSTALLING PACKAGE ',toupper(x)))
        if (x == 'xda') {
          install_github("ujjwalkarn/xda")
        } else {
          install.packages(x, quiet = TRUE)
        }
      }, error = function(e) {
        stop(paste0('  ERROR: ',e))
      })
    } 
    message(paste0('LOADING PACKAGE ',toupper(x)))
    library(x,character.only = TRUE, quietly = TRUE)
  }
  
  # Get index of columns in a dataset
  get_indexes <- function(columns,dataset) {
    columns <- tolower(columns)
    indexes <- c()
    for (colname in columns) {
      indexes <- c(indexes,match(colname,tolower(names(dataset))))
    }
    return(indexes)
  }
  
  # Function to load and rename RData file, and allocate memory to it
  loadRData <- function(fileName,colalloc){
    #loads an RData file, and returns it
    load(fileName)
    alloc.col(get(ls()[ls() != "fileName"]),colalloc)
  }
  
  # Function to load and rename RData file
  loadRData_noalloc <- function(fileName) {
    #loads an RData file, and returns it
    load(fileName)
    get(ls()[ls() != "fileName"])
  }
  
  # IMPORT PACKAGES
  # (these are normally loaded by the main script - 10SetParameters)
  pkgTest('jsonlite')
  pkgTest('data.table')
  pkgTest('ff')
  pkgTest('ffbase')
  
  path <- 'E:/Citi_Amazon'
  params <- fromJSON(file.path(path,'params.json'), simplifyDataFrame=TRUE)
  argus <- loadRData(paste0(params$filepath,'.RData'),params$colalloc)
  
  main_11(params,argus)
  
}