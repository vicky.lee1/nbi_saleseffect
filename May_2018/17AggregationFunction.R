# Purpose: Recode exposures and Build Analysis Dataset
# Assumption: there is only two time periods (e.g pre vs. test)
#  Input:  argus
#  Output: argus_recoded
#          R_Datasets\\argus_recoded_save.RData
# Packages required: data.table, matrixStats
# Author: Yali Chen
# Aug, 2016
#
# References:
# use data.table package to read in large files 
# Reference: http://stackoverflow.com/questions/1727772/quickly-reading-very-large-tables-as-dataframes-in-r
# data.table with lapply
# Reference: http://stackoverflow.com/questions/16943939/elegantly-assigning-multiple-columns-in-data-table-with-lapply
# rowsums using reduce: 
# Reference: http://stackoverflow.com/questions/29270078/rowsums-in-a-data-table-in-r
# Reference: http://stackoverflow.com/questions/18881073/creating-dummy-variables-in-r-data-table

#rm(list = ls(all = TRUE))
#options(run.main=TRUE)

################################################################################
############################# Main Function ####################################
################################################################################
{
  
  main_17 <- function(params,argus_recoded) {
    
    message(paste0('\n  Memory usage is: ',memory.size(max=FALSE),' MB'))
    
    # 2. Recoding (data.table + set)
    message("\n  Recoding Exposures")
    
    # Over-allocation access
    # These functions (truelength, alloc.col) are experimental and somewhat advanced. 
    # By experimental we mean their names might change and perhaps the syntax, 
    # argument names and types. So if you write a lot of code using them, 
    # you have been warned! 
    # They should work and be stable, though, so please report problems with them.
    
    # I encounter memory issues, below is the turn around for now:
    # http://stackoverflow.com/questions/5171593/r-memory-management-cannot-allocate-vector-of-size-n-mb
    
    # If you want to see more details, please comment out the line below:
    # ?alloc.col
    
    # 300 in the below function is the abitary number I assigned. Assuming 
    # the max # variables/columns we could have in argus_recoded dataset is 300
    # If you have more than 300 columns, please edit the number in the below function
    #alloc.col(argus_recoded,300)
    
    ############### Initialize sample, weight. Assign respondentid
    message('  - Generating sample, weight and respondentid columns.')
    argus_recoded[
      , `:=`(
        sample = 1
        , weight = 1
        , respondentid = .I)
      ]
    
    ############### Create transyes flags for pre1, pre2, pre3, etc
    segment.transyes.period.i <- CreateMetricsList(period.def =lapply(params$period.def,tolower)
                                                  ,segmentlst = tolower(params$segmentlst)
                                                  ,metrics = tolower(params$penlist))
    segment.trans.period.i <- CreateMetricsList(period.def = lapply(params$period.def,tolower)
                                               ,segmentlst = tolower(params$segmentlst)
                                               ,metrics = tolower(params$renstats[2]))
    write_list('  - Generating transyes columns:',unlist(segment.transyes.period.i),4,80)
    argus_recoded[, 
        (unlist(segment.transyes.period.i)) := lapply(unlist(segment.trans.period.i)
        , function(x) ifelse(get(x) > 0, 1, 0))
        ]
    
    # Aggregate metrics (spend, trans, transyes) for segments (merchants, category),
    # time periods (pre, test)
    # TotalorMonthly = 1, Total Metrics
    # TotalorMonthly = 2, Monthly Metrics
    message(paste0('  - Aggregating metrics (spend, trans, transyes) for segments',
                   ' (merchants, category) and time periods (pre, test)'))
    segment.metrics.period <- CreateMetricsList(period.def = tolower(names(params$period.def))
                                               ,segmentlst = tolower(params$segmentlst)
                                               ,metrics = tolower(params$renstats))
    segment.metrics.period.i   <- CreateMetricsList(period.def = lapply(params$period.def,tolower)
                                                   ,segmentlst = tolower(params$segmentlst)
                                                   ,metrics = tolower(params$renstats))
    segment.transyes.period    <- CreateMetricsList(period.def = tolower(names(params$period.def))
                                                   ,segmentlst = tolower(params$segmentlst)
                                                   ,metrics = tolower(params$penlist))
    if (params$TotalorMonthly == 1) {
      
      message(paste0('    Using \"Total Metrics\" for studies where pre and ',
                     'test are of the same lengths'))
      write_list('    Generating segment metrics columns:',unlist(segment.metrics.period),6,80)
      for (i in seq_along(segment.metrics.period)) {
        # i = 1
        set(argus_recoded, j = segment.metrics.period[[i]],
            value = rowSums(argus_recoded[,segment.metrics.period.i[[i]],
                                          with = FALSE], na.rm=TRUE)
            )    
      }
      write_list('    Generating segment transyes columns (max):',
                 unlist(segment.transyes.period),6,80)
      write_list('    Generating total segment transyes columns (same as segment transyes):',
                 paste0(unlist(segment.transyes.period),'_total'),6,80)
      for (i in seq_along(segment.transyes.period)) {
        # i=4
        set(argus_recoded, j = segment.transyes.period[[i]],
            value = rowMaxs(as.matrix(argus_recoded[,segment.transyes.period.i[[i]], with = FALSE]), na.rm=TRUE)
            )
        set(argus_recoded, 
            j     = paste0(segment.transyes.period[[i]],'_total'),
            value = argus_recoded[,segment.transyes.period[[i]],with=FALSE]
            )
      }
      
    } else if (params$TotalorMonthly == 2) {
      
      message(paste0('    Using \"Monthly Metrics\" for studies where pre and ',
                     'test are of different lengths'))
      write_list('    Generating segment metrics columns:',unlist(segment.metrics.period),6,80)
      for (i in seq_along(segment.metrics.period)) {
        # i = 1
        set(argus_recoded, j = segment.metrics.period[[i]],
            value = rowSums(argus_recoded[,segment.metrics.period.i[[i]],with = FALSE], na.rm=TRUE)/
                    (params$days.def[[unlist(strsplit(segment.metrics.period[[i]],"_"))[3]]]/30))
      }
      
      write_list('    Generating segment transyes columns (sums):',unlist(segment.transyes.period),6,80)
      for (i in seq_along(segment.transyes.period)) {
      #   set(argus_recoded, j = segment.transyes.period[[i]],
      #       value = rowSums(argus_recoded[,segment.transyes.period.i[[i]],
      #       with = FALSE], na.rm=TRUE)/(params$days.def[[unlist(strsplit(segment.metrics.period[[i]],"_"))[3]]]/30))
        # i <- 2
        to_max <- c()
        for (iper in params$short_periods) {
          
          underscore_pos_lt<-lapply(segment.transyes.period.i[[i]], function(x) tail(gregexpr("_", x, fixed = TRUE )[[1]], n=1))
          names(underscore_pos_lt)<-segment.transyes.period.i[[i]]
          
          period.i<-
            lapply(segment.transyes.period.i[[i]], function (x) substr(x, underscore_pos_lt[[x]]+1,nchar(x) ) )
          
          to_max <- c(to_max, grep(paste0("\\b",iper,"\\b"), period.i ))
        }
        to_max <- segment.transyes.period.i[[i]][to_max]
        to_sum <- setdiff(segment.transyes.period.i[[i]],to_max)
        num_months <- params$days.def[[unlist(strsplit(segment.metrics.period[[i]],"_"))[3]]]/30
        if (length(to_sum) == 0) {
          stop(paste0("    ERROR: Doing monthly calculation but taking the max",
                      " of all periods. If want to take the max of all periods,",
                      " need to change to TotalorMonthly = 1"))
        #   set(argus_recoded,
        #       j     = segment.transyes.period[[i]],
        #       value = rowMaxs(as.matrix(argus_recoded[,to_max,with=FALSE]),na.rm=TRUE)
        #   )
        } else
        if (length(to_max) > 0) {
          # num_months <- length(to_sum)+1
          set(argus_recoded,
              j     = segment.transyes.period[[i]],
              value = rowMins(matrix(c(rep(1,nrow(argus_recoded)),
                                       (rowSums(argus_recoded[,to_sum,with=FALSE], na.rm=TRUE)+
                                          rowMaxs(as.matrix(argus_recoded[,to_max,with=FALSE]),na.rm=TRUE))/
                                         num_months
                                      ), nrow=nrow(argus_recoded),ncol=2),na.rm=TRUE)
              )
        } else {
          # num_months <- length(to_sum)
          set(argus_recoded,
              j     = segment.transyes.period[[i]],
              value = rowMins(matrix(c(rep(1,nrow(argus_recoded)),
                                       rowSums(argus_recoded[,to_sum,with=FALSE],na.rm=TRUE)/
                                         num_months
                                      ), nrow=nrow(argus_recoded),ncol=2),na.rm=TRUE)
             )
        }
      }
      
      write_list('    Generating total segment transyes columns (max):',
                 paste0(unlist(segment.transyes.period),'_total'),6,80)
      for (i in seq_along(segment.transyes.period)) {
        set(argus_recoded, j = paste0(segment.transyes.period[[i]],'_total'),
            value = rowMaxs(as.matrix(argus_recoded[,segment.transyes.period.i[[i]],
                                                    with = FALSE]), na.rm=TRUE))
      }
      
    }
    
    ############### Build analysis flag for T-test later
    message('  - Building analysis flag for T-test later')
    segment.transyes <- CreateMetricsList(period.def = NULL
                                         ,segmentlst = tolower(params$segmentlst)
                                         ,metrics = tolower(params$penlist))
    write_list('    Generating overall segment transyes columns:',unlist(segment.transyes),6,80)
    for (i in seq_along(segment.transyes)) {
      #i=1
      set(argus_recoded, j = segment.transyes[[i]],
          value = rowMaxs(as.matrix(argus_recoded[,
                  paste0(unlist(segment.transyes.period),'_total')[c(2*i-1,2*i)],with = FALSE]
                  ), na.rm = TRUE))
    }
    
    ############### Build own dollars per trans vars for T-test later
    message('  - Building own dollars per trans vars for T-test later')
    segment.d.per.tr.period <- CreateMetricsList(period.def = tolower(names(params$period.def))
                                                ,segmentlst = tolower(params$segmentlst)
                                                ,metrics = tolower(params$statslst[3]))
    segment.trans.period <- CreateMetricsList(period.def = tolower(names(params$period.def))
                                             ,segmentlst = tolower(params$segmentlst)
                                             ,metrics = tolower(params$renstats[2]))
    segment.spend.period <- CreateMetricsList(period.def = tolower(names(params$period.def))
                                             ,segmentlst = tolower(params$segmentlst)
                                             ,metrics = tolower(params$renstats[1]))
    write_list('    Generating columns:',unlist(segment.d.per.tr.period),6,80)
    for (i in seq_along(segment.d.per.tr.period)) {
      set(argus_recoded, j = segment.d.per.tr.period[[i]],
          value = ifelse(argus_recoded[[segment.trans.period[[i]]]] == 0, 0, 
                         argus_recoded[[segment.spend.period[[i]]]]/
                         argus_recoded[[segment.trans.period[[i]]]]))
    }
    
    ############### Share for T-tests later
    message('  - Share for T-tests later')
    segment.share.period <- CreateMetricsList(period.def = tolower(names(params$period.def))
                                             ,segmentlst = tolower(params$segmentlst)
                                             ,metrics = tolower(params$statslst[4]))
    totalvar.spend.period <- CreateMetricsList(period.def = tolower(names(params$period.def))
                                              ,segmentlst = tolower(params$totalvar)
                                              ,metrics = tolower(params$renstats[1]))
    write_list('    Generating columns:',unlist(segment.share.period),6,80)
    for (i in seq_along(segment.share.period)) {
      set(argus_recoded, j = segment.share.period[[i]], value = ifelse(
          argus_recoded[[totalvar.spend.period[[ifelse(i %% 2 != 0,1,2)]]]] == 0,
          0, (argus_recoded[[segment.spend.period[[i]]]]/
          argus_recoded[[totalvar.spend.period[[ifelse(i %% 2 != 0, 1, 2)]]]])*100))
    }
    
    ############### Need to calculate difference variable
    message('  - Calculating difference variables')
    
    
    
    
    for(ipre in seq_along(params$pre_list)) {
      # ipre = 1
      antime <-params$pre_list[[ipre]]$antime
      pretime <- params$pre_list[[ipre]]$pretime
      
      segment.transyes.pre    <- CreateMetricsList(period.def = pretime
                                                   ,segmentlst = tolower(params$segmentlst)
                                                   ,metrics = c(tolower(params$penlist), tolower(params$statslst))) 
      
      segment.transyes.test    <- CreateMetricsList(period.def = "test"
                                                    ,segmentlst = tolower(params$segmentlst)
                                                    ,metrics = c(tolower(params$penlist), tolower(params$statslst)))
      
      
      segment.transyes.testdiff <- CreateMetricsList(period.def = paste0(tolower(antime),"diff")
                                                     ,segmentlst = tolower(params$segmentlst)
                                                     ,metrics = c(tolower(params$penlist), tolower(params$statslst)))
      
      
      write_list('    Generating columns:',unlist(segment.transyes.testdiff),6,80)
      for (i in seq_along(segment.transyes.testdiff)) {
        set(argus_recoded, j = segment.transyes.testdiff[[i]],
            value = argus_recoded[[segment.transyes.test[[i]]]] - 
              argus_recoded[[segment.transyes.pre[[i]]]])
      }
      
    }
    # Commented generation of these columns because they did not appear in SAS
    # dataset for TEST_STUDY (Longhorn)
    # segment.transyes2.testdiff <- CreateMetricsList(period.def = "testdiff"
    #                                                ,segmentlst = tolower(params$segmentlst)
    #                                                ,metrics = paste0(tolower(params$penlist),"2"))
    # #            unlist(segment.transyes2.testdiff),6,80)
    # # message('      but it was commented out to make output match SAS output for TEST_STUDY.')
    # write_list('    Generating columns:',unlist(segment.transyes2.testdiff),6,80)
    # argus_recoded[,(unlist(segment.transyes2.testdiff)) := lapply(
    #     unlist(segment.transyes.testdiff),
    #     function(x) ifelse(get(x) < 0, -1, ifelse(get(x) == 0, 0, 1)))]


    ############### Remove top 1% of exposure outliers for digital study
    # Moved to script 18
    
    # Remove columns that are not needed anymore
    if (params$mod_17$clean) { argus_recoded[, unlist(segment.metrics.period.i) :=NULL] }
    
    # Save as RData
    message(paste0('\n  Writing ',params$datapath,'/argus_17.RData'))
    save(argus_recoded, file=paste0(params$datapath,"/argus_17.RData"))
    foo <- gc()
    message(paste0('\n  Memory usage is: ',memory.size(max=FALSE),' MB'))
    message(paste0('\n  Dataset has ',nrow(argus_recoded),' rows and ',ncol(argus_recoded),' columns.'))
    return(argus_recoded)
    
  }
  
}
################################################################################
############################## Run Main Function ###############################
################################################################################
if (getOption('run.main', default=TRUE)) {
  
  # LOAD UTILITY FUNCTIONS 
  # (these are normally loaded by the main script - 10SetParameters)
  
  # Function that checks if package is installed.
  # If installed, it loads it. If it isn't, it installs it.
  pkgTest <- function(x) {
    if (!(x %in% installed.packages()[, 'Package'])) {
      tryCatch({
        message(paste0('INSTALLING PACKAGE ',toupper(x)))
        install.packages(x, quiet = TRUE)
      }, error = function(e) {
        stop(paste0('  ERROR: ',e))
      })
    } 
    message(paste0('LOADING PACKAGE ',toupper(x)))
    library(x,character.only = TRUE, quietly = TRUE)
  }
  
  # Function to format list to write to file
  write_list <- function(prefix,list,indent,total_length) {
    llist <- length(list)
    cond <- 1
    count <- 1
    while (cond) {
      ltoprint <- paste(list[1:count],collapse=', ')
      line <- paste(prefix,ltoprint)
      lline <- nchar(line)
      if (lline < total_length) {
        if (count < llist) {
          count <- count+1
        } else {
          cond <- 0
        }
      } else {
        cond <- 0
        count <- count-1
        if (count < 1) {
          line <- prefix
        } else {
          ltoprint <- paste(list[1:count],collapse=', ')
          line <- paste(prefix,ltoprint)
        }
      }
    }
    message(line)
    
    prefix <- paste(rep(" ",indent),collapse="")
    
    while (count < llist) {
      cond <- 1
      count <- count+1
      count2 <- count
      while (cond) {
        ltoprint <- paste(list[count2:count],collapse=', ')
        line <- paste0(prefix,ltoprint)
        lline <- nchar(line)
        if (lline < total_length) {
          if (count < llist) {
            count <- count+1
          } else {
            cond <- 0
          }
        } else {
          cond <- 0
          count <- count-1
          if (count < count2) {
            count <- count+1
          } else {
            ltoprint <- paste(list[count2:count],collapse=', ')
            line <- paste0(prefix,ltoprint)
          }
        }
      }
      message(line)
    }
  }
  
  # Function to load and rename RData file, and allocate memory to it
  loadRData <- function(fileName,colalloc){
    #loads an RData file, and returns it
    load(fileName)
    alloc.col(get(ls()[ls() != "fileName"]),colalloc)
  }
  
  # Function to load and rename RData file
  loadRData_noalloc <- function(fileName) {
    #loads an RData file, and returns it
    load(fileName)
    get(ls()[ls() != "fileName"])
  }
  
  # Function to create list of metrics
  CreateMetricsList <- function(segmentlst,metrics,period.def) {
    if (is.null(period.def) == FALSE) {
      outputlist <- vector(mode="list"
                           , length = length(segmentlst) * length(metrics) * length(period.def))
      
      i <- 1
      for (x in seq_along(segmentlst)){
        for (y in seq_along(metrics)){
          for (z in seq_along(period.def)){
            outputlist[i] <- lapply(
              period.def[z]
              ,function(m) paste(segmentlst[x],metrics[y],m,sep="_"))
            i<- i + 1
          }
        }
      }
      return(outputlist)
    }
    
    else if (is.null(period.def) == TRUE){
      outputlist <- vector(mode="list"
                           , length= length(segmentlst) * length(metrics))
      
      i <- 1
      for (x in seq_along(segmentlst)){
        for (y in seq_along(metrics)){
          outputlist[i] <- paste(segmentlst[x],metrics[y],sep="_")
          i<- i + 1
        }
      }
      return(outputlist)
    }
    
  }
  
  # IMPORT PACKAGES
  # (these are normally loaded by the main script - 10SetParameters)
  pkgTest('haven')
  pkgTest('jsonlite')
  pkgTest('data.table')
  pkgTest('matrixStats')
  
  path <- 'E:/Taco_Bell'
  params <- fromJSON(file.path(path,'params.json'), simplifyDataFrame=TRUE)
  
  # Load the input file
  if (params$use_sas_input) {
    if (params$val_format == 'sas7bdat') {
      message(paste0('  Loading ',params$val_dir,'/SAS_Datasets/argus_',
                     tolower(params$datatype),'.sas7bdat'))
      argus_recoded <- as.data.table(haven::read_sas(paste0(params$val_dir,
                      '/SAS_Datasets/argus_',tolower(params$datatype),'.sas7bdat')))
    } else if (params$val_format == 'RData') {
      if (path == 'E:/McDonalds') {
        message(paste0('  Loading ',params$val_dir,'/SAS_Datasets/argus_',
                       tolower(params$datatype),'_1.RData'))
        argus_recoded <- loadRData_noalloc(paste0(params$val_dir,
                         '/SAS_Datasets/argus_',tolower(params$datatype),'_1.RData'))
      } else {
        message(paste0('  Loading ',params$val_dir,'/SAS_Datasets/argus_',
                       params$datatype,'.RData'))
        argus_recoded <- loadRData_noalloc(paste0(params$val_dir,
                         '/SAS_Datasets/argus_',tolower(params$datatype),'.RData'))
      }
    }
    names(argus_recoded) <- tolower(names(argus_recoded)) 
  } else {
    message(paste0('  Loading ',params$datapath,'/argus_12.RData'))
    argus_recoded <- loadRData(paste0(params$datapath,'/argus_12.RData'),
                               params$colalloc)
  }
  
  argus_recoded <- main_17(params,argus_recoded)
  
}