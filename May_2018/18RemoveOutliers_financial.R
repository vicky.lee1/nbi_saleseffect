#'SCRIPT 18 - NBI SE
#'Brendan Doohan on May 16, 2016
#'Remove Outliers

#'LOAD PACKAGES AND RUN RECODE AND BUCKET SCRIPTS
#'ASK RUI IF WE SHOULD BUCKET BY SPEND, REMOVE SPEND OUTLIERS AND THEN BUCKET AGAIN
#'YALI MENTIONED THIS ON MAY 12, 2016 SINCE IT DOES NOT MAKE SENSE TO FIRST BUCKET AND THEN REMOVE OUTLIERS
#'
#'N.B. THERE ARE NO OUTLIERS FOR THE TEST DATA. YOU MAY NEED TO CREATE AN EXTRA ROW 
#'WITH VERY LARGE VALUES (USING COLSUMS FUNCTION?), OR
#'REDUCE STANDARD TO 3 STD AWAY FROM MEAN IN ORDER TO SEE RESULTS.

# Purpose: Remove exposure outliers and spending outliers
#  Input:  argus_recoded
#  Output: argus_recoded
#          R_Datasets\\remove.expfreq.RData
#          R_Datasets\\remove.spend.RData

#rm(list = ls(all = TRUE))
#options(run.main=TRUE)

################################################################################
########################## Auxiliary Functions #################################
################################################################################
{
  
  # Function to Remove Exposure Outlier
  #inds <- argus_recoded
  #explist <- tolower(params$explist)
  RemoveExposureOutliers <- function(inds, expmax, explist) {
    # By default, we remove top 1% exposure freq outlier, expmax = 0.01
    # Assuming first item in explist is the "total exposure" variable
    quants <- quantile(inds[inds[[explist[1]]] != 0][[explist[1]]]
      , probs = c(1-expmax))
    message('  - Identifying exposure outliers as respondents with:')
    message(paste0('    ',explist[1],' >= ',quants))
    wherestate <- parse(text=paste0(explist[1]," >= ",quants))
    # Respondents that need to be removed
    remove.expfreq 	<- inds[eval(wherestate)]
    return (remove.expfreq)
  }
  
  # Function to Remove Spend Outlier
  # inds <- argus_recoded
  # explist <- tolower(params$explist)
  RemoveSpendOutliers <- function(inds, cutoff, category.metrics.period, 
                                  log.category.metrics.period, explist, cat) {
    
    # check if spending metrics were created correctly in previous scripts
    invisible(lapply(unlist(category.metrics.period)
                     , function(x) if (!(x %in% colnames(inds))) { 
                       message(paste0("  Error: Category Spending Variable \""
                                      , x
                                      , "\" does not exist. Please check recoding in Script 17."))
                     }
    ))
    
    # Lists of category spend/trans pre/test and their log transformation are 
    # created in script 999FunctionToCreateLists
    # category.metrics.period
    # log.category.metrics.period
    
    # Keep only respondentid, explist, category spend/trans pre/test
    spdtrans = inds[
      #, c("respondentid",explist,unlist(category.metrics.period),paste(cat,'gap',sep='_'))
      , c("respondentid",explist[1],unlist(category.metrics.period),paste(cat,'gap',sep='_'))
      , with=FALSE ]
    
    
    # /*Calculate Mean and Standard Deviation of absolute change of spend from pre to test*/
    
    # Get log transformation
    for(i in seq_along(category.metrics.period)) { 
      #i = 7
      set(spdtrans,
          j 	  = log.category.metrics.period[[i]],
          value = ifelse(spdtrans[[category.metrics.period[[i]]]] > 0, 
                         log(spdtrans[[category.metrics.period[[i]]]]), 
                         NA))
    }
    spdtrans[, 'changelog':=ifelse(get(paste(cat, 'gap', sep = '_')) != 0, 
                                   log(abs(get(paste(cat, 'gap', sep = '_')))) , NA) ]
    
    # update log.category.metrics.period
    
    log.category.metrics.period <- list.append(log.category.metrics.period, 'changelog')
    
    
    # Calculate std, mean, and upper.list = mean + cutoff * std
    std <- spdtrans[
      , lapply(.SD, sd, na.rm=TRUE)
      , .SDcols = c(unlist(log.category.metrics.period))
      ]
    
    mean <- spdtrans[
      , lapply(.SD, mean, na.rm=TRUE)
      , .SDcols = c(unlist(log.category.metrics.period))
      ]
    
    upper.limit <- mean + cutoff * std
    
    ############################################################################
    # # Original code:
    # # Create dataset that has all the respondents we want to remove
    # remove.ds.vec <- c("remove1","remove2","remove3","remove4")
    # lapply(seq_along(log.category.metrics.period), 
    #        function(x) {
    #          wherestate <- parse(text=paste0(log.category.metrics.period[[x]],
    #                                          ">",
    #                                          upper.limit[[x]]))
    #          remove.spend <- spdtrans[eval(wherestate)]
    #          assign(remove.ds.vec[x], remove.spend)#, envir = .GlobalEnv)
    #        })
    # remove.spend <- rbind(remove1,remove2,remove3,remove4)
    # # Clear key so that when use unique function to remove duplicate below, 
    # # it will remove duplicated based on all columns, rather than just the key columns
    # setkey(remove.spend,NULL)
    ############################################################################
    # Replaced by Vanessa with:
    message('  - Identifying spending outliers as respondents with:')
    remove.spend  <- NULL
    for (x in seq_along(log.category.metrics.period)) {
      #x =3
      wherestate <- parse(text=paste0(log.category.metrics.period[[x]],
                                      ">",
                                      upper.limit[[x]]))
      message(paste0('    ',log.category.metrics.period[[x]]," > ",upper.limit[[x]]))
      remove.spend <- rbind(remove.spend,spdtrans[eval(wherestate)])
    }
    ############################################################################
  
    # Remove duplicates
    remove.spend = unique(remove.spend)
    
    return (remove.spend)
  }
  
  # Function to validate "comparison" dataset against "model" dataset.
  validate_18 <- function(params,filename_list,key_column_list) {
    
    path <- params$path
    dump_diff_file <- params$dump_diff_file
    
    if (!file.exists(paste0(path,'/Validation'))) { 
      dir.create(paste0(path,'/Validation')) 
    }
    
    for (j in 1:length(filename_list)) {
      #j<-2
      ModelFileName <- filename_list[[j]][1]
      ModelFileType <- substr(ModelFileName,regexpr('.',ModelFileName,fixed=TRUE)+1
                              ,nchar(ModelFileName))
      
      if (file.exists(ModelFileName)) {
        
        key_column <- key_column_list[[j]]
        
        # Find the matching file in the comparison folder
        ComparisonFileName <- filename_list[[j]][2]
        ComparisonFileType <- substr(ComparisonFileName,regexpr('.',ComparisonFileName,fixed=TRUE)+1
                                     ,nchar(ComparisonFileName))
        
        message(paste0('\n  Validating ',ComparisonFileName))
        message(paste0('  against ',ModelFileName))
        
        # Output folder and file names
        outfolder <- paste0(path,'/Validation')
        filename <- substr(ModelFileName,1,regexpr('.',ModelFileName,fixed=TRUE)-1)
        char_pos <- gregexpr('/',filename,fixed=TRUE)[[1]]
        filename <- substr(filename,char_pos[length(char_pos)]+1,nchar(filename))
        filename <- paste0('/diff_',filename)
        outname <- base::paste0(outfolder,filename)
        
        # Upload files
        if (ModelFileType == 'sas7bdat') {
          Modelfile <- haven::read_sas(ModelFileName)
        } else if (ModelFileType == 'csv') {
          Modelfile <- read.csv(file=ModelFileName)
        } else if (ModelFileType == 'RData') {
          Modelfile <- loadRData_noalloc(ModelFileName)
        } else if (ModelFileType == 'xlsx') {
          message('  - Please, add capability to read xlsx files in validate_17 function.')
        }
        if (!('data.table' %in% class(Modelfile))) {
          Modelfile <- as.data.table(Modelfile)
        }
        colnames(Modelfile) <- base::tolower(base::colnames(Modelfile))
        
        # if respondentid not exist
        if ('randomid'%in%colnames(Modelfile)) {
          colnames(Modelfile)[ colnames(Modelfile)=='randomid']='respondentid'
        }
       
        if (nrow(Modelfile) == 0) {
          
          if (ComparisonFileType == 'sas7bdat') {
            Comparisonfile <- haven::read_sas(ComparisonFileName)
          } else if (ComparisonFileType == 'csv') {
            Comparisonfile <- read.csv(file=ComparisonFileName)
          } else if (ComparisonFileType == 'RData') {
            Comparisonfile <- loadRData_noalloc(ComparisonFileName)
          } else if (ComparisonFileType == 'xlsx') {
            message('  - Please, add capability to read xlsx files.')
          }
          colnames(Comparisonfile) <- base::tolower(base::colnames(Comparisonfile))
          
          if (nrow(Comparisonfile) == 0) {
            
            message('  - No discrepancies were found.')
            
          } else {
            
            message('  - Discrepancies were found.')
            message(paste0('    Dimensions of model dataset were ',
                           toString(nrow(Modelfile)),' x ',
                           toString(ncol(Modelfile))))
            message(paste0('    while dimensions of comparison dataset were ',
                           toString(nrow(Comparisonfile)),' x ',
                           toString(ncol(Comparisonfile))))
            
          }
          
        } else {
          
          if (ComparisonFileType == 'sas7bdat') {
            Comparisonfile <- haven::read_sas(ComparisonFileName)
          } else if (ComparisonFileType == 'csv') {
            Comparisonfile <- read.csv(file=ComparisonFileName)
          } else if (ComparisonFileType == 'RData') {
            Comparisonfile <- loadRData_noalloc(ComparisonFileName)
          } else if (ComparisonFileType == 'xlsx') {
            message('  - Please, add capability to read xlsx files.')
          }
          if (!('data.table' %in% class(Comparisonfile))) {
            Comparisonfile <- as.data.table(Comparisonfile)
          }
          colnames(Comparisonfile) <- base::tolower(base::colnames(Comparisonfile))
          
          if (nrow(Comparisonfile) == 0) {
            
            message('  - Discrepancies were found.')
            message(paste0('    Dimensions of model dataset were ',
                           toString(nrow(Modelfile)),' x ',
                           toString(ncol(Modelfile))))
            message(paste0('    while dimensions of comparison dataset were ',
                           toString(nrow(Comparisonfile)),' x ',
                           toString(ncol(Comparisonfile))))
            
          } else {
            
            if ((j == 2) & (params$path == 'E:/Citi_Amazon')) {
              # Delete columns with all NAs and "share" columns
              to_delete <- c(grep('citi_az_share',colnames(Modelfile),value=T),
                             grep('citi_ol_share',colnames(Modelfile),value=T))
              for (colname in colnames(Modelfile)) {
                if (length(unique(Modelfile[[colname]])) == 1) { 
                  if (is.na(unique(Modelfile[[colname]]))) {
                    to_delete <- c(to_delete,colname)
                  }
                }
              }
              to_delete <- unique(to_delete)
              Modelfile[, (to_delete) := NULL]
            }
            
            
            # non_overlap <- setdiff(Modelfile[,respondentid],
            #                        Comparisonfile[,respondentid])
            # Modelfile[((respondentid %in% non_overlap) & (total_exp < 141)),
            #           c('respondentid','total_exp'),with=F]
                             
            # Select only respondents that exist in both files
                        overlap <- intersect(Modelfile[,respondentid],
                                 Comparisonfile[,respondentid])
            message(paste0('  - SAS file contains ',nrow(Modelfile),
                           ' respondents, while R file contains ',nrow(Comparisonfile),
                           ' respondents.'))
            message(paste0('    Doing validation for respondents that are ',
                           'in both files: ',length(overlap)))
            Modelfile <- Modelfile[which(respondentid %in% overlap),]
            Comparisonfile <- Comparisonfile[which(respondentid %in% overlap),]
            
            # Select only columns that exist in both files and make their data types equal
            overlap_cols <- intersect(colnames(Modelfile),colnames(Comparisonfile))
            for (icol in overlap_cols) {
              #icol = overlap_cols[1]
              if(class(Comparisonfile[[icol]]) == "integer") {
                set(Modelfile, j = icol, 
                    value = as.integer(gsub(',','',Modelfile[[icol]])))
              } else if (class(Comparisonfile[[icol]]) == "character") {
                set(Modelfile, j = icol,
                    value = as.character(Modelfile[[icol]]))
              } else if (class(Comparisonfile[[icol]]) == "numeric") {
                set(Modelfile, j = icol,
                    value = as.numeric(Modelfile[[icol]]))
              }
            }
            
            # Compare files
            validate_dt_file(Modelfile = Modelfile[,overlap_cols,with=F],
                             Comparisonfile = Comparisonfile[,overlap_cols,with=F],
                             key_column,outname,dump_diff_file)
            rm(Modelfile)
            rm(Comparisonfile)
            foo <- gc()
            
          }
          
        }
        
      } else {
        
        message(paste0('  - ',ModelfileName,' does not exist.'))
        
      }
      
    }
    
  }
  
}
################################################################################
############################# Main Function ####################################
################################################################################
{
  
  main_18 <- function(params,argus_recoded) {
    
    # Parameter for RemoveExposureOutliers (remove top 1% exposure freq outlier)
    expmax <- params$expmax #0.01 
    
    # Parameter for RemoveSpendOutliers (upper.limit <- mean + cutoff * std)
    cutoff <- params$cutoff # 4 

    message(paste0('\n  Memory usage is: ',memory.size(max=FALSE),' MB'))

    # parameters for RemoveSpendOutliers
    #for(iren in tolower(params$renstats)){
    iren = "spend"
    cat = tolower(head(params$segmentlst, 1))
    set(argus_recoded, j = paste(cat, 'gap', sep="_"),
        value = argus_recoded[[paste(cat, iren, 'test', sep="_") ]]-
                argus_recoded[[paste(cat, iren, params$cat.gap.prep, sep="_")]])
    #}
  
    category.metrics.period <- CreateMetricsList(head(tolower(params$segmentlst),1),
                                                 tolower(params$renstats),
                                                 c(tolower(params$cat.gap.prep),'test'))
    # add "gap".
    # category.metrics.period <- list.append(category.metrics.period,
    #                                        paste(cat, 'gap', sep = '_') )
    
    # Yali's version of log.category.metrics.period
    # log.category.metrics.period <- lapply(category.metrics.period, 
    #                                       function(x) paste0("log_",x))
    # To match McDonalds' log.category.metrics.period
    log.category.metrics.period <- CreateMetricsList(tolower(params$renstats),
                                                     c('pre','test'),
                                                     "log")
    # add gap.
    # log.category.metrics.period<-list.append(log.category.metrics.period, 
    #                                          '_gap')
    
    log.category.metrics.period <- lapply(log.category.metrics.period, 
                                          function(x) gsub("_","",x))
    
    message('\n  Get spending outliers')
    
    remove.spend <- RemoveSpendOutliers(argus_recoded, cutoff, category.metrics.period, 
                                        log.category.metrics.period, tolower(params$explist),cat)
    
    message(paste0('  - Found ',nrow(remove.spend),' outliers.'))
    message(paste0('  - Writing spending outliers to ',params$outputpath,'/remove.csv'))
    write.csv(as.data.frame(remove.spend), file = paste0(params$outputpath,
              '/remove.csv'), quote = TRUE, eol = "\n", 
              na = "NA", row.names = FALSE, fileEncoding = "")
    
    message('\n  Get exposure outliers')
    
    remove.expfreq <- RemoveExposureOutliers(argus_recoded, expmax, tolower(params$explist))
    
    message(paste0('  - Found ',nrow(remove.expfreq),' outliers.'))
    message(paste0('  - Writing exposure outliers to ',params$outputpath,'/remove_exp.csv'))
    write.csv(as.data.frame(remove.expfreq), file = paste0(params$outputpath,
              '/remove_exp.csv'), quote = TRUE, eol = "\n", 
              na = "NA", row.names = FALSE, fileEncoding = "")
    
    if (params$datatype == "neu") {
      remove <- rbind(remove.expfreq[,.(respondentid)],
                      remove.spend[,.(respondentid)])
    } else {
      remove <- remove.spend[,.(respondentid)]
    }
    rm(remove.spend)
    
    # clear key and remove duplicates
    setkey(remove,NULL)
    remove <- unique(remove)
    # reinstate the key
    setkey(remove, respondentid)
    setkey(argus_recoded, respondentid)
    
    # subset argus_recoded to those rows that have indexes that don't appear in remove
    # if remove is empty, then set argus_recoded_remove_outlier = argus_recoded
    
    # if (params$mod_18$val) {
    #   if (dim(remove.expfreq)[1] == 0) {
    #     message(paste0('\n  Writing ',params$datapath,'/argus_18_withspendoutliers.RData'))
    #     save(argus_recoded, file=paste0(params$datapath,"/argus_18_withspendoutliers.RData"))
    #   } else {
    #     message('\n  Remove exposure outliers')
    #     setkey(remove.expfreq, respondentid)
    #     argus_recoded <- argus_recoded[-argus_recoded[remove.expfreq[,.(respondentid)]
    #                                                  ,which=TRUE, nomatch=0]]
    #     message(paste0('\n  Writing ',params$datapath,'/argus_18_withspendoutliers.RData'))
    #     save(argus_recoded, file=paste0(params$datapath,"/argus_18_withspendoutliers.RData"))
    #     rm(argus_recoded)
    #     foo <- gc()
    #     argus_recoded <- loadRData(paste0(params$datapath,'/argus_17.RData'),
    #                                params$colalloc)
    #     setkey(argus_recoded, respondentid)
    #   }
    # }
    rm(remove.expfreq)
    
    message('\n  Remove all outliers')
    if (dim(remove)[1] > 0) {
      argus_recoded <- argus_recoded[-argus_recoded[remove, which=TRUE, nomatch=0] ]
    }
    rm(remove)
    foo <- gc()
    
    # Save as RData
    message(paste0('\n  Memory usage is: ',memory.size(max=FALSE),' MB'))
    message(paste0('\n  Writing ',params$datapath,'/argus_18.RData'))
    
    
    save(argus_recoded, file=paste0(params$datapath,"/argus_18.RData"))
    
    # Validation of output
    if (params$mod_18$val) {
      
      rm(argus_recoded)
      foo <- gc()
      
      # The list of names of files to compare, in pairs (first is always the model file,
      # and second is always the comparison file)
      filename_list <- list(c(paste0(params$val_dir,'/SAS_Datasets/remove.',
                                     params$val_format),
                              paste0(params$outputpath,'/remove.csv')),
                            c(paste0(params$val_dir,'/SAS_Datasets/',
                                     tolower(params$datatype),'_argus_recoded.',
                                     params$val_format),
                              paste0(params$datapath,'/','argus_18.RData'))
      )
      
      # For each filename in filename_list, provide the name of the column(s) that
      # identify each unique row
      # Ex. key_column_list <- list('respondentid',
      #                             c('hhid','personid'))
      key_column_list <- list('respondentid','respondentid')
      
      validate_18(params,filename_list,key_column_list)
      
      argus_recoded <- loadRData(paste0(params$datapath,'/argus_18.RData'),
                                 params$colalloc)
    }
    
    foo <- gc()
    message(paste0('\n  Memory usage is: ',memory.size(max=FALSE),' MB'))
    message(paste0('\n  Dataset has ',nrow(argus_recoded),' rows and ',
                                      ncol(argus_recoded),' columns.'))
    return(argus_recoded)
    
  }

}
################################################################################
############################## Run Main Function ###############################
################################################################################
if (getOption('run.main', default=TRUE)) {
  
  # UTILITY FUNCTIONS (these are normally loaded by 10SetParameters)
  
  # Function that checks if package is installed.
  # If installed, it loads it. If it isn't, it installs it.
  pkgTest <- function(x) {
    if (!(x %in% installed.packages()[, 'Package'])) {
      tryCatch({
        message(paste0('INSTALLING PACKAGE ',toupper(x)))
        if (x == 'xda') {
          install_github("ujjwalkarn/xda")
        } else {
          install.packages(x, quiet = TRUE)
        }
      }, error = function(e) {
        stop(paste0('  ERROR: ',e))
      })
    } 
    message(paste0('LOADING PACKAGE ',toupper(x)))
    library(x,character.only = TRUE, quietly = TRUE)
  }
  
  # Function to format list to write to file
  write_list <- function(prefix,list,indent,total_length) {
    llist <- length(list)
    cond <- 1
    count <- 1
    while (cond) {
      ltoprint <- paste(list[1:count],collapse=', ')
      line <- paste(prefix,ltoprint)
      lline <- nchar(line)
      if (lline < total_length) {
        if (count < llist) {
          count <- count+1
        } else {
          cond <- 0
        }
      } else {
        cond <- 0
        count <- count-1
        if (count < 1) {
          line <- prefix
        } else {
          ltoprint <- paste(list[1:count],collapse=', ')
          line <- paste(prefix,ltoprint)
        }
      }
    }
    message(line)
    
    prefix <- paste(rep(" ",indent),collapse="")
    
    while (count < llist) {
      cond <- 1
      count <- count+1
      count2 <- count
      while (cond) {
        ltoprint <- paste(list[count2:count],collapse=', ')
        line <- paste0(prefix,ltoprint)
        lline <- nchar(line)
        if (lline < total_length) {
          if (count < llist) {
            count <- count+1
          } else {
            cond <- 0
          }
        } else {
          cond <- 0
          count <- count-1
          if (count < count2) {
            count <- count+1
          } else {
            ltoprint <- paste(list[count2:count],collapse=', ')
            line <- paste0(prefix,ltoprint)
          }
        }
      }
      message(line)
    }
  }
  
  # Get index of columns in a dataset
  get_indexes <- function(columns,dataset) {
    columns <- tolower(columns)
    indexes <- c()
    for (colname in columns) {
      indexes <- c(indexes,match(colname,tolower(names(dataset))))
    }
    return(indexes)
  }
  
  # This function returns TRUE wherever elements are different, including NA's,
  # and FALSE everywhere else.
  compareNA <- function(v1,v2) {
    same <- (v1 == v2) | (is.na(v1) & is.na(v2))
    same[is.na(same)] <- FALSE
    return(!same)
  }
  
  # Function to compare two data.table objects and write differences to file
  validate_dt_file <- function(Modelfile,Comparisonfile,key_column,outname,
                               dump_diff_file) {
    
    # First test: Check that both files have the same number of columns and rows
    
    ncolSAS <- base::ncol(Modelfile)
    nrowSAS <- base::nrow(Modelfile)
    ncolCSV <- base::ncol(Comparisonfile)
    nrowCSV <- base::nrow(Comparisonfile)
    
    if ((ncolSAS == ncolCSV) && (nrowSAS == nrowCSV)) { 
      
      # Second test: Check that columns in both files have the same names
      #              (case can be different (uppercase vs. lowercase) and order
      #               of the columns can be different, but names have to be the same)
      
      SAScolnames <- base::sort(base::tolower(base::colnames(Modelfile)))
      CSVcolnames <- base::sort(base::tolower(base::colnames(Comparisonfile)))
      ndiff <- 0
      for (icol in 1:base::length(SAScolnames)) {
        if (SAScolnames[icol] != CSVcolnames[icol]) { ndiff <- ndiff+1 }
      }
      
      if (ndiff == 0) {
        
        # Third test: Compare files column-by-column
        
        # Rearrange Rfile columns to be in the same order as Modelfile
        base::names(Modelfile) <- base::tolower(base::names(Modelfile))
        base::names(Comparisonfile) <- base::tolower(base::names(Comparisonfile))
        if (ncolSAS > 1) {
          Comparisonfile <- Comparisonfile[,names(Modelfile),with=F]
        }
        
        # Sort datasets according to key_column to have rows in the same order
        indexes <- get_indexes(key_column,Modelfile)
        key_names <- base::names(Modelfile)[indexes]
        if (nrowSAS > 1) {
          Modelfile <- setorderv(Modelfile,key_names)
          Comparisonfile <- setorderv(Comparisonfile,key_names)
        }
        
        # Use "compare" function to validate each column in the file against its 
        # SAS counterpart
        count <- 0
        for (i in 1:ncolSAS) {
          #i=6
          comparison <- compare::compare(Modelfile[[i]], Comparisonfile[[i]]
                                         ,equal = TRUE
                                         ,coerce = TRUE
                                         #,shorten = TRUE
                                         #,ignoreOrder = TRUE
                                         #,ignoreNameCase = TRUE
                                         #,ignoreNames = TRUE
                                         ,ignoreAttrs = TRUE
                                         ,round = TRUE
                                         #,ignoreCase = TRUE
                                         #,trim = TRUE
                                         #,dropLevels = TRUE
                                         #,ignoreLevelOrder = TRUE
                                         #,ignoreDimOrder = TRUE
                                         #,ignoreColOrder = TRUE
                                         #,ignoreComponentOrder = TRUE
                                         #,colsOnly = TRUE
                                         #,allowAll = TRUE
          )
          
          # If discrepancies are found, write discrepancies to file for evaluation
          # by the user
          if (!comparison$result) {
            count <- count+1
            base::message(paste0('  - Found ',sum(compareNA(comparison$tM[],comparison$tC[])),
                                 ' discrepancies in column: ',base::names(Modelfile)[i]))
            if (dump_diff_file) {
              SAS_np <- Modelfile[compareNA(comparison$tM[], comparison$tC[]),c(indexes,i),with=F]
              
              CSV_np <- Comparisonfile[compareNA(comparison$tM[], comparison$tC[]),c(indexes,i),with=F]
              
              base::names(SAS_np) <- base::paste0('Model_',base::names(SAS_np))
              base::names(CSV_np) <- base::paste0('Comparison_',base::names(CSV_np))
              for (j in 1:base::length(key_names)) {
                base::names(CSV_np)[base::names(CSV_np) == base::paste0('Comparison_',key_names[j])] <- key_names[j]
                base::names(SAS_np)[base::names(SAS_np) == base::paste0('Model_',key_names[j])] <- key_names[j]
              }
              
              diff <- base::merge(SAS_np, CSV_np, by=key_names, all=TRUE)
              base::message(paste0('    ',nrow(diff),' discrepancies found.'))
              model_name <- paste0('Model_',names(Modelfile)[i])
              comparison_name <- paste0('Comparison_',names(Modelfile)[i])
              if (class(diff[[model_name]]) == 'numeric') {
                diff$`%Diff` <- round((diff[,comparison_name,with=F]-diff[,model_name,with=F])/
                                        diff[,model_name,with=F]*100,0)
              }
              
              if (count == 1) {
                xlsx::write.xlsx(diff,paste0(outname,'.xlsx'),row.names=FALSE
                                 ,sheetName = base::names(Modelfile)[i])
              } else {
                xlsx::write.xlsx(diff,paste0(outname,'.xlsx'),row.names=FALSE
                                 ,sheetName = base::names(Modelfile)[i],append = TRUE)
              }
              
              jgc()
            }
          }
        }
        
        if (count == 0) {
          base::message('  - No discrepancies were found.')
        } else {
          if (dump_diff_file) {
            base::message(paste0('  - Report saved in ',outname,'.xlsx'))
            # Sys.setenv(RSTUDIO_PANDOC="C:/Program Files/RStudio/bin/pandoc")
            # render(paste0(params$code_loc,"/gen_val_report.Rmd"),#'all',
            #        output_file = paste0(outname,'.html'),
            #        params = list(path = params$path, outname = outname),
            #        quiet = T)
          }
        }
        
        
      } else {
        
        base::message('  - Discrepancies were found.')
        base::message('    Column names in model and comparison datasets do not match.')
        write_list('    Column names in model dataset are:', SAScolnames, 
                   6,80)
        write_list('    Column names in comparison dataset are:', CSVcolnames, 
                   6,80)
        
      }
      
    } else {
      
      base::message('  - Discrepancies were found.')
      base::message(base::paste0('    The model dataset has ',base::toString(nrowSAS)
                                 ,' rows and ',base::toString(ncolSAS),' columns.'))
      base::message(base::paste0('    while the comparison dataset has ',base::toString(nrowCSV)
                                 ,' rows and ',base::toString(ncolCSV),' columns.'))
    }
    
  }
  
  # Function to collect Java garbage when using xlsx package
  jgc <- function() {
    base::gc()
    .jcall("java/lang/System", method = "gc")
  }
  
  # Function to load and rename RData file, and allocate memory to it
  loadRData <- function(fileName,colalloc){
    #loads an RData file, and returns it
    load(fileName)
    alloc.col(get(ls()[ls() != "fileName"]),colalloc)
  }
  
  # Function to load and rename RData file
  loadRData_noalloc <- function(fileName) {
    #loads an RData file, and returns it
    load(fileName)
    get(ls()[ls() != "fileName"])
  }
  
  # Function to create list of metrics
  CreateMetricsList <- function(segmentlst,metrics,period.def) {
    if (is.null(period.def) == FALSE) {
      outputlist <- vector(mode="list"
                           , length = length(segmentlst) * length(metrics) * length(period.def))
      
      i <- 1
      for (x in seq_along(segmentlst)){
        for (y in seq_along(metrics)){
          for (z in seq_along(period.def)){
            outputlist[i] <- lapply(
              period.def[z]
              ,function(m) paste(segmentlst[x],metrics[y],m,sep="_"))
            i<- i + 1
          }
        }
      }
      return(outputlist)
    }
    
    else if (is.null(period.def) == TRUE){
      outputlist <- vector(mode="list"
                           , length= length(segmentlst) * length(metrics))
      
      i <- 1
      for (x in seq_along(segmentlst)){
        for (y in seq_along(metrics)){
          outputlist[i] <- paste(segmentlst[x],metrics[y],sep="_")
          i<- i + 1
        }
      }
      return(outputlist)
    }
    
  }
  
  # IMPORT PACKAGES
  # (these are normally loaded by the main script - 10SetParameters)
  pkgTest('haven')
  options(java.parameters = "-Xmx16000m")
  pkgTest('rJava')
  pkgTest('xlsx')
  pkgTest('compare')
  pkgTest('jsonlite')
  pkgTest('data.table')
  pkgTest('rmarkdown')
  pkgTest('knitr') # For all tables
  pkgTest('kableExtra')
  pkgTest('rlist')
  
  path <- 'E:/Citi_Amazon'
  params <- fromJSON(file.path(path,'params.json'), simplifyDataFrame=TRUE)
  
  message(paste0('  Loading ',params$datapath,'/argus_17.RData'))
  argus_recoded <- loadRData(paste0(params$datapath,'/argus_17.Rdata'),
                             params$colalloc)
  
  main_18(params,argus_recoded)
  
}