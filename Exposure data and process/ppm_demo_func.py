# -*- coding: utf-8 -*-
"""
Created on Wed Aug 29 14:42:47 2018

@author: chaner02
"""
def agebreak(agecol):
    if (agecol >= 1 and agecol <=17):
        return 1 
    elif agecol <= 24:
        return 2 
    elif agecol <= 34:
        return 3
    elif agecol <= 44:
        return 4
    elif agecol <= 54:
        return 5
    elif agecol <= 64:
        return 6
    elif agecol >= 65:
        return 7

def edu(educol):
    if  '<12' in educol or 'NA' in educol:
        return 1 
    elif 'HS' in educol:
        return 2 
    elif 'SC' in educol:
        return 3
    elif 'CG' in educol:
        return 4

def race(racecol):
    #black
    if racecol=='B':
        return 3  
    #white
    elif racecol=='O':
        return 1
    #hispanic
    elif racecol=='H':
        return 2

def income(incomecol):
    if incomecol == 'INC $75K+':
        return 4 
    elif incomecol == 'INC $50,000-$74,999':
        return 3 
    elif incomecol == 'INC $25,000-$49,999': 
        return 2 
    elif incomecol == 'INC <$25K':
        return 1 

def hhsize(hhcol):
    if hhcol == 1:
        return 1 
    elif hhcol == 2:
        return 2
    elif hhcol == 3:
        return 3 
    elif hhcol == 4:
        return 4 
    elif hhcol > 4:
        return 5 