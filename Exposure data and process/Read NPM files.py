# -*- coding: utf-8 -*-
"""
Created on Fri Jun 08 14:24:11 2018

@author: chaner02
"""

import pandas as pd
from pandas import ExcelWriter
from pandas import ExcelFile
import numpy as np
fn='Lowes on NFL Game and Post Summary_With Fox Only Flag.xlsx'

path=r'F:\SE\FOX_NFL\NPM Exposure'


sheetnames= ['FOX','NBC','CBS']
for sheetname in sheetnames:
    #read FOX, NBC, CBS on three different excel sheets
    globals()[sheetname]=pd.read_excel(path+'\\'+fn, sheetname='Lowes '+sheetname)
    #get the only three columns we need
    globals()[sheetname]=globals()[sheetname][['Household ID','Person ID','Exposure Unit Count']]
    #reanme Exposure Unit Count accordingly
    globals()[sheetname]=globals()[sheetname].rename(columns={'Exposure Unit Count':sheetname+'_Exp'})

#merge FOX, NBC, CBS
expO = pd.merge(NBC, FOX, how='outer', on = ['Household ID','Person ID'])
expfinal=pd.merge(expO, CBS, how='outer', on = ['Household ID','Person ID'])

#Total
expfinal['Total_Exp']=expfinal['NBC_Exp']+expfinal['FOX_Exp']+expfinal['CBS_Exp']
expfinal.shape #(94360, 5)
expfinal[expfinal['Total_Exp']>0].shape # 44,518 

#Fox Only
condition_FOX=((expfinal['FOX_Exp']>0) & (expfinal['CBS_Exp']==0) & (expfinal['NBC_Exp']==0))
expfinal['FoxOnly_Exp']=np.where(condition_FOX, expfinal['FOX_Exp'], 0)
expfinal[(expfinal['FoxOnly_Exp']>0)].shape #7898

#CBS Only
condition_CBS=((expfinal['CBS_Exp']>0) & (expfinal['FOX_Exp']==0) & (expfinal['NBC_Exp']==0))
expfinal['CBSOnly_Exp']=np.where(condition_CBS, expfinal['CBS_Exp'], 0)
expfinal[(expfinal['CBSOnly_Exp']>0)].shape #6145

#NBC only        
condition_NBC=((expfinal['NBC_Exp']>0) & (expfinal['FOX_Exp']==0) & (expfinal['CBS_Exp']==0))
expfinal['NBCOnly_Exp']=np.where(condition_NBC, expfinal['NBC_Exp'], 0)
expfinal[(expfinal['NBCOnly_Exp']>0)].shape #1825


#Fox_CBS_only
condition_FOX_CBS=(expfinal['FOX_Exp']>0) & (expfinal['CBS_Exp']>0) &  (expfinal['NBC_Exp']==0)
expfinal['FOX_CBSOnly_Exp']=np.where((condition_FOX_CBS), expfinal['FOX_Exp']+expfinal['CBS_Exp'], 0)
expfinal[(expfinal['FOX_CBSOnly_Exp']>0)].shape #11110

#FOX_NBC_only
condition_FOX_NBC=(expfinal['FOX_Exp']>0) & (expfinal['NBC_Exp']>0) &  (expfinal['CBS_Exp']==0)
expfinal['FOX_NBCOnly_Exp']=np.where((condition_FOX_NBC), expfinal['FOX_Exp']+expfinal['NBC_Exp'], 0)
expfinal[(expfinal['FOX_NBCOnly_Exp']>0)].shape #2306
         
#CBS_NBC_only
condition_CBS_NBC=(expfinal['CBS_Exp']>0) & (expfinal['NBC_Exp']>0) &  (expfinal['FOX_Exp']==0)
expfinal['CBS_NBCOnly_Exp']=np.where((condition_CBS_NBC), expfinal['CBS_Exp']+expfinal['NBC_Exp'], 0)
expfinal[(expfinal['CBS_NBCOnly_Exp']>0)].shape #1574


#Fox_NBC_CBS
condition_all3=(expfinal['CBS_Exp']>0) & (expfinal['NBC_Exp']>0) &  (expfinal['FOX_Exp'] > 0)
expfinal['All3_Exp']=np.where((condition_all3), expfinal['CBS_Exp']+expfinal['NBC_Exp']+expfinal['FOX_Exp'], 0)
expfinal[(expfinal['All3_Exp']>0)].shape #13660

expfinal.rename(columns={'Household ID': 'NPM_HHLD', 'Person ID': 'Person_ID'}, inplace=True)

expfinal.to_csv(path+'\\'+'FOX_NFL_exp.csv', index=False)