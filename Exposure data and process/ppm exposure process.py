# -*- coding: utf-8 -*-
"""
Created on Mon Aug 27 15:31:16 2018

@author: chaner02
"""

import pandas as pd 
import numpy as np

##########ppm_demo_func.py on S drive has all the recoding functions we need for ppm.  Don't change the code below######
import sys
sys.path.insert(0, r'S:\NBI_Sales_Effect\PPM Demos\Python script to read ppm and recoding')
import ppm_demo_func as ppmfuncs
demo_path= r'S:\NBI_sales_effect\PPM demos'
###########prep ppm demo###############################################################################################################


#specify the file names
filenames=['Raw_Demo_2018FEB 2018 0201-0228_PPM', 'Raw_Demo_2018MAR 2018 0301-0328_PPM', 'Raw_Demo_2018APR 2018 0329-0425_PPM']

dfs_list=[]
for file in filenames:
    df = pd.read_csv(demo_path+"\\"+file+".csv", sep='|')
    print(file,'-----', len(df))
    #recoding start 
    df['PNLST_SOURCE_ID']=df["HOUSEHOLDID"].map(str) + "-" + df["PERSONID"].map(str)
    df['AGE8'] = df["AGE"].apply(ppmfuncs.agebreak)
    df['EDUCATION2'] = df["EDUCATION_LEVEL"].apply(ppmfuncs.edu)
    df['MALE'] = [1 if name=='M' else 2 for name in df['GENDER']]
    df['RACE2'] = df["RACE"].apply(ppmfuncs.race)   
    df['INCOME'] = df['HH_INCOME'].apply(ppmfuncs.income)
	df['KIDS_ANY'] = [1 if kid > 0 else 0 for kid in df['KIDS_ANY'] ]    
    df['HH_SIZE']= df['HH_SIZE'].apply(ppmfuncs.hhsize)
	df=df.drop(['EDUCATION_LEVEL','HH_INCOME'],axis=1)
    #recoding end 
    dfs_list.append(df)


ppm_demo_recoded = pd.concat(dfs_list)
len(ppm_demo_recoded) #215773
ppm_demo_recoded=ppm_demo_recoded.drop_duplicates(['PNLST_SOURCE_ID'], keep='first') 
len(ppm_demo_recoded) #82012

del ppm_exp_demos_recoded['MARKET_NAME'] #deleted as not needed 

        
###########pre ppm exposure##########################################################################

datapath = r'S:\NBI_Sales_Effect\WestWood Jimmy Johns\Data_Exposure'
outpath = r'S:\NBI_Sales_Effect\WestWood Jimmy Johns\SAS_Datasets'
ppm_exposures = pd.read_csv(datapath+'\\Jimmy Johns Report.csv')
len(ppm_exposures) #52524

ppm_exposures['Panelist_ID'] = ppm_exposures['Panelist_ID'].str.replace(' ','')
ppm_exposures['PPM_HHLD'] = ppm_exposures['Panelist_ID'].str.split('-').str[0] 
ppm_exposures['Person_ID'] = ppm_exposures['Panelist_ID'].str.split('-').str[1] 


#merge ppm exposure with ppm demo 
ppm_exp_demos_recoded = pd.merge(ppm_demo_recoded, ppm_exposures, left_on=['PNLST_SOURCE_ID'], right_on=['Panelist_ID'], how='inner')
ppm_exp_demos_recoded = ppm_exp_demos_recoded[ppm_exp_demos_recoded['Unified_Weight'] > 0]
len(ppm_exp_demos_recoded) #46946


ppm_exp_demos_recoded.to_csv(outpath+'\\WestWood ppm exposure.csv', index=False)



