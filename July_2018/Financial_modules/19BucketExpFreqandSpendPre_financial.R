# Purpose: Bucket exposure frequency and Pre-period spend
#  Input:   argus_recoded
#  Output:  argus_recoded
#           R_Datasets\\argus_recoded.RData

#rm(list = ls(all = TRUE))
#options(run.main=TRUE)

################################################################################
########################## Auxiliary Functions #################################
################################################################################
{
  
  # Function to validate "comparison" dataset against "model" dataset.
  validate_19 <- function(params,filename_list,key_column_list) {
    
    path <- params$path
    dump_diff_file <- params$dump_diff_file
    
    if (!file.exists(paste0(path,'/Validation'))) { 
      dir.create(paste0(path,'/Validation')) 
    }
    
    for (j in 1:length(filename_list)) {
      #j<-1
      ModelFileName <- filename_list[[j]][1]
      ModelFileType <- substr(ModelFileName,regexpr('.',ModelFileName,fixed=TRUE)+1
                              ,nchar(ModelFileName))
      
      if (file.exists(ModelFileName)) {
        
        key_column <- key_column_list[[j]]
        
        # Find the matching file in the comparison folder
        ComparisonFileName <- filename_list[[j]][2]
        ComparisonFileType <- substr(ComparisonFileName,regexpr('.',
                                                                ComparisonFileName,fixed=TRUE)+1,
                                     nchar(ComparisonFileName))
        
        message(paste0('\n  Validating ',ComparisonFileName))
        message(paste0('  against ',ModelFileName))
        
        # Output folder and file names
        outfolder <- paste0(path,'/Validation')
        filename <- substr(ModelFileName,1,regexpr('.',ModelFileName,fixed=TRUE)-1)
        char_pos <- gregexpr('/',filename,fixed=TRUE)[[1]]
        filename <- substr(filename,char_pos[length(char_pos)]+1,nchar(filename))
        filename <- paste0('/diff_',filename)
        outname <- base::paste0(outfolder,filename)
        
        # Upload files
        if (ModelFileType == 'sas7bdat') {
          Modelfile <- haven::read_sas(ModelFileName)
        } else if (ModelFileType == 'csv') {
          Modelfile <- read.csv(file=ModelFileName)
        } else if (ModelFileType == 'RData') {
          Modelfile <- loadRData_noalloc(ModelFileName)
        } else if (ModelFileType == 'xlsx') {
          message('  - Please, add capability to read xlsx files.')
        }
        if (!('data.table' %in% class(Modelfile))) {
          Modelfile <- as.data.table(Modelfile)
        }
        colnames(Modelfile) <- base::tolower(base::colnames(Modelfile))
        
        if (nrow(Modelfile) == 0) {
          
          if (ComparisonFileType == 'sas7bdat') {
            Comparisonfile <- haven::read_sas(ComparisonFileName)
          } else if (ComparisonFileType == 'csv') {
            Comparisonfile <- read.csv(file=ComparisonFileName)
          } else if (ComparisonFileType == 'RData') {
            Comparisonfile <- loadRData_noalloc(ComparisonFileName)
          } else if (ComparisonFileType == 'xlsx') {
            message('  - Please, add capability to read xlsx files.')
          }
          colnames(Comparisonfile) <- base::tolower(base::colnames(Comparisonfile))
          
          if (nrow(Comparisonfile) == 0) {
            
            message('  - No discrepancies were found.')
            
          } else {
            
            message('  - Discrepancies were found.')
            message(paste0('    Dimensions of model dataset were ',
                           toString(nrow(Modelfile)),' x ',
                           toString(ncol(Modelfile))))
            message(paste0('    while dimensions of comparison dataset were ',
                           toString(nrow(Comparisonfile)),' x ',
                           toString(ncol(Comparisonfile))))
            
          }
          
        } else {
          
          if (ComparisonFileType == 'sas7bdat') {
            Comparisonfile <- haven::read_sas(ComparisonFileName)
          } else if (ComparisonFileType == 'csv') {
            Comparisonfile <- read.csv(file=ComparisonFileName)
          } else if (ComparisonFileType == 'RData') {
            Comparisonfile <- loadRData_noalloc(ComparisonFileName)
          } else if (ComparisonFileType == 'xlsx') {
            message('  - Please, add capability to read xlsx files.')
          }
          if (!('data.table' %in% class(Comparisonfile))) {
            Comparisonfile <- as.data.table(Comparisonfile)
          }
          colnames(Comparisonfile) <- base::tolower(base::colnames(Comparisonfile))
          
          if (nrow(Comparisonfile) == 0) {
            
            message('  - Discrepancies were found.')
            message(paste0('    Dimensions of model dataset were ',
                           toString(nrow(Modelfile)),' x ',
                           toString(ncol(Modelfile))))
            message(paste0('    while dimensions of comparison dataset were ',
                           toString(nrow(Comparisonfile)),' x ',
                           toString(ncol(Comparisonfile))))
            
          } else {
            
            # add respondentid to Modelfile
            Modelfile[, 'respondentid' := randomid]
            
            if (params$path == 'E:/Citi_Amazon') {
              # Delete columns with all NAs and "share" columns
              to_delete <- c(grep('citi_az_share',colnames(Modelfile),value=T),
                             grep('citi_ol_share',colnames(Modelfile),value=T))
              for (colname in colnames(Modelfile)) {
                if (length(unique(Modelfile[[colname]])) == 1) { 
                  if (is.na(unique(Modelfile[[colname]]))) {
                    to_delete <- c(to_delete,colname)
                  }
                }
              }
              to_delete <- unique(to_delete)
              Modelfile[, (to_delete) := NULL]
            }
            
            # Select only respondents that exist in both files
            overlap <- intersect(Modelfile[,respondentid],
                                 Comparisonfile[,respondentid])
            message(paste0('  - SAS file contains ',nrow(Modelfile),
                           ' respondents, while R file contains ',nrow(Comparisonfile),
                           ' respondents.'))
            message(paste0('    Doing validation for respondents that are ',
                           'in both files: ',length(overlap)))
            Modelfile <- Modelfile[which(respondentid %in% overlap),]
            Comparisonfile <- Comparisonfile[which(respondentid %in% overlap),]
            
            # Select only columns that exist in both files
            overlap_cols <- intersect(colnames(Modelfile),colnames(Comparisonfile))
            for (icol in overlap_cols) {
              #icol = overlap_cols[15]
              if( class(Comparisonfile[[icol]]) == "integer"){
                set(Modelfile, j = icol,
                    value = as.integer(gsub(',','',Modelfile[[icol]])))
              }else if(class(Comparisonfile[[icol]]) == "character"){
                set(Modelfile, j = icol,
                    value = as.character(Modelfile[[icol]]))
              }else if(class(Comparisonfile[[icol]]) == "numeric"){
                set(Modelfile, j = icol,
                    value = as.numeric(Modelfile[[icol]]))
              }
            }
            
            # # Replace NA with 0 in Modelfile to match Comparisonfile
            # for (i in seq_len(ncol(Modelfile))) { 
            #   set(Modelfile, which(is.na(Modelfile[[i]])), i, 0) 
            # }
            # 
            # # Replace NA with 0 in Comparisonfile to match Modelfile
            # for (i in seq_len(ncol(Comparisonfile))) { 
            #   set(Comparisonfile, which(is.na(Comparisonfile[[i]])), i, 0) 
            # }
            
            # Compare files
            # Select only columns that exist in both files
            validate_dt_file(Modelfile[,overlap_cols,with=F],
                             Comparisonfile[,overlap_cols,with=F],
                             key_column,outname,dump_diff_file)
            
            if (file.exists(paste0(outname,'.xlsx'))) {
              
              bucket_vars <- tolower(params$explist)
              buckets <- tolower(params$expfreq)
              names(buckets) <- bucket_vars
              
              wb <- loadWorkbook(paste0(outname,'.xlsx'))
              sheet_names <- names(getSheets(wb))
              for (sheetname in sheet_names) {
                # sheetname <- sheet_names[2]
                loc_bucket <- grep(sheetname,buckets)
                if (length(loc_bucket) > 0) {
                  sheet <- read.xlsx(file=paste0(outname,'.xlsx'),
                                     sheetName=sheetname)
                  names(sheet) <- gsub('X.','%',names(sheet))
                  bucket_var <- names(buckets)[loc_bucket]
                  sheet <- merge(sheet,Comparisonfile[,c(key_column,
                                                         bucket_var),with=F],
                                 by=key_column)
                  removeSheet(wb,sheetName=sheetname)
                  new_sheet <- createSheet(wb,sheetName=sheetname)
                  addDataFrame(sheet,new_sheet,row.names=F)
                  rm(sheet)
                  foo <- gc()
                }
              }
              saveWorkbook(wb,paste0(outname,'.xlsx'))
            }
            rm(Modelfile)
            rm(Comparisonfile)
            foo <- gc()
          }
          
        }
        
      } else {
        
        message(paste0('  - ',ModelFileName,' does not exist.'))
        
      }
      
    }
    
  }
  
  # bucket.defs = bucket.defs[[i]]
  # expforcut = explist[i]
  # bucket.unexposed = params$bucket.unexposed[i]
  # expforbucket = explist[i]
  # expfreq = expfreq[i]
  create_bucket <- function(argus_recoded,bucket.defs,expforcut,
                            bucket.unexposed,expforbucket,expfreq) {
    
    if (substr(bucket.defs[1],1,1) == 'Q') {
      # Use quantiles to create cuts
      numbkt <- as.integer(substr(bucket.defs,
                                  gregexpr('_',bucket.defs,fixed=T)[[1]]+1,
                                  nchar(bucket.defs)))
      quants <- quantile(argus_recoded[argus_recoded[[expforcut]] > 
                                         bucket.unexposed][[expforcut]],
                         probs = seq(0,1,1/numbkt))
      quants[1] <- bucket.unexposed
    } else {
      # Use user-provided bucket definitions to create cuts
      numbkt <- 0
      if (tail(bucket.defs, 1)=='max') {
        if (length(bucket.defs) == 1) {
          quants <- c(bucket.unexposed,max(argus_recoded[[expforcut]]))
        } else {
          quants <- c(bucket.unexposed, 
                      as.numeric(bucket.defs[1:(length(bucket.defs)-1)]), 
                      max(argus_recoded[[expforcut]]))
        }
      } else {
        quants <- c(bucket.unexposed,as.numeric(bucket.defs))
      }
      names(quants) <- c('unexposed',as.character(seq(1,length(quants)-1)))
    }
    labels <- seq(1,length(quants)-1, 1)
    
    message(paste0("    Quantiles of ", expforcut, " are "))
    message(format(names(quants),width=10,justify='right'))
    message(format(quants,width=10))
    message(paste0("    with labels"))
    message(format(labels,width=10,justify='right'))
    if (length(unique(quants)) <= numbkt) {
      
      message(paste0("    Warning: One of the buckets for ",expforcut,
                     " has a width of zero."))
      quant <- 1
      while (quant < length(quants)) {
        #quant=2
        if (quants[[quant]] == quants[[quant+1]]) {
          message(paste0('    Deleting ',names(quants)[quant+1],' quantile.'))
          quants <- quants[-(quant+1)]
          labels <- labels[-quant]
          message(paste0("    Quantiles of ", expforcut, " are now"))
          message(format(names(quants),width=10,justify='right'))
          message(format(quants,width=10))
          message(paste0("    with labels"))
          message(format(labels,width=10,justify='right'))
        } else {
          quant <- quant+1
        }
      }
      
    }
    
    # recode expfreq
    message(paste0('    Generating ',expfreq))
    argus_recoded[, (expfreq) := cut(argus_recoded[[expforbucket]],
                                      breaks=quants,labels=labels)]
    set(argus_recoded
        , i     = which(argus_recoded[[expforbucket]] <= bucket.unexposed)
        , j     = expfreq
        , value = '0')
    # set(argus_recoded
    #     , i     = which(is.na(argus_recoded[[expfreq]]))
    #     , j     = expfreq
    #     , value = '0')
    set(argus_recoded
        , j 	  = expfreq
        , value = as.integer(as.character(argus_recoded[[expfreq]])))
    
    return(argus_recoded)
  }
  
}
################################################################################
############################# Main Function ####################################
################################################################################
{
  
  main_19 <- function(params,argus_recoded) {
    
    # 0. Read argus data
    message(paste0('\n  Memory usage is: ',memory.size(max=FALSE),' MB'))
    
    # 1. Bucket exposures automatically into 3 groups
    #    (if unexposed is less than 10%, does it even make sense to 
    #     create H/M/L groups)
    message("\n  Bucket exposures")# automatically into three groups")
    
    #unexposed.upper <- params$bucket.unexposed
    explist <- tolower(params$explist)
    expfreq <- tolower(params$expfreq)
    expyes <- tolower(params$expyes)
    
    # Convert bucket.defs back to list 
    # (it is converted to matrix when saved to json)
    if (class(params$bucket.defs) == "matrix") {
      bucket.defs <- lapply(1:(nrow(params$bucket.defs)), 
                        function (i) params$bucket.defs[i ,])
    } else {
      bucket.defs <- params$bucket.defs
    }
    
    if (params$bucket.method == 1) { # if bucket.method == 1
      
      for (i in seq_along(explist)) {
        #i<-1
        message(paste0('\n  - Bucketing ',explist[i]))
        
        argus_recoded <- create_bucket(argus_recoded,bucket.defs[[i]],explist[i],
                                       params$bucket.unexposed[i],explist[i],
                                       expfreq[i])
      
        {
        # if (substr(bucket.defs[[i]][1],1,1) == 'Q') {
        #   # Use quantiles to create cuts
        #   numbkt <- as.integer(substr(bucket.defs[[i]],
        #                               gregexpr('_',bucket.defs[[i]],fixed=T)[[1]]+1,
        #                               nchar(bucket.defs[[i]])))
        #   quants <- quantile(argus_recoded[argus_recoded[[explist[i]]] > 
        #                                      params$bucket.unexposed[i]][[explist[i]]],
        #                      probs = seq(0,1,1/numbkt))
        #   quants[1] <- params$bucket.unexposed[i]
        # } else {
        #   # Use user-provided bucket definitions to create cuts
        #   numbkt <- 0
        #   if (tail(bucket.defs[[i]], 1)=='max') {
        #     if (length(bucket.defs[[i]]) == 1) {
        #       quants <- c(params$bucket.unexposed[i],
        #                   max(argus_recoded[[explist[i]]]))
        #     } else {
        #       quants <- c(params$bucket.unexposed[i], 
        #                   as.numeric(bucket.defs[[i]][1:(length(bucket.defs[[i]])-1)]), 
        #                   max(argus_recoded[[explist[i]]]))
        #     }
        #   } else {
        #     quants <- c(params$bucket.unexposed[i],as.numeric(bucket.defs[[i]]))
        #   }
        #   names(quants) <- c('unexposed',as.character(seq(1,length(quants)-1)))
        # }
        # labels <- seq(1, length(quants)-1, 1)
        # 
        # message(paste0("    Quantiles of ", explist[i], " are "))
        # message(format(names(quants),width=10,justify='right'))
        # message(format(quants,width=10))
        # message(paste0("    with labels"))
        # message(format(labels,width=10,justify='right'))
        # if (length(unique(quants)) <= numbkt) {
        #   
        #   message(paste0("    Warning: One of the buckets for ",explist[i],
        #                  " has a width of zero."))
        #   quant <- 1
        #   while (quant < length(quants)) {
        #     #quant=2
        #     if (quants[[quant]] == quants[[quant+1]]) {
        #       message(paste0('    Deleting ',names(quants)[quant+1],' quantile.'))
        #       quants <- quants[-(quant+1)]
        #       labels <- labels[-quant]
        #       message(paste0("    Quantiles of ", explist[i], " are now"))
        #       message(format(names(quants),width=10,justify='right'))
        #       message(format(quants,width=10))
        #       message(paste0("    with labels"))
        #       message(format(labels,width=10,justify='right'))
        #     } else {
        #       quant <- quant+1
        #     }
        #   }
        #   
        # }
        # 
        # # recode expfreq
        # message(paste0('    Generating ',expfreq[i]))
        # argus_recoded[, expfreq[i] := cut(argus_recoded[[explist[i]]],
        #                                   breaks=quants,labels=labels)]
        # set(argus_recoded
        #     , i     = which(argus_recoded[[explist[i]]] <= params$bucket.unexposed[i])
        #     , j     = expfreq[i]
        #     , value = '0')
        # # set(argus_recoded
        # #     , i     = which(is.na(argus_recoded[[expfreq[i]]]))
        # #     , j     = expfreq[i]
        # #     , value = '0')
        # set(argus_recoded
        #     , j 	  = expfreq[i]
        #     , value = as.integer(as.character(argus_recoded[[expfreq[i]]])))
        }
        
        # Make unexposed group consistent across exposures
        if (params$consistent.unexposed) {
          if ('unexp_flag' %in% colnames(argus_recoded)) {
            set(argus_recoded
                , i     = which(argus_recoded[[expfreq[i]]] == 0)
                , j     = expfreq[i]
                , value = NA)
            set(argus_recoded
                , i     = which(argus_recoded[['unexp_flag']] == 1)
                , j     = expfreq[i]
                , value = 0)
          } else if (i > 1) {
            set(argus_recoded
                , i     = which((is.na(argus_recoded[[explist[i]]])) | 
                                  (is.na(argus_recoded[[explist[1]]])) |
                                  ((argus_recoded[[explist[i]]] <= params$bucket.unexposed[i]) & 
                                     (argus_recoded[[explist[1]]] > params$bucket.unexposed[1])))
                , j     = expfreq[i]
                , value = NA)
          }
        } 
        
        # recode expyes
        message(paste0('    Generating ',expyes[i]))
        set(argus_recoded
            , j     = expyes[i]
            , value = ifelse(is.na(argus_recoded[[expfreq[i]]]), NA, 
                             ifelse(argus_recoded[[expfreq[i]]] == 0, 0, 1)))
      } # close loop over exposure
      
    } else if (params$bucket.method == 2) { # if bucket.method == 2
      
      # Assuming the first variable in explist is the "total" variable
      message('\n  - Generating cuts using Total Exposure variable')
      for (i in seq_along(explist)){
        
        argus_recoded <- create_bucket(argus_recoded,bucket.defs[[1]],explist[1],
                                       params$bucket.unexposed[1],explist[i],
                                       expfreq[i])
      {
      # if (substr(bucket.defs[[1]][1],1,1) == 'Q') {
      #   # Use quantiles to create cuts
      #   numbkt <- as.integer(substr(bucket.defs[[1]],
      #                               gregexpr('_',bucket.defs[[1]],fixed=T)[[1]]+1,
      #                               nchar(bucket.defs[[1]])))
      #   quants <- quantile(argus_recoded[argus_recoded[[explist[1]]] > 
      #                                      params$bucket.unexposed[1]][[explist[1]]],
      #                      probs = seq(0,1,1/numbkt))
      #   quants[1] <- params$bucket.unexposed[1]
      # } else {
      #   numbkt <- 0
      #   if (tail(bucket.defs[[1]], 1) == 'max') {
      #     if (length(bucket.defs[[1]]) == 1) {
      #       quants <- c(params$bucket.unexposed[1],
      #                   max(argus_recoded[[explist[1]]]))
      #     } else {
      #       quants <- c(params$bucket.unexposed[1], 
      #                   as.numeric(bucket.defs[[1]][1:(length(bucket.defs[[1]])-1)]), 
      #                   max(argus_recoded[[explist[1]]]))
      #     }
      #   } else {
      #     quants <- c(params$bucket.unexposed[1], as.numeric(bucket.defs[[1]]))
      #   }
      #   names(quants) <- c('unexposed',as.character(seq(1,length(quants)-1)))
      # }
      # labels <- seq(1, length(quants)-1, 1)
      #   
      # message(paste0("    Quantiles of ", explist[i], " are "))
      # message(format(names(quants),width=10,justify='right'))
      # message(format(quants,width=10))
      # message(paste0("    with labels"))
      # message(format(labels,width=10,justify='right'))
      # 
      # if (length(unique(quants)) <= numbkt) {
      #   
      #   message(paste0("    Warning: One of the buckets for ",explist[i],
      #                  " has a width of zero."))
      #   quant <- 1
      #   while (quant < length(quants)) {
      #     #quant=2
      #     if (quants[[quant]] == quants[[quant+1]]) {
      #       message(paste0('    Deleting ',names(quants)[quant+1],' quantile.'))
      #       quants <- quants[-(quant+1)]
      #       labels <- labels[-quant]
      #       message(paste0("    Quantiles of ", explist[i], " are now"))
      #       message(format(names(quants),width=10,justify='right'))
      #       message(format(quants,width=10))
      #       message(paste0("    with labels"))
      #       message(format(labels,width=10,justify='right'))
      #     } else {
      #       quant <- quant+1
      #     }
      #   }
      #   
      # }
      # 
      # for (i in seq_along(explist)){
      #   
      #   # recode expfreq
      #   message(paste0('    Bucketing ',explist[i],'. Generating ',expfreq[i]))
      #   argus_recoded[, expfreq[i] := cut(argus_recoded[[explist[i]]],
      #                                     breaks=quants,labels=labels)]
      #   set(argus_recoded
      #       , i     = which(argus_recoded[[explist[i]]] <= params$bucket.unexposed[1])
      #       , j     = expfreq[i]
      #       , value = '0')
      #   # set(argus_recoded
      #   #     , i     = which(is.na(argus_recoded[[expfreq[i]]]))
      #   #     , j     = expfreq[i]
      #   #     , value = '0')
      #   set(argus_recoded
      #       , j 	  = expfreq[i]
      #       , value = as.integer(as.character(argus_recoded[[expfreq[i]]])))
        }
        # Make unexposed group consistent across exposures
        
        if (params$consistent.unexposed) {
          if ('unexp_flag' %in% colnames(argus_recoded)) {
            set(argus_recoded
                , i     = which(expfreq[i] == 0)
                , j     = expfreq[i]
                , value = NA)
            set(argus_recoded
                , i     = which(unexp_flag == 1)
                , j     = expfreq[i]
                , value = 0)
          } else if (i > 1) {
            set(argus_recoded
                , i     = which((is.na(argus_recoded[[explist[i]]])) | 
                                  (is.na(argus_recoded[[explist[1]]])) |
                                  ((argus_recoded[[explist[i]]] <= params$bucket.unexposed[i]) & 
                                     (argus_recoded[[explist[1]]] > params$bucket.unexposed[1])))
                , j     = expfreq[i]
                , value = NA)
          }
        }
        
        # recode expyes
        message(paste0('    Generating ',expyes[i]))
        set(argus_recoded
            , j     = expyes[i]
            , value = ifelse(is.na(argus_recoded[[expfreq[i]]]), NA,
                             ifelse(argus_recoded[[expfreq[i]]] == 0, 0, 1)))      
      }
    }
    
    
    # 2. Bucket Heavy Brand Buyer History based on preperiod
    fv_list <- tolower(params$bucklist)
    if (length(fv_list) > 0) {
      
      message("\n  Bucket Heavy Brand Buyer History based on preperiod")
      numbkt <- 3
      for (i in seq_along(fv_list)) {
        #i<-1
        message(paste0('\n  - Bucketing ',fv_list[i]))
        
        argus_recoded <- create_bucket(argus_recoded,'Q_3',fv_list[i],0,
                                       fv_list[i],paste0(fv_list[i],"_hml"))
        
        {
        # # create quantiles
        # quants <- quantile(argus_recoded[argus_recoded[[fv_list[i]]] > 0][[fv_list[i]]],
        #                    probs = round(seq(0,1,1/numbkt),2))
        # quants[1] <- 0
        # labels <- seq(1,numbkt,1)
        # 
        # message(paste0("    Quantiles of ", fv_list[i], " are "))
        # message(format(names(quants),width=10,justify='right'))
        # message(format(quants,width=10))
        # message(paste0("    with labels"))
        # message(format(labels,width=10,justify='right'))
        # 
        # if (length(unique(quants)) <= numbkt) {
        #   message(paste0("    Warning: One of the buckets for ",fv_list[i],
        #                  " has a width of zero."))
        #   quant <- 1
        #   while (quant < length(quants)) {
        #     #quant=2
        #     if (quants[[quant]] == quants[[quant+1]]) {
        #       message(paste0('    Deleting ',names(quants)[quant+1],' quantile.'))
        #       quants <- quants[-(quant+1)]
        #       labels <- labels[-quant]
        #       message(paste0("    Quantiles of ", fv_list[i], " are now"))
        #       message(format(names(quants),width=10,justify='right'))
        #       message(format(quants,width=10))
        #       message(paste0("    with labels"))
        #       message(format(labels,width=10,justify='right'))
        #     } else {
        #       quant <- quant+1
        #     }
        #   }
        #   
        # }
        # 
        # # recoding / bucketing
        # message(paste0('    Generating ',fv_list[i],"_hml"))
        # 
        # set(argus_recoded
        #     ,j     = paste0(fv_list[i],"_hml")
        #     ,value = cut(argus_recoded[[fv_list[i]]], breaks=quants, labels=labels))
        # set(argus_recoded
        #     , i     = which(argus_recoded[[fv_list[i]]] == 0)
        #     , j     = paste0(fv_list[i],"_hml")
        #     , value = '0')
        # # set(argus_recoded
        # #     , i     = which(is.na(argus_recoded[[paste0(fv_list[i],"_hml")]]))
        # #     , j     = paste0(fv_list[i],"_hml")
        # #     , value = '0')
        # set(argus_recoded
        #     , j 	  = paste0(fv_list[i],"_hml")
        #     , value = as.integer(as.character(argus_recoded[[paste0(fv_list[i],"_hml")]])))
        }
        
      }
      
    }
    
    # 3. Save as RData
    message(paste0('\n  Memory usage is: ',memory.size(max=FALSE),' MB'))
    message(paste0('\n  Writing ',params$datapath,'/argus_19.RData'))
    save(argus_recoded, file=paste0(params$datapath,"/argus_19.RData"))
    
    foo <- gc()
    message(paste0('\n  Memory usage is: ',memory.size(max=FALSE),' MB'))
    message(paste0('\n  Dataset has ',nrow(argus_recoded),' rows and ',
                   ncol(argus_recoded),' columns.'))
    
    if (params$mod_19$val) {
      
      rm(argus_recoded)
      foo <- gc()
      message(paste0('\n  Memory usage is: ',memory.size(max=FALSE),' MB'))
      
      # The list of names of files to compare, in pairs (first is always the model file,
      # and second is always the comparison file)
      filename_list <- list(c(paste0(params$val_dir,'/SAS_Datasets/',
                                     tolower(params$datatype),'_argus_recoded.',
                                     params$val_format),
                              paste0(params$datapath,'/','argus_19.RData'))
      )
      
      # For each filename in filename_list, provide the name of the column(s) that
      # identify each unique row
      # Ex. key_column_list <- list('respondentid',
      #                             c('hhid','personid'))
      key_column_list <- list('respondentid')
      
      validate_19(params,filename_list,key_column_list)
      
      argus_recoded <- loadRData(paste0(params$datapath,"/argus_19.RData"),
                                 params$colalloc)
    }
    
    return(argus_recoded)  
  }
  
}
################################################################################
############################## Run Main Function ###############################
################################################################################
if (getOption('run.main', default=TRUE)) {
  
  # UTILITY FUNCTIONS (these are normally loaded by 10SetParameters)
  
  # Function that checks if package is installed.
  # If installed, it loads it. If it isn't, it installs it.
  pkgTest <- function(x) {
    if (!(x %in% installed.packages()[, 'Package'])) {
      tryCatch({
        message(paste0('INSTALLING PACKAGE ',toupper(x)))
        if (x == 'xda') {
          install_github("ujjwalkarn/xda")
        } else {
          install.packages(x, quiet = TRUE)
        }
      }, error = function(e) {
        stop(paste0('  ERROR: ',e))
      })
    } 
    message(paste0('LOADING PACKAGE ',toupper(x)))
    library(x,character.only = TRUE, quietly = TRUE)
  }
  
  # Function to format list to write to file
  write_list <- function(prefix,list,indent,total_length) {
    llist <- length(list)
    cond <- 1
    count <- 1
    while (cond) {
      ltoprint <- paste(list[1:count],collapse=', ')
      line <- paste(prefix,ltoprint)
      lline <- nchar(line)
      if (lline < total_length) {
        if (count < llist) {
          count <- count+1
        } else {
          cond <- 0
        }
      } else {
        cond <- 0
        count <- count-1
        if (count < 1) {
          line <- prefix
        } else {
          ltoprint <- paste(list[1:count],collapse=', ')
          line <- paste(prefix,ltoprint)
        }
      }
    }
    message(line)
    
    prefix <- paste(rep(" ",indent),collapse="")
    
    while (count < llist) {
      cond <- 1
      count <- count+1
      count2 <- count
      while (cond) {
        ltoprint <- paste(list[count2:count],collapse=', ')
        line <- paste0(prefix,ltoprint)
        lline <- nchar(line)
        if (lline < total_length) {
          if (count < llist) {
            count <- count+1
          } else {
            cond <- 0
          }
        } else {
          cond <- 0
          count <- count-1
          if (count < count2) {
            count <- count+1
          } else {
            ltoprint <- paste(list[count2:count],collapse=', ')
            line <- paste0(prefix,ltoprint)
          }
        }
      }
      message(line)
    }
  }
  
  # Get index of columns in a dataset
  get_indexes <- function(columns,dataset) {
    columns <- tolower(columns)
    indexes <- c()
    for (colname in columns) {
      indexes <- c(indexes,match(colname,tolower(names(dataset))))
    }
    return(indexes)
  }
  
  # This function returns TRUE wherever elements are different, including NA's,
  # and FALSE everywhere else.
  compareNA <- function(v1,v2) {
    same <- (v1 == v2) | (is.na(v1) & is.na(v2))
    same[is.na(same)] <- FALSE
    return(!same)
  }
  
  # Function to compare two data.table objects and write differences to file
  validate_dt_file <- function(Modelfile,Comparisonfile,key_column,outname,
                               dump_diff_file) {
    
    # First test: Check that both files have the same number of columns and rows
    
    ncolSAS <- base::ncol(Modelfile)
    nrowSAS <- base::nrow(Modelfile)
    ncolCSV <- base::ncol(Comparisonfile)
    nrowCSV <- base::nrow(Comparisonfile)
    
    if ((ncolSAS == ncolCSV) && (nrowSAS == nrowCSV)) { 
      
      # Second test: Check that columns in both files have the same names
      #              (case can be different (uppercase vs. lowercase) and order
      #               of the columns can be different, but names have to be the same)
      
      SAScolnames <- base::sort(base::tolower(base::colnames(Modelfile)))
      CSVcolnames <- base::sort(base::tolower(base::colnames(Comparisonfile)))
      ndiff <- 0
      for (icol in 1:base::length(SAScolnames)) {
        if (SAScolnames[icol] != CSVcolnames[icol]) { ndiff <- ndiff+1 }
      }
      
      if (ndiff == 0) {
        
        # Third test: Compare files column-by-column
        
        # Rearrange Rfile columns to be in the same order as Modelfile
        base::names(Modelfile) <- base::tolower(base::names(Modelfile))
        base::names(Comparisonfile) <- base::tolower(base::names(Comparisonfile))
        if (ncolSAS > 1) {
          Comparisonfile <- Comparisonfile[,names(Modelfile),with=F]
        }
        
        # Sort datasets according to key_column to have rows in the same order
        indexes <- get_indexes(key_column,Modelfile)
        key_names <- base::names(Modelfile)[indexes]
        if (nrowSAS > 1) {
          Modelfile <- setorderv(Modelfile,key_names)
          Comparisonfile <- setorderv(Comparisonfile,key_names)
        }
        
        # Use "compare" function to validate each column in the file against its 
        # SAS counterpart
        count <- 0
        for (i in 1:ncolSAS) {
          #i=6
          comparison <- compare::compare(Modelfile[[i]], Comparisonfile[[i]]
                                         ,equal = TRUE
                                         ,coerce = TRUE
                                         #,shorten = TRUE
                                         #,ignoreOrder = TRUE
                                         #,ignoreNameCase = TRUE
                                         #,ignoreNames = TRUE
                                         ,ignoreAttrs = TRUE
                                         ,round = TRUE
                                         #,ignoreCase = TRUE
                                         #,trim = TRUE
                                         #,dropLevels = TRUE
                                         #,ignoreLevelOrder = TRUE
                                         #,ignoreDimOrder = TRUE
                                         #,ignoreColOrder = TRUE
                                         #,ignoreComponentOrder = TRUE
                                         #,colsOnly = TRUE
                                         #,allowAll = TRUE
          )
          
          # If discrepancies are found, write discrepancies to file for evaluation
          # by the user
          if (!comparison$result) {
            count <- count+1
            base::message(paste0('  - Found discrepancies in column: ',
                                 base::names(Modelfile)[i]))
            if (dump_diff_file) {
              SAS_np <- Modelfile[compareNA(comparison$tM[], comparison$tC[]),c(indexes,i),with=F]
              
              CSV_np <- Comparisonfile[compareNA(comparison$tM[], comparison$tC[]),c(indexes,i),with=F]
              
              base::names(SAS_np) <- base::paste0('Model_',base::names(SAS_np))
              base::names(CSV_np) <- base::paste0('Comparison_',base::names(CSV_np))
              for (j in 1:base::length(key_names)) {
                base::names(CSV_np)[base::names(CSV_np) == base::paste0('Comparison_',key_names[j])] <- key_names[j]
                base::names(SAS_np)[base::names(SAS_np) == base::paste0('Model_',key_names[j])] <- key_names[j]
              }
              
              diff <- base::merge(SAS_np, CSV_np, by=key_names, all=TRUE)
              base::message(paste0('    ',nrow(diff),' discrepancies found.'))
              model_name <- paste0('Model_',names(Modelfile)[i])
              comparison_name <- paste0('Comparison_',names(Modelfile)[i])
              if (class(diff[[model_name]]) == 'numeric') {
                diff$`%Diff` <- round((diff[,comparison_name,with=F]-diff[,model_name,with=F])/
                                        diff[,model_name,with=F]*100,0)
              }
              
              if (count == 1) {
                xlsx::write.xlsx(diff,paste0(outname,'.xlsx'),row.names=FALSE
                                 ,sheetName = base::names(Modelfile)[i])
              } else {
                xlsx::write.xlsx(diff,paste0(outname,'.xlsx'),row.names=FALSE
                                 ,sheetName = base::names(Modelfile)[i],append = TRUE)
              }
              
              jgc()
            }
          }
        }
        
        if (count == 0) {
          base::message('  - No discrepancies were found.')
        } else {
          if (dump_diff_file) {
            base::message(paste0('  - Report saved in ',outname,'.xlsx'))
            # Sys.setenv(RSTUDIO_PANDOC="C:/Program Files/RStudio/bin/pandoc")
            # render(paste0(params$code_loc,"/gen_val_report.Rmd"),#'all',
            #        output_file = paste0(outname,'.html'),
            #        params = list(path = params$path, outname = outname),
            #        quiet = T)
          }
        }
        
        
      } else {
        
        base::message('  - Discrepancies were found.')
        base::message('    Column names in model and comparison datasets do not match.')
        write_list('    Column names in model dataset are:', SAScolnames, 
                   6,80)
        write_list('    Column names in comparison dataset are:', CSVcolnames, 
                   6,80)
        
      }
      
    } else {
      
      base::message('  - Discrepancies were found.')
      base::message(base::paste0('    The model dataset has ',base::toString(nrowSAS)
                                 ,' rows and ',base::toString(ncolSAS),' columns.'))
      base::message(base::paste0('    while the comparison dataset has ',base::toString(nrowCSV)
                                 ,' rows and ',base::toString(ncolCSV),' columns.'))
    }
    
  }
  
  # Function to collect Java garbage when using xlsx package
  jgc <- function() {
    base::gc()
    .jcall("java/lang/System", method = "gc")
  }
  
  # Function to load and rename RData file, and allocate memory to it
  loadRData <- function(fileName,colalloc){
    #loads an RData file, and returns it
    load(fileName)
    alloc.col(get(ls()[ls() != "fileName"]),colalloc)
  }
  
  # Function to load and rename RData file
  loadRData_noalloc <- function(fileName) {
    #loads an RData file, and returns it
    load(fileName)
    get(ls()[ls() != "fileName"])
  }
  
  # IMPORT PACKAGES
  # (these are normally loaded by the main script - 10SetParameters)
  pkgTest('haven')
  options(java.parameters = "-Xmx16000m")
  pkgTest('rJava')
  pkgTest('xlsx')
  pkgTest('compare')
  pkgTest('jsonlite')
  pkgTest('data.table')
  pkgTest('rmarkdown')
  pkgTest('knitr') # For all tables
  pkgTest('kableExtra')
  
  path <- 'E:/Citi_Amazon'
  params <- fromJSON(file.path(path,'params.json'), simplifyDataFrame=TRUE)
  message(paste0('  Loading ',params$datapath,'/argus_18.RData'))
  argus_recoded <- loadRData(paste0(params$datapath,"/argus_18.RData"),
                             params$colalloc)
  if (params$path == 'E:/CapOne') {
    set(argus_recoded, j = 'total_rsp_exp',
        value = ifelse(argus_recoded[['total_rsp_exp']] >  2, argus_recoded[['total_rsp_exp']],
                       ifelse(argus_recoded[['total_rsp_exp']] <= 2, 0, NA)))
  }
  main_19(params,argus_recoded)
  
}